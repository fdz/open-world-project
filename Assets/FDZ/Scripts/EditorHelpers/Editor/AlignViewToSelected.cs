﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.ShortcutManagement;

namespace FDZ
{
    public static class AlignViewToSelected
    {
        // _ no modifier
        // # shift
        // % ctrl
        // & alt

        [MenuItem("FDZ/Align View To Selected/Front _1")]
        public static void AlignViewToSelectedFront()
        {
            Align(Side.Front);
        }

        [MenuItem("FDZ/Align View To Selected/Back &1")]
        public static void AlignViewToSelectedBack()
        {
            Align(Side.Back);
        }

        [MenuItem("FDZ/Align View To Selected/Right &2")]
        public static void AlignViewToSelectedRight()
        {
            Align(Side.Right);
        }

        [MenuItem("FDZ/Align View To Selected/Left _2")]
        public static void AlignViewToSelectedLeft()
        {
            Align(Side.Left);
        }

        [MenuItem("FDZ/Align View To Selected/Top _3")]
        public static void AlignViewToSelectedTop()
        {
            Align(Side.Top);
        }

        [MenuItem("FDZ/Align View To Selected/Bottom &3")]
        public static void AlignViewToSelectedBottom()
        {
            Align(Side.Bottom);
        }

        public enum Side
        {
            Front,
            Back,
            Right,
            Left,
            Top,
            Bottom
        }

        public static void Align(Side side)
        {
            var sceneView = SceneView.lastActiveSceneView;
            var selected = Selection.activeGameObject?.transform;
            if (selected != null)
            {
                Debug.Log($"Align View To Selected ({side})");
                sceneView.pivot = selected.position;
                switch (side)
                {
                    default:
                    case Side.Front:
                    sceneView.rotation = Quaternion.LookRotation(-selected.forward, selected.up);
                    break;

                    case Side.Back:
                    sceneView.rotation = Quaternion.LookRotation(selected.forward, selected.up);
                    break;

                    case Side.Right:
                    sceneView.rotation = Quaternion.LookRotation(-selected.right, selected.up);
                    break;

                    case Side.Left:
                    sceneView.rotation = Quaternion.LookRotation(selected.right, selected.up);
                    break;

                    case Side.Top:
                    sceneView.rotation = Quaternion.LookRotation(-selected.up, selected.forward);
                    break;

                    case Side.Bottom:
                    sceneView.rotation = Quaternion.LookRotation(selected.up, selected.forward);
                    break;

                }
            }
        }
    }
}