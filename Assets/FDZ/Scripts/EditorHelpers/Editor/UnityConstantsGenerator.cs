﻿// https://github.com/nickgravelyn/UnityToolbag
/*
The MIT License (MIT)

Copyright (c) 2017, Nick Gravelyn

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
/*
UnityConstants is a code generator that produces a single file called UnityConstants.cs containing values for items you may modify in the editor. This tool currently generates constants for the following:

Tags
Sorting Layers
Layers
Scenes in the build configuration
You can run this tool by choosing Generate UnityConstants.cs from the Edit menu. When you first run the command it will prompt you for a directory in which to save UnityConstants.cs. Each time after this, it will find that file and replace it, making it very quick to re-run the process.

This is an example of what you'll see in UnityConstants.cs:

namespace UnityConstants
{
    public static class Tags
    {
        public const string Untagged = "Untagged";
        public const string Respawn = "Respawn";
        public const string Finish = "Finish";
        public const string EditorOnly = "EditorOnly";
        public const string MainCamera = "MainCamera";
        public const string Player = "Player";
        public const string GameController = "GameController";
        public const string Test = "Test";
        public const string AnotherTest = "Another Test";
        public const string _3Testing = "3 Testing";
    }

    public static class SortingLayers
    {
        public const int Default = 0;
        public const int Second = 1;
        public const int NewLayer3 = 3;
        public const int Another = 2;
        public const int NewLayer5 = 5;
        public const int Foreground = 7;
    }

    public static class Layers
    {
        public const int Default = 0;
        public const int TransparentFX = 1;
        public const int IgnoreRaycast = 2;
        public const int Water = 4;
        public const int Layer8 = 8;
        public const int Layer12 = 12;
    }

    public static class Scenes
    {
        public const int TestScene = 0;
        public const int TestScene2 = 1;
        public const int GameSaveSystemDemo = 2;
    }
}
*/

using System.Text.RegularExpressions;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using UnityEditor;

namespace FDZ.EditorHelpers
{
    public static class UnityConstantsGenerator
    {
        [MenuItem("Edit/Generate UnityConstants.cs")]
        public static void Generate()
        {
            // Try to find an existing file in the project called "UnityConstants.cs"
            string filePath = string.Empty;
            foreach (var file in Directory.GetFiles(Application.dataPath, "*.cs", SearchOption.AllDirectories)) {
                if (Path.GetFileNameWithoutExtension(file) == "UnityConstants") {
                    filePath = file;
                    break;
                }
            }

            // If no such file exists already, use the save panel to get a folder in which the file will be placed.
            if (string.IsNullOrEmpty(filePath)) {
                string directory = EditorUtility.OpenFolderPanel("Choose location for UnityConstants.cs", Application.dataPath, "");

                // Canceled choose? Do nothing.
                if (string.IsNullOrEmpty(directory)) {
                    return;
                }

                filePath = Path.Combine(directory, "UnityConstants.cs");
            }

            // Write out our file
            using (var writer = new StreamWriter(filePath)) {
                writer.WriteLine("// This file is auto-generated. Modifications are not saved.");
                writer.WriteLine();
                writer.WriteLine("namespace UnityConstants");
                writer.WriteLine("{");

                // Write out the tags
                writer.WriteLine("    public static class Tags");
                writer.WriteLine("    {");
                foreach (var tag in UnityEditorInternal.InternalEditorUtility.tags) {
                    writer.WriteLine("        /// <summary>");
                    writer.WriteLine("        /// Name of tag '{0}'.", tag);
                    writer.WriteLine("        /// </summary>");
                    writer.WriteLine("        public const string {0} = \"{1}\";", MakeSafeForCode(tag), tag);
                }
                writer.WriteLine("    }");
                writer.WriteLine();

                // Write out sorting layers
                writer.WriteLine("    public static class SortingLayers");
                writer.WriteLine("    {");
                foreach (var layer in SortingLayer.layers) {
                    writer.WriteLine("        /// <summary>");
                    writer.WriteLine("        /// ID of sorting layer '{0}'.", layer.name);
                    writer.WriteLine("        /// </summary>");
                    writer.WriteLine("        public const int {0} = {1};", MakeSafeForCode(layer.name), layer.id);
                }
                writer.WriteLine("    }");
                writer.WriteLine();

                // Write out layers
                writer.WriteLine("    public static class Layers");
                writer.WriteLine("    {");
                for (int i = 0; i < 32; i++) {
                    string layer = UnityEditorInternal.InternalEditorUtility.GetLayerName(i);
                    if (!string.IsNullOrEmpty(layer)) {
                        writer.WriteLine("        /// <summary>");
                        writer.WriteLine("        /// Index of layer '{0}'.", layer);
                        writer.WriteLine("        /// </summary>");
                        writer.WriteLine("        public const int {0} = {1};", MakeSafeForCode(layer), i);
                    }
                }
                writer.WriteLine();
                for (int i = 0; i < 32; i++) {
                    string layer = UnityEditorInternal.InternalEditorUtility.GetLayerName(i);
                    if (!string.IsNullOrEmpty(layer)) {
                        writer.WriteLine("        /// <summary>");
                        writer.WriteLine("        /// Bitmask of layer '{0}'.", layer);
                        writer.WriteLine("        /// </summary>");
                        writer.WriteLine("        public const int {0}Mask = 1 << {1};", MakeSafeForCode(layer), i);
                    }
                }
                writer.WriteLine("    }");
                writer.WriteLine();

                // Write out scenes
                writer.WriteLine("    public static class Scenes");
                writer.WriteLine("    {");
                int sceneIndex = 0;
                foreach (var scene in EditorBuildSettings.scenes) {
                    if (!scene.enabled) {
                        continue;
                    }

                    var sceneName = Path.GetFileNameWithoutExtension(scene.path);

                    writer.WriteLine("        /// <summary>");
                    writer.WriteLine("        /// ID of scene '{0}'.", sceneName);
                    writer.WriteLine("        /// </summary>");
                    writer.WriteLine("        public const int {0} = {1};", MakeSafeForCode(sceneName), sceneIndex);

                    sceneIndex++;
                }
                writer.WriteLine("    }");
                writer.WriteLine();

                // Write out Input axes
                writer.WriteLine("    public static class Axes");
                writer.WriteLine("    {");
                var axes = new HashSet<string>();
                var inputManagerProp = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0]);
                foreach (SerializedProperty axe in inputManagerProp.FindProperty("m_Axes")) {
                    var name = axe.FindPropertyRelative("m_Name").stringValue;
                    var variableName = MakeSafeForCode(name);
                    if (!axes.Contains(variableName)) {
                        writer.WriteLine("        /// <summary>");
                        writer.WriteLine("        /// Input axis '{0}'.", name);
                        writer.WriteLine("        /// </summary>");
                        writer.WriteLine("        public const string {0} = \"{1}\";", variableName, name);
                        axes.Add(variableName);
                    }
                }
                writer.WriteLine("    }");

                // End of namespace UnityConstants
                writer.WriteLine("}");
                writer.WriteLine();
            }

            // Refresh
            AssetDatabase.Refresh();
        }

        private static string MakeSafeForCode(string str)
        {
            str = Regex.Replace(str, "[^a-zA-Z0-9_]", "_", RegexOptions.Compiled);
            if (char.IsDigit(str[0])) {
                str = "_" + str;
            }
            return str;
        }
    }
}