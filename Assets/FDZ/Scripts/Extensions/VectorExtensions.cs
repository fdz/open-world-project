﻿using UnityEngine;

namespace FDZ
{
	public static class VectorExtensions
	{
        // Vector3
		public static Vector3 xoz(this Vector3 self) => new Vector3(self.x, 0f, self.z);
		public static Vector2 xz(this Vector3 self) => new Vector2(self.x, self.z);
		public static Vector2Int xzInt(this Vector3 self) => new Vector2Int((int)self.x, (int)self.z);
		public static Vector2Int xzIntRound(this Vector3 self) => new Vector2Int(Mathf.RoundToInt(self.x), Mathf.RoundToInt(self.z));

        // Vector2Int
        public static Vector3 xoy(this Vector2Int self) => new Vector3(self.x, 0f, self.y);

        // Vector2
		public static Vector3 xoy(this Vector2 self) => new Vector3(self.x, 0f, self.y);
        public static Vector2 Rotate(this Vector2 v, float degrees)
        {
            float radians = degrees * Mathf.Deg2Rad;
            float sin = Mathf.Sin(radians);
            float cos = Mathf.Cos(radians);

            float tx = v.x;
            float ty = v.y;

            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }
    }
}