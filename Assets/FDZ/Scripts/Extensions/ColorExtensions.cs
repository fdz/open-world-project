﻿using UnityEngine;

namespace FDZ
{
	public static class MoreColors
	{
		public static readonly Color orange = new Color(1f, .5f, 0f);
	}

	public static partial class Exntensions
	{
		public static Color GetDarker(this Color self, float amount)
		{
			return new Color(self.r - amount, self.g - amount, self.b - amount, self.a);
		}

		public static Color GetLighter(this Color self, float amount)
		{
			return new Color(self.r + amount, self.g + amount, self.b + amount, self.a);
		}

		public static Color GetWithAlpha(this Color self, float alpha)
		{
			return new Color(self.r, self.g, self.b, alpha);
		}

		public static Color GetWithGreen(this Color self, float green)
		{
			return new Color(self.r, green, self.b);
		}
	}
} // end of namespace
