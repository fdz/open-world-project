﻿using UnityEngine.Audio;
namespace FDZ
{
	public static partial class Extensions
	{
		public static float GetFloat(this AudioMixer self, string key)
		{
			self.GetFloat(key, out var value);
			return value;
		}
	} // end of class
} // end of namespace
