﻿using UnityEngine;
namespace FDZ
{
	public static class CapsuleColliderExtensions
    {
        public static Vector3 GetMainAxis(this CapsuleCollider capsuleCollider)
        {
            switch (capsuleCollider.direction)
            {
                case 0: // X
                    return capsuleCollider.transform.right;
                case 1: // Y
                    return capsuleCollider.transform.up;
                case 2: // Z
                    return capsuleCollider.transform.forward;
            }
            return capsuleCollider.transform.up;
        }

        public static float GetLossyHeight(this CapsuleCollider capsuleCollider)
        {
            switch (capsuleCollider.direction)
            {
                case 0: // X
                    return capsuleCollider.height * capsuleCollider.transform.lossyScale.x;
                case 1: // Y
                    return capsuleCollider.height * capsuleCollider.transform.lossyScale.y;
                case 2: // Z
                    return capsuleCollider.height * capsuleCollider.transform.lossyScale.z;
            }
            return capsuleCollider.height;
        }

        public static float GetLossyRadius(this CapsuleCollider capsuleCollider)
        {
            // https://answers.unity.com/questions/634320/how-to-get-correct-scale-for-circlecollider2d-radi.html
            switch (capsuleCollider.direction)
            {
                case 0: // X
                    return capsuleCollider.radius * Mathf.Max(capsuleCollider.transform.lossyScale.y, capsuleCollider.transform.lossyScale.z);
                case 1: // Y
                    return capsuleCollider.radius * Mathf.Max(capsuleCollider.transform.lossyScale.x, capsuleCollider.transform.lossyScale.z);
                case 2: // Z
                    return capsuleCollider.radius * Mathf.Max(capsuleCollider.transform.lossyScale.x, capsuleCollider.transform.lossyScale.y);
            }
            return capsuleCollider.radius;
        }
    }
}