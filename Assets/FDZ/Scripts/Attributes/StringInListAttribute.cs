﻿using System;
using UnityEngine;
namespace FDZ.Attributes
{
	/// <summary>
	/// https://gist.github.com/ProGM/9cb9ae1f7c8c2a4bd3873e4df14a6687
	/// [StringInList("Cat", "Dog")] public string Animal;
	/// [StringInList(typeof(PropertyDrawersHelper), "AllSceneNames")] public string SceneName; 
	/// </summary>
	public class StringInListAttribute : PropertyAttribute
	{
		public delegate string[] GetStringList();

		public StringInListAttribute(params string[] list)
		{
			List = list;
		}

		public StringInListAttribute(Type type, string methodName)
		{
			var method = type.GetMethod(methodName);
			if (method != null)
			{
				List = method.Invoke(null, null) as string[];
			}
			else
			{
				Debug.LogError("NO SUCH METHOD " + methodName + " FOR " + type);
			}
		}

		public string[] List
		{
			get;
			private set;
		}
	}
}
// 	// EXAMPLE
// 	// MyBehavior.CS
// 	public class MyBehavior : MonoBehaviour
// 	{
// 		// This will store the string value
// 		[StringInList("Cat", "Dog")] public string Animal;
// 		// This will store the index of the array value
// 		[StringInList("John", "Jack", "Jim")] public int PersonID;
// 		// Showing a list of loaded scenes
// 		[StringInList(typeof(PropertyDrawersHelper), "AllSceneNames")] public string SceneName;
// 	}
// 	// PropertyDrawersHelper.cs
// 	public static class PropertyDrawersHelper
// 	{
// #if UNITY_EDITOR
// 		public static string[] AllSceneNames()
// 		{
// 			var temp = new System.Collections.Generic.List<string>();
// 			foreach (UnityEditor.EditorBuildSettingsScene S in UnityEditor.EditorBuildSettings.scenes)
// 			{
// 				if (S.enabled)
// 				{
// 					string name = S.path.Substring(S.path.LastIndexOf('/') + 1);
// 					name = name.Substring(0, name.Length - 6);
// 					temp.Add(name);
// 				}
// 			}
// 			return temp.ToArray();
// 		}
// #endif
// 	}