﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FDZ
{
    public static class Utils
    {
        public static Vector3 Round(Vector3 v)
        {
            v.x = (float)System.Math.Round(v.x);
            v.y = (float)System.Math.Round(v.y);
            v.z = (float)System.Math.Round(v.z);
            return v;
        }

        public static Vector3 Round(Vector3 v, int decimals)
        {
            v.x = (float)System.Math.Round(v.x, decimals);
            v.y = (float)System.Math.Round(v.y, decimals);
            v.z = (float)System.Math.Round(v.z, decimals);
            return v;
        }

        public static bool IsWorldPositionInView(Vector3 worldPosition, Camera camera)
        {
            var viewportPosition = camera.WorldToViewportPoint(worldPosition);
            return viewportPosition.x >= 0 && viewportPosition.x <= 1f && viewportPosition.y >= 0 && viewportPosition.y <= 1f;
        }

        /// <summary>
        /// https://forum.unity.com/threads/vector-bilinear-interpolation-of-a-square-grid.205644/
        /// Given a (u,v) coordinate that defines a 2D local position inside a planar quadrilateral, find the
        /// absolute 3D (x,y,z) coordinate at that location.
        ///
        ///  0 <----u----> 1
        ///  a ----------- b    0
        ///  |             |   /|\
        ///  |             |    |
        ///  |             |    v
        ///  |  *(u,v)     |    |
        ///  |             |   \|/
        ///  d------------ c    1
        ///
        /// a, b, c, and d are the vertices of the quadrilateral. They are assumed to exist in the
        /// same plane in 3D space, but this function will allow for some non-planar error.
        ///
        /// Variables u and v are the two-dimensional local coordinates inside the quadrilateral.
        /// To find a point that is inside the quadrilateral, both u and v must be between 0 and 1 inclusive.  
        /// For example, if you send this function u=0, v=0, then it will return coordinate "a".  
        /// Similarly, coordinate u=1, v=1 will return vector "c". Any values between 0 and 1
        /// will return a coordinate that is bi-linearly interpolated between the four vertices.
        /// </summary>
        public static Vector3 QuadLerp(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float u, float v)
        {
            Vector3 abu = Vector3.Lerp(a, b, u);
            Vector3 dcu = Vector3.Lerp(d, c, u);
            return Vector3.Lerp(abu, dcu, v);
        }

        public static Vector2 QuadLerp(Vector2 a, Vector2 b, Vector2 c, Vector2 d, float u, float v)
        {
            Vector3 abu = Vector2.Lerp(a, b, u);
            Vector3 dcu = Vector2.Lerp(d, c, u);
            return Vector2.Lerp(abu, dcu, v);
        }

        public static float QuadLerp(float a, float b, float c, float d, float u, float v)
        {
            float abu = Mathf.Lerp(a, b, u);
            float dcu = Mathf.Lerp(d, c, u);
            return Mathf.Lerp(abu, dcu, v);
        }

        public static void GetAxes(Vector3 up, out Vector3 right, out Vector3 forward)
        {
            right = new Vector3(up.y, up.z, up.x);
            forward = Vector3.Cross(up, right);
        }

        public static int RoundToEvenInt(float value)
        {
            return (int)(value * 0.5f) * 2;
        }

        /// <summary>
        /// https://gamedev.stackexchange.com/questions/111100/intersection-of-a-line-and-a-rectangle
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <param name="intersetion"></param>
        /// <returns></returns>
        public static bool LineLineIntersection(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 intersection)
        {
            float x1 = a1.x;
            float y1 = a1.y;
            float x2 = a2.x;
            float y2 = a2.y;

            float x3 = b1.x;
            float y3 = b1.y;
            float x4 = b2.x;
            float y4 = b2.y;

            float denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
            if (denominator == 0)
            {
                intersection = new Vector2();
                return false;
            }

            float xNominator = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
            float yNominator = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);

            float px = xNominator / denominator;
            float py = yNominator / denominator;

            intersection = new Vector2(px, py);
            return true;
        }


        #region compare floats
        // https://stackoverflow.com/questions/17333/what-is-the-most-effective-way-for-float-and-double-comparison

        public static bool GreatherThan(float a, float b, float epsilon = float.Epsilon)
        {
            return ApproximatelyEqual(a, b, epsilon) || DefinitelyGreaterThan(a, b, epsilon);
        }
        public static bool LessThan(float a, float b, float epsilon = float.Epsilon)
        {
            return ApproximatelyEqual(a, b, epsilon) || DefinitelyLessThan(a, b, epsilon);
        }
        public static bool ApproximatelyEqual(float a, float b, float epsilon = float.Epsilon)
        {
            return Mathf.Abs(a - b) <= ((Mathf.Abs(a) < Mathf.Abs(b) ? Mathf.Abs(b) : Mathf.Abs(a)) * epsilon);
        }

        public static bool EssentiallyEqual(float a, float b, float epsilon = float.Epsilon)
        {
            return Mathf.Abs(a - b) <= ((Mathf.Abs(a) > Mathf.Abs(b) ? Mathf.Abs(b) : Mathf.Abs(a)) * epsilon);
        }

        public static bool DefinitelyGreaterThan(float a, float b, float epsilon = float.Epsilon)
        {
            return (a - b) > ((Mathf.Abs(a) < Mathf.Abs(b) ? Mathf.Abs(b) : Mathf.Abs(a)) * epsilon);
        }

        public static bool DefinitelyLessThan(float a, float b, float epsilon = float.Epsilon)
        {
            return (b - a) > ((Mathf.Abs(a) < Mathf.Abs(b) ? Mathf.Abs(b) : Mathf.Abs(a)) * epsilon);
        }
        #endregion

        /// <summary>
        /// curSize = Remap(curTime, startTime, endTime, startSize, endSize);
        /// curRedValue = Remap(curTime, startTime, endTime, 0, 255);
        /// </summary>
        public static float Remap(float x, float aMin, float aMax, float bMin, float bMax)
        {
            return (x - aMin) / (aMax - aMin) * (bMax - bMin + bMin);
        }

        public static Vector3 EulerNormalize(Vector3 eulerAngles)
        {
            // keep pitch between -89 and 89
            if (eulerAngles.x > 89)
            {
                eulerAngles.x = 89;
            }
            if (eulerAngles.x < -89)
            {
                eulerAngles.x = -89;
            }

            // keep yaw between -180 and 180
            while (eulerAngles.y < -180)
            {
                eulerAngles.y += 360;
            }
            while (eulerAngles.y > 180)
            {
                eulerAngles.y -= 360;
            }

            return eulerAngles;
        }

        public static Vector3 Vector3RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle)
        {
            // https://answers.unity.com/questions/47115/vector3-rotate-around.html
            return angle * (point - pivot) + pivot;
        }

        /// <summary>
        /// https://forum.unity.com/threads/solved-how-to-get-rotation-value-that-is-in-the-inspector.460310/
        /// converting to +/- 180:
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float WrapAngle(float angle)
        {
            angle %= 360;
            if (angle > 180)
                return angle - 360;
            return angle;
        }

        /// <summary>
        /// https://forum.unity.com/threads/solved-how-to-get-rotation-value-that-is-in-the-inspector.460310/
        /// converting to +/- 180:
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static float UnwrapAngle(float angle)
        {
            if (angle >= 0)
                return angle;
            angle = -angle % 360;
            return 360 - angle;
        }

        public static float QuaternionSingedAngle(Quaternion a, Quaternion b)
        {
            // https://answers.unity.com/questions/26783/how-to-get-the-signed-angle-between-two-quaternion.html
            // get a "forward vector" for each rotation
            var fromRotation = a * Vector3.forward;
            var toRortation = b * Vector3.forward;
            // get a numeric angle for each vector, on the X-Z plane (relative to world forward)
            var angleA = Mathf.Atan2(fromRotation.x, fromRotation.z) * Mathf.Rad2Deg;
            var angleB = Mathf.Atan2(toRortation.x, toRortation.z) * Mathf.Rad2Deg;
            // get the signed difference in these angles
            return Mathf.DeltaAngle(angleA, angleB);
        }

        public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
        {
            transform.localScale = Vector3.one;
            transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
        }

        public static void EventSystem_RaycastAll(Vector3 position, List<RaycastResult> results)
        {
            // https://answers.unity.com/questions/1009987/detect-canvas-object-under-mouse-because-only-some.html
            PointerEventData pointerData = new PointerEventData(EventSystem.current) { pointerId = -1 };
            pointerData.position = position;
            EventSystem.current.RaycastAll(pointerData, results);
        }

        public static Vector2 RotatePoint(Vector2 point, Vector2 center, float angle)
        {
            angle = (angle) * (Mathf.PI / 180f); // Convert to radians
            var rotatedX = Mathf.Cos(angle) * (point.x - center.x) - Mathf.Sin(angle) * (point.y - center.y) + center.x;
            var rotatedY = Mathf.Sin(angle) * (point.x - center.x) + Mathf.Cos(angle) * (point.y - center.y) + center.y;
            return new Vector2(rotatedX, rotatedY);
        }

        public static float SignedAngle(Vector3 a, Vector3 b)
        {
            // https://answers.unity.com/questions/181867/is-there-way-to-find-a-negative-angle.html
            var angle = Vector3.Angle(a, b);
            var cross = Vector3.Cross(a, b);
            if (cross.y < 0)
                angle = -angle;
            return angle;
        }

        public static int NearestMultiple(float value, float factor)
        {
            return (int)(System.Math.Round((value / (double)factor), System.MidpointRounding.AwayFromZero) * factor);
        }

        public static Vector3 Abs(Vector3 v3)
        {
            return new Vector3(Mathf.Abs(v3.x), Mathf.Abs(v3.y), Mathf.Abs(v3.z));
        }

        public static bool IsInsideSurfaceArc2D(Vector2 arcPosition, Vector2 arcUp, Vector2 otherPosition, float arcAngle)
        {
            var direction = (otherPosition - arcPosition);
            var angle = Vector2.Angle(arcUp, direction) * 2;
            // Debug.Log(angle);
            return angle < arcAngle;
        }

        public static Vector2 Vector2RotateAroundPoint(Vector2 point, Vector2 center, float angle)
        {
            // https://www.gamefromscratch.com/post/2012/11/24/GameDev-math-recipes-Rotating-one-point-around-another-point.aspx
            angle = (angle) * (Mathf.PI / 180); // Convert to radians
            var rotatedX = Mathf.Cos(angle) * (point.x - center.x) - Mathf.Sin(angle) * (point.y - center.y) + center.x;
            var rotatedY = Mathf.Sin(angle) * (point.x - center.x) + Mathf.Cos(angle) * (point.y - center.y) + center.y;
            return new Vector2(rotatedX, rotatedY);
        }

        public static Vector3 AverageVector3(IEnumerable<Vector3> vectors)
        {
            var addedVector = Vector3.zero;
            int vectorCount = 0;
            foreach (var vector in vectors)
            {
                vectorCount++;
                addedVector += vector;
            }
            return addedVector / vectorCount;
        }

        public static RaycastHit AverageRaycastHit(IEnumerable<RaycastHit> hits, System.Func<RaycastHit, bool> validateHit = null)
        {
            var addedPoint = Vector3.zero;
            var addedNormal = Vector3.zero;
            int hitCount = 0;
            foreach (var hit in hits)
            {
                hitCount++;
                if (validateHit == null || validateHit.Invoke(hit))
                {
                    addedPoint += hit.point;
                    addedNormal += hit.normal;
                }
            }
            var averageHit = new RaycastHit();
            averageHit.point = addedPoint / hitCount;
            averageHit.normal = addedNormal / hitCount;
            return averageHit;
        }
    } // end of class
} // end of namespace
