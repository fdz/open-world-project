﻿namespace FDZ
{
	// http://www.alanzucconi.com/2015/07/26/enum-flags-and-bitwise-operators/
	public static class BitMask
	{
		public static int ToMask(this int self)
		{
			return (1 << self);
		}

		// INT

		public static int Set(int a, int b)
		{
			return a | b;
		}

		public static int Unset(int a, int b)
		{
			return a & (~b);
		}

		public static bool Has(int a, int b)
		{
			return (a & b) == b;
		}

		public static int Toggle(int a, int b)
		{
			return a ^ b;
		}

		// ENUMS

		public static T Set<T>(T a, T b) where T : System.Enum
		{
			int setted = Set((int)(object)a, (int)(object)b);
			object obj = System.Enum.ToObject(typeof(T), setted);
			return (T)obj;
		}

		public static T Unset<T>(T a, T b) where T : System.Enum
		{
			int unsetted = Unset((int)(object)a, (int)(object)b);
			object obj = System.Enum.ToObject(typeof(T), unsetted);
			return (T)obj;
		}

		public static bool Has<T>(T a, T b) where T : System.Enum
		{
			return Has((int)(object)a, (int)(object)b);
		}

		public static T Toggle<T>(T a, T b) where T : System.Enum
		{
			int toggled = Toggle((int)(object)a, (int)(object)b);
			object obj = System.Enum.ToObject(typeof(T), toggled);
			return (T)obj;
		}
	} // end of class
} // end of namespace
