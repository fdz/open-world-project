﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
    public static class Physics3DExt
    {
        //public static bool RaycastAround(float radius, int rayCount, Vector3 center, Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction)
        public static List<RaycastHit> RaycastAroundAll(float radius, int rayCount, Vector3 center, Vector3 direction, float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction, System.Func<RaycastHit, bool> validateHit = null)
        {
            var hits = new List<RaycastHit>();
            direction.Normalize();
            //Debug.DrawRay(center, direction, Color.magenta);
            Vector3 perpendicularDirection = Vector3.Cross(direction, Vector3.right).normalized; // https://answers.unity.com/questions/1333667/perpendicular-to-a-3d-direction-vector.html
            float angleStep = 360f / rayCount;
            for (float angle = 0; angle < 360f; angle += angleStep)
            {
                Vector3 origin = center + Quaternion.AngleAxis(angle, direction) * perpendicularDirection * radius; // Rotate around point, similar to https://answers.unity.com/questions/532297/rotate-a-vector-around-a-certain-point.html
                //Debug.DrawLine(center, origin, Color.magenta);
                //Debug.DrawRay(origin, direction * maxDistance, Color.red);
                if (Physics.Raycast(origin, direction, out RaycastHit hitInfo, maxDistance, layerMask, queryTriggerInteraction))
                {
                    if (validateHit == null || validateHit.Invoke(hitInfo))
                    {
                        hits.Add(hitInfo);
                        Debug.DrawRay(hitInfo.point, hitInfo.normal, Color.blue);
                    }
                }
            }
            return hits;

        }
    }
}