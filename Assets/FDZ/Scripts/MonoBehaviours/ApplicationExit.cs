﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public class ApplicationExit : MonoBehaviour
	{
		public KeyCode keyCode = KeyCode.Escape;

		private void Update()
		{
			if (Input.GetKeyDown(keyCode))
			{
				Application.Quit();
			}
		}
	} // end of class
} // end of namespace