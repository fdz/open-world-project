﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
    /// <summary>
    /// Reference: https://wiki.unity3d.com/index.php/FramesPerSecond
    /// </summary>
    public class FPSDisplay : MonoBehaviour
    {
        public KeyCode keyCode_clearMinMax = KeyCode.F11;
        public UnityEngine.UI.Text text;

        float deltaTime = 0.0f;

        float msecLowest = float.MaxValue;
        float fpsLowest = float.MaxValue;

        float msecHighest = float.MinValue;
        float fpsHighest = float.MinValue;

        void Update()
        {
            deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        }

        public void ClearMinMax()
        {
            msecLowest = float.MaxValue;
            fpsLowest = float.MaxValue;
            msecHighest = float.MinValue;
            fpsHighest = float.MinValue;
        }

        private void LateUpdate()
        {
            float msec = deltaTime * 1000.0f;
            float fps = 1.0f / deltaTime;

            if (msec < msecLowest)
            {
                msecLowest = msec;
            }
            else if (msec > msecHighest)
            {
                msecHighest = msec;
            }

            if (fps < fpsLowest)
            {
                fpsLowest = fps;
            }
            else
            {
                fpsHighest = fps;
            }

            string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            text += string.Format("\nL: {0:0.0} ms ({1:0.} fps)", msecLowest, fpsLowest);
            text += string.Format("\nH: {0:0.0} ms ({1:0.} fps)", msecHighest, fpsHighest);
            text += $"\nPress {keyCode_clearMinMax} to clear L and H";

            this.text.text = text;

            if (Input.GetKeyDown(keyCode_clearMinMax))
            {
                ClearMinMax();
            }
        }
    } // end of class
} // end of namespace