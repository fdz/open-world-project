﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
    public class FPSTarget : MonoBehaviour
    {
        [Range(0, 4)] public int vSyncCount = 1;
        [Range(0, 300)] public int targetFrameRate = 60;

        void Start()
        {
            vSyncCount = QualitySettings.vSyncCount = Mathf.Clamp(vSyncCount, 0, 4);
            Application.targetFrameRate = targetFrameRate;
        }
    }
}