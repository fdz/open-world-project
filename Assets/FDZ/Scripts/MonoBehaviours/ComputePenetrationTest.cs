﻿// This uses ALINE
/*
using Drawing;
using UnityEngine;

namespace FDZ
{
	public class ComputePenetrationTest : MonoBehaviourGizmos
	{
        public float distanceA;
        public float distanceB;
        public int iterationsB;

        public override void DrawGizmos()
        {
            var collider = GetComponent<SphereCollider>();
            var position = transform.position;
            var radius = collider.radius;

            Draw.WireSphere(position, radius, Color.green);

            var colliders = Physics.OverlapSphere(position, radius);

            var newPositionA = position;
            foreach (var otherCollider in colliders)
            {
                if (otherCollider != collider &&
                    Physics.ComputePenetration(
                        collider, newPositionA, Quaternion.identity,
                        otherCollider, otherCollider.transform.position, otherCollider.transform.rotation,
                        out Vector3 direction , out float distance))
                {
                    newPositionA += direction * distance;
                    Draw.Ray(position - direction * radius, direction * distance, Color.red);
                }
            }
            Draw.WireSphere(newPositionA, radius, Color.red);
            distanceA = (position - newPositionA).magnitude;


            var newPositionB = position;
            foreach (var otherCollider in colliders)
            {
                if (otherCollider == collider)
                {
                    continue;
                }
                int iterations = 0;
                bool first = true;
                while (Physics.ComputePenetration(
                        collider, newPositionB, Quaternion.identity,
                        otherCollider, otherCollider.transform.position, otherCollider.transform.rotation,
                        out Vector3 direction, out float distance))
                {
                    var angle = Vector3.Angle(direction, Vector3.up);
                    if (angle >= 45f) // is wall
                    {
                        newPositionB += direction * distance;
                        continue;
                    }
                    if (first)
                    {
                        newPositionB.y += 0.001f;
                    }
                    else
                    {
                        newPositionB.y += distance;
                    }
                    first = false;
                    iterations++;
                }
                iterationsB = iterations;
            }
            Draw.WireSphere(newPositionB, radius, Color.magenta);
            distanceB = (position - newPositionB).magnitude;
        }
    }
}
*/