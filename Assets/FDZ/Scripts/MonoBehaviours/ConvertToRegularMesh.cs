﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace FDZ
{
	// Sebastian Lague: INTEGRATION 02 - Making an RPG in Unity (E08)
	// https://www.youtube.com/watch?v=ZBLvKR2E62Q&list=PLPV2KyIb3jR4KLGCCAciWQ5qHudKtYeP7&index=10&t=1215s
	public class ConvertToRegularMesh : MonoBehaviour
	{
		[ContextMenu("Convert to regular mesh")]
		void Convert()
		{
			var skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
			var meshRenderer = gameObject.AddComponent<MeshRenderer>();
			var meshFilter = gameObject.AddComponent<MeshFilter>();
			meshFilter.sharedMesh = skinnedMeshRenderer.sharedMesh;
			meshRenderer.sharedMaterials = skinnedMeshRenderer.sharedMaterials;
			GameObject.DestroyImmediate(skinnedMeshRenderer);
			GameObject.DestroyImmediate(this);
		}
	} // end of class
} // end of namespace
