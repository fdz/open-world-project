﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
	public static class UtilsInput
    {
        public static float GetAxis(KeyCode negative, KeyCode positive)
        {
            var axis = 0f;
            if (Input.GetKey(negative))
            {
                axis -= 1f;
            }
            if (Input.GetKey(positive))
            {
                axis += 1f;
            }
            return axis;
        }

        public static float GetAxisDown(KeyCode negative, KeyCode positive)
        {
            var axis = 0f;
            if (Input.GetKeyDown(negative))
            {
                axis -= 1f;
            }
            if (Input.GetKeyDown(positive))
            {
                axis += 1f;
            }
            return axis;
        }

        public static Vector2 GetAxis2D(KeyCode left, KeyCode right, KeyCode back, KeyCode forward)
        {
            var direction = new Vector2();
            if (Input.GetKey(left))
            {
                direction.x -= 1f;
            }
            if (Input.GetKey(right))
            {
                direction.x += 1f;
            }
            if (Input.GetKey(back))
            {
                direction.y -= 1f;
            }
            if (Input.GetKey(forward))
            {
                direction.y += 1f;
            }
            return direction.normalized;
        }

        public static Vector2 GetWASD()
        {
            return GetAxis2D(KeyCode.A, KeyCode.D, KeyCode.S, KeyCode.W);
        }

        public static Vector3 GetMoveDirectionFromMainCamera()
        {
            var direction = GetWASD();
            var cameraTransform = Camera.main.transform;
            var right = new Vector3(cameraTransform.right.x, 0f, cameraTransform.right.z).normalized;
            var forward = new Vector3(cameraTransform.forward.x, 0f, cameraTransform.forward.z).normalized;
            return (right * direction.x + forward * direction.y).normalized;
        }
    }
}