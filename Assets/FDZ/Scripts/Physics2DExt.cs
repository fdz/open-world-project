﻿using System.Collections.Generic;

namespace UnityEngine
{
	public static class Physics2DExt
	{
        public static List<RaycastHit2D> MultiRaycast(Vector2 origin, Vector2 direction, float distance, int layerMask, float width, int rayCount)
        {
            var halfWidth = width / 2;
            var step = width / rayCount;
            var offset = 0f;
            var perpendicularDirection = Vector2.Perpendicular(direction);
            List<RaycastHit2D> hits = new List<RaycastHit2D>();
            while (offset <= halfWidth)
            {
                hits.Add(Physics2D.Raycast(origin + perpendicularDirection * offset, direction, distance, layerMask));
                // Debug.DrawRay(origin + perpendicularDirection * offset, direction * distance, Color.red);
                if (offset != 0)
                {
                    hits.Add(Physics2D.Raycast(origin + perpendicularDirection * -offset, direction, distance, layerMask));
                    // Debug.DrawRay(origin + perpendicularDirection * -offset, direction * distance, Color.red);
                }
                offset += step;
            }
            return hits;
        }
	} // end of class
} // end of namespace
