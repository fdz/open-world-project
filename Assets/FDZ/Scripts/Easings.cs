﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ
{
    public enum EaseStyle
    {
        InSine,
        OutSine,
        InOutSine,

        InCubic,
        OutCubic,
        InOutCubic,

        InQuint,
        OutQuint,
        InOutQuint,

        InCirc,
        OutCirc,
        InOutCirc,

        InElastic,
        OutElastic,
        InOutElastic,

        InQuad,
        OutQuad,
        InOutQuad,

        InQuart,
        OutQuart,
        InOutQuart,

        InExpo,
        OutExpo,
        InOutExpo,

        InBack,
        OutBack,
        InOutBack,

        InBounce,
        OutBounce,
        InOutBounce,
    }

    /// <summary>
    /// https://easings.net
    /// functions assume that x goes from 0 to 1
    /// </summary>
    public static class Easings
    {
        /// <summary>
        /// Useful for doint stuff with the editor
        /// functions assume that x goes from 0 to 1
        /// </summary>
        public static void Ease(float x, EaseStyle style)
        {
            switch (style)
            {
                case EaseStyle.InSine: InSine(x); break;
                case EaseStyle.OutSine: OutSine(x); break;
                case EaseStyle.InOutSine: InOutSine(x); break;

                case EaseStyle.InCubic: InCubic(x); break;
                case EaseStyle.OutCubic: OutCubic(x); break;
                case EaseStyle.InOutCubic: InOutCubic(x); break;

                case EaseStyle.InQuint: InQuint(x); break;
                case EaseStyle.OutQuint: OutQuint(x); break;
                case EaseStyle.InOutQuint: InOutQuint(x); break;

                case EaseStyle.InCirc: InCirc(x); break;
                case EaseStyle.OutCirc: OutCirc(x); break;
                case EaseStyle.InOutCirc: InOutCirc(x); break;

                case EaseStyle.InElastic: InElastic(x); break;
                case EaseStyle.OutElastic: OutElastic(x); break;
                case EaseStyle.InOutElastic: InOutElastic(x); break;

                case EaseStyle.InQuad: InQuad(x); break;
                case EaseStyle.OutQuad: OutQuad(x); break;
                case EaseStyle.InOutQuad: InOutQuad(x); break;

                case EaseStyle.InQuart: InQuart(x); break;
                case EaseStyle.OutQuart: OutQuart(x); break;
                case EaseStyle.InOutQuart: InOutQuart(x); break;

                case EaseStyle.InExpo: InExpo(x); break;
                case EaseStyle.OutExpo: OutExpo(x); break;
                case EaseStyle.InOutExpo: InOutExpo(x); break;

                case EaseStyle.InBack: InBack(x); break;
                case EaseStyle.OutBack: OutBack(x); break;
                case EaseStyle.InOutBack: InOutBack(x); break;

                case EaseStyle.InBounce: InBounce(x); break;
                case EaseStyle.OutBounce: OutBounce(x); break;
                case EaseStyle.InOutBounce: InOutBounce(x); break;
            }
        }

        #region Sine

        public static float InSine(float x)
        {
            return 1 - Mathf.Cos((x * Mathf.PI) / 2);
        }

        public static float OutSine(float x)
        {
            return Mathf.Sin((x * Mathf.PI) / 2);
        }

        public static float InOutSine(float x)
        {
            return -(Mathf.Cos(Mathf.PI * x) - 1) / 2;
        }

        #endregion

        #region Cubic

        public static float InCubic(float x)
        {
            return x * x * x;
        }

        public static float OutCubic(float x)
        {
            return 1 - Mathf.Pow(1 - x, 3);
        }

        public static float InOutCubic(float x)
        {
            return x < 0.5 ? 4 * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 3) / 2;
        }

        #endregion

        #region Quint

        public static float InQuint(float x)
        {
            return x * x * x * x * x;
        }

        public static float OutQuint(float x)
        {
            return 1 - Mathf.Pow(1 - x, 5);
        }

        public static float InOutQuint(float x)
        {
            return x < 0.5 ? 16 * x * x * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 5) / 2;
        }

        #endregion

        #region Circ

        public static float InCirc(float x)
        {
            return 1 - Mathf.Sqrt(1 - Mathf.Pow(x, 2));
        }

        public static float OutCirc(float x)
        {
            return Mathf.Sqrt(1 - Mathf.Pow(x - 1, 2));
        }

        public static float InOutCirc(float x)
        {
            return x < 0.5
                ? (1 - Mathf.Sqrt(1 - Mathf.Pow(2 * x, 2))) / 2
                : (Mathf.Sqrt(1 - Mathf.Pow(-2 * x + 2, 2)) + 1) / 2;
        }

        #endregion

        #region Elastic

        public static float InElastic(float x)
        {
            const float c4 = (2f * Mathf.PI) / 3f;
            return x <= float.Epsilon // x === 0
                ? 0
                : x >= 1f - float.Epsilon//: x === 1
                ? 1
                : -Mathf.Pow(2, 10 * x - 10) * Mathf.Sin((x * 10f - 10.75f) * c4);
        }

        public static float OutElastic(float x)
        {
            const float c4 = (2f * Mathf.PI) / 3f;
            return x <= float.Epsilon // x === 0
                ? 0
                : x >= 1f - float.Epsilon//: x === 1
                ? 1
                : Mathf.Pow(2, -10 * x) * Mathf.Sin((x * 10f - 0.75f) * c4) + 1;
        }

        public static float InOutElastic(float x)
        {
            const float c5 = (2f * Mathf.PI) / 4.5f;
            return x <= float.Epsilon // x === 0
              ? 0
              : x >= 1f - float.Epsilon//: x === 1
              ? 1
              : x < 0.5
              ? -(Mathf.Pow(2, 20 * x - 10) * Mathf.Sin((20f * x - 11.125f) * c5)) / 2
              : (Mathf.Pow(2, -20 * x + 10) * Mathf.Sin((20f * x - 11.125f) * c5)) / 2 + 1;
        }

        #endregion

        #region Quad

        public static float InQuad(float x)
        {
            return x * x;
        }

        public static float OutQuad(float x)
        {
            return 1 - (1 - x) * (1 - x);
        }

        public static float InOutQuad(float x)
        {
            return x < 0.5 ? 2 * x * x : 1 - Mathf.Pow(-2 * x + 2, 2) / 2;
        }

        #endregion

        #region Quart

        public static float InQuart(float x)
        {
            return x * x * x * x;
        }

        public static float OutQuart(float x)
        {
            return 1 - Mathf.Pow(1 - x, 4);
        }

        public static float InOutQuart(float x)
        {
            return x < 0.5 ? 8 * x * x * x * x : 1 - Mathf.Pow(-2 * x + 2, 4) / 2;
        }

        #endregion

        #region Expo

        public static float InExpo(float x)
        {
            return x <= float.Epsilon // x === 0
            ? 0
            : Mathf.Pow(2, 10 * x - 10);
        }

        public static float OutExpo(float x)
        {
            return x <= float.Epsilon // x === 0
            ? 1
            : 1 - Mathf.Pow(2, -10 * x);
        }

        public static float InOutExpo(float x)
        {
            return x <= float.Epsilon // x === 0
                ? 0
                : x >= 1f - float.Epsilon//: x === 1
                ? 1
                : x < 0.5 ? Mathf.Pow(2, 20 * x - 10) / 2
                : (2 - Mathf.Pow(2, -20 * x + 10)) / 2;
        }

        #endregion

        #region Back

        public static float InBack(float x)
        {
            const float c1 = 1.70158f;
            const float c3 = c1 + 1f;
            return c3 * x * x * x - c1 * x * x;
        }

        public static float OutBack(float x)
        {
            const float c1 = 1.70158f;
            const float c3 = c1 + 1f;
            return 1 + c3 * Mathf.Pow(x - 1, 3) + c1 * Mathf.Pow(x - 1, 2);
        }

        public static float InOutBack(float x)
        {
            const float c1 = 1.70158f;
            const float c2 = c1 * 1.525f;
            return x < 0.5
              ? (Mathf.Pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2
              : (Mathf.Pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
        }

        #endregion

        #region Bounce

        public static float InBounce(float x)
        {
            return 1 - OutBounce(1 - x);
        }

        public static float OutBounce(float x)
        {
            const float n1 = 7.5625f;
            const float d1 = 2.75f;

            if (x < 1 / d1)
            {
                return n1 * x * x;
            }
            else if (x < 2 / d1)
            {
                return n1 * (x -= 1.5f / d1) * x + 0.75f;
            }
            else if (x < 2.5 / d1)
            {
                return n1 * (x -= 2.25f / d1) * x + 0.9375f;
            }
            else
            {
                return n1 * (x -= 2.625f / d1) * x + 0.984375f;
            }
        }

        public static float InOutBounce(float x)
        {
            return x < 0.5
              ? (1 - OutBounce(1 - 2 * x)) / 2
              : (1 + OutBounce(2 * x - 1)) / 2;
        }

        #endregion
    }
}