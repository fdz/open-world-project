﻿using UnityEngine;

namespace FDZ
{
    /// <summary>
    /// [Jorgue Rodriguez] Math For Game Developers
    /// Unity has the class Bounds with functions like Intersects() and IntersectsRay()
    /// </summary>
    public struct AABB
    {
        public Vector3 min;
        public Vector3 max;

        public AABB(Vector3 min, Vector3 max)
        {
            this.min = min;
            this.max = max;
        }

        public static AABB operator +(AABB aabb, Vector3 position)
        {
            aabb.min += position;
            aabb.max += position;
            return aabb;
        }

        public override string ToString()
        {
            return $"({min}, {max})";
        }

        /// <summary>
        /// [Jorgue Rodriguez] Math For Game Developers: Part 15 and 16 bullet collision
        /// </summary>
        public static bool IntersectsLine(AABB aabbBox, Vector3 v0, Vector3 v1, out Vector3 intersection, out float fraction)
        {
            float f_low = 0;
            float f_high = 1;

            if (!ClipLine(0, aabbBox, v0, v1, ref f_low, ref f_high) ||
                !ClipLine(1, aabbBox, v0, v1, ref f_low, ref f_high) ||
                !ClipLine(2, aabbBox, v0, v1, ref f_low, ref f_high))
            {
                intersection = Vector3.zero;
                fraction = 0f;
                return false;
            }

            // The formula for I: http://youtu.be/USjbg5QXk3g?t=6m24s
            // f_low = (p1.y - v0.y) / (v1.y - v0.y)
            Vector3 b = v1 - v0;
            intersection = v0 + b * f_low;
            fraction = f_low;

            return true;
        }

        private static bool ClipLine(int d, AABB aabbBox, Vector3 v0, Vector3 v1, ref float f_low, ref float f_high)
        {
            // f_low and f_high are the results from all clipping so far. We'll write our results back out to those parameters.

            // f_dim_low and f_dim_high are the results we're calculating for this current dimension.
            float f_dim_low, f_dim_high;

            // Find the point of intersection in this dimension only as a fraction of the total vector http://youtu.be/USjbg5QXk3g?t=3m12s
            f_dim_low = (aabbBox.min[d] - v0[d]) / (v1[d] - v0[d]);
            f_dim_high = (aabbBox.max[d] - v0[d]) / (v1[d] - v0[d]);

            // Make sure low is less than high
            if (f_dim_high < f_dim_low)
            {
                var aux = f_dim_high;
                f_dim_high = f_dim_low;
                f_dim_low = aux;
            }

            // If this dimension's high is less than the low we got then we definitely missed. http://youtu.be/USjbg5QXk3g?t=7m16s
            if (f_dim_high < f_low)
                return false;

            // Likewise if the low is less than the high.
            if (f_dim_low > f_high)
                return false;

            // Add the clip from this dimension to the previous results http://youtu.be/USjbg5QXk3g?t=5m32s
            f_low = Mathf.Max(f_dim_low, f_low);
            f_high = Mathf.Min(f_dim_high, f_high);

            if (f_low > f_high)
                return false;

            return true;
        }
    } // calss
} // namespace