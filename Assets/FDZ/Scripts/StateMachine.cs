﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

// // Enum : ValueType, IComparable, IConvertible, IFormattable
// // String : IEnumerable<char>, IEnumerable, ICloneable, IComparable, IComparable<String>, IConvertible, IEquatable<String>
// public class StateMachine<TOwner, TTransition> where TTransition : System.IComparable, System.IConvertible
// {

// }
// public class StateMachineTest
// {
//     enum Transitions { }
//     void Test()
//     {
//         StateMachine<FDZ.Game.Character, Transitions> A;
//         StateMachine<FDZ.Game.Character, string> B;
//     }
// }

/*
public class State<TOwner> : FDZ.IState<TOwner>
{
    public System.Action<TOwner> OnEnterEvent,
    OnExitEvent,
    OnUpdateEvent,
    OnLateUpdateEvent,
    OnFixedUpdateEvent;

    public void OnEnter(TOwner owner)
    {
        OnEnterEvent?.Invoke(owner);
    }

    public void OnExit(TOwner owner)
    {
        OnExitEvent?.Invoke(owner);
    }
    
    public void OnUpdate(TOwner owner)
    {
        OnUpdateEvent?.Invoke(owner);
    }
    
    public void OnFixedUpdate(TOwner owner)
    {
        OnLateUpdateEvent?.Invoke(owner);
    }
    
    public void OnLateUpdate(TOwner owner)
    {
        OnFixedUpdateEvent?.Invoke(owner);
    }
}
*/

namespace FDZ
{
	public interface IState { }

	public static class IStateExtensions
	{
		public static bool Is<T>(this IState self) where T : IState
		{
			return self.GetType() == typeof(T);
		}
	} // end of class

	public interface IState<TOwner> : IState
	{
		void OnEnter(TOwner owner);
		void OnExit(TOwner owner);
		void OnUpdate(TOwner owner);
		void OnFixedUpdate(TOwner owner);
		void OnLateUpdate(TOwner owner);
	} // end of interface

	public class Transitions<TOwner, TState> where TState : IState<TOwner>
	{
		Dictionary<TState, Dictionary<TState, List<System.Func<TOwner, bool>>>> transitions = new Dictionary<TState, Dictionary<TState, List<System.Func<TOwner, bool>>>>();

		// public Dictionary<TState, List<System.Func<TOwner, bool>>> this [TState from]
		// {
		// 	get { return transitions[from]; }
		// }

		private void Init(TState from, TState to)
		{
			if (!transitions.ContainsKey(from))
			{
				transitions.Add(from, new Dictionary<TState, List<System.Func<TOwner, bool>>>());
			}
			if (!transitions[from].ContainsKey(to))
			{
				transitions[from].Add(to, new List<System.Func<TOwner, bool>>());
			}
		}

		public bool ContainsKey(TState state)
		{
			return transitions.ContainsKey(state);
		}

		public void Add(TState from, TState to, System.Func<TOwner, bool> condition)
		{
			Init(from, to);
			transitions[from][to].Add(condition);
		}

		public Dictionary<TState, List<System.Func<TOwner, bool>>> Get(TState from)
		{
			if (transitions.ContainsKey(from) == false)
			{
				return null;
			}
			return transitions[from];
		}
	} // end of class

	public class StateMachine<TOwner, TState> where TState : FDZ.IState<TOwner>
	{
		public Transitions<TOwner, TState> transitions;
		public TState CurrentState { get; private set; }
		public TState PreviousState { get; private set; }
		public float CurrentStateTimer { get; private set; }
		public bool IsActive { get; private set; }

		Dictionary<TState, List<System.Func<TOwner, bool>>> currentStateTransitions;

		public StateMachine()
		{

		}

		public StateMachine(Transitions<TOwner, TState> transitions)
		{
			this.transitions = transitions;
		}

		public bool Begin(TState state, TOwner owner)
		{
			if (state != null && transitions.ContainsKey(state))
			{
				IsActive = true;
				ChangeState(state, owner);
			}
			return IsActive;
		}

		public bool DoTransition(TOwner owner)
		{
			if (IsActive)
			{
				if (currentStateTransitions == null)
				{
					Debug.LogWarning("state machine reached state with no transitions");
					IsActive = false;
					return false;
				}
				foreach (var conditionsList in currentStateTransitions)
				{
					foreach (var condition in conditionsList.Value)
					{
						if (condition.Invoke(owner))
						{
							ChangeState(conditionsList.Key, owner);
							return true;
						}
					}
				}
			}
			return false;
		}

		private void ChangeState(TState to, TOwner owner)
		{
			PreviousState = CurrentState;
			CurrentState = to;
			if (PreviousState != null)
			{
				PreviousState.OnExit(owner);
			}
			CurrentState.OnEnter(owner);
			currentStateTransitions = transitions.Get(CurrentState);
			CurrentStateTimer = 0f;
			// Debug.Log("Changed State from " + PreviousState?.GetType().Name + " to " + CurrentState.GetType().Name);
		}

		public void OnUpdate(TOwner owner)
		{
			if (IsActive)
			{
				CurrentState.OnUpdate(owner);
				CurrentStateTimer += Time.deltaTime;
			}
		}

		public void OnLateUpdate(TOwner owner)
		{
			if (IsActive)
			{
				CurrentState.OnLateUpdate(owner);
			}
		}

		public void OnFixedUpdate(TOwner owner)
		{
			if (IsActive)
			{
				CurrentState.OnFixedUpdate(owner);
			}
		}
	} // end of class
} // end of name space
