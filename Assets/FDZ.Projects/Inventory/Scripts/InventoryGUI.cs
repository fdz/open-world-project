﻿using UnityEngine;
using UnityEngine.UI;

namespace FDZ.Projects.Inventory
{
    public class InventoryGUI : MonoBehaviour
    {
        public const int SLOT_SIZE = 64;
        public const float SUB_INVENTORY_SPACING = 5f;
        public const float ROTATION_SPEED = 12f;
        public const float ZOOM_IN_SPEED = 16f;
        public const float ZOOM_OUT_SPEED = 4f;

        public Inventory inventory;
        [HideInInspector] public bool zoom;

        public Vector3 auxLocalScale = Vector3.one;

        public RectTransform rectTransform { get => transform as RectTransform; }

        public Vector2Int Size { get => inventory.size; }

        private void Awake()
        {
            SetBorderVisibility(false);
        }

        private void LateUpdate()
        {
            if (zoom)
            {
                var itemGUI = gameObject.GetComponentUp<InventoryItemGUI>();
                if (itemGUI != null)
                {
                    itemGUI.transform.SetAsLastSibling();
                }
                var previousParent = transform.parent;
                var temporaryParent = transform.gameObject.GetComponentUp<Canvas>().transform;
                transform.SetParent(temporaryParent);
                transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, Time.deltaTime * ZOOM_IN_SPEED);
                transform.SetParent(previousParent);
            }
            else
            {
                transform.localScale = Vector3.Lerp(transform.localScale, auxLocalScale, Time.deltaTime * ZOOM_OUT_SPEED);
            }
        }

        public void SetBorderVisibility(bool value)
        {
            transform.Find("Border").gameObject.SetActive(value);
        }

        public Vector2Int WorldPositionToTileCoordsToInt(Vector2 position)
        {
            position = WorldPositionToTileCoords(position);
            return new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        }

        public Vector2 WorldPositionToTileCoords(Vector2 position)
        {
            position = transform.InverseTransformPoint(position);
            return new Vector2(
                position.x / SLOT_SIZE + inventory.size.x / 2f,
                -position.y / SLOT_SIZE + inventory.size.y / 2f);
        }

        public void UpdateGUI()
        {
            var transform = this.transform as RectTransform;
            transform.sizeDelta = inventory.size * SLOT_SIZE;
            var background = transform.Find("Background").GetComponent<RawImage>();
            background.uvRect = new Rect(Vector2.zero, inventory.size);
        }

        public bool CanPlace(InventoryItemGUI itemGUI, Vector2Int position, float rotation)
        {
            return inventory.CanPlace(itemGUI.item, position, rotation);
        }

        public bool Place(InventoryItemGUI itemGUI)
        {
            if (inventory.Place(itemGUI.item))
            {
                MakeItemGUIAsChild(itemGUI);
                return true;
            }
            return false;
        }


        public bool Place(InventoryItemGUI itemGUI, Vector2Int position, float rotation)
        {
            if (inventory.Place(itemGUI.item, position, rotation))
            {
                MakeItemGUIAsChild(itemGUI);
                return true;
            }
            return false;
        }

        public void MakeItemGUIAsChild(InventoryItemGUI itemGUI)
        {
            itemGUI.transform.SetParent(this.transform);
            itemGUI.rectTransform.sizeDelta = itemGUI.item.size * SLOT_SIZE;
            var localTopLeftCorner = new Vector2(-inventory.size.x * SLOT_SIZE / 2f, inventory.size.y * SLOT_SIZE / 2f);
            var itemRect = itemGUI.item.CurrentRect;
            var itemOffset = new Vector2(itemRect.width / 2f, -itemRect.height / 2f);
            var itemPosition = new Vector2(itemRect.x, -itemRect.y) + itemOffset;
            itemGUI.transform.localPosition = localTopLeftCorner + itemPosition * SLOT_SIZE;
            itemGUI.transform.localEulerAngles = new Vector3(0f, 0f, itemGUI.item.rotation);
            itemGUI.transform.localScale = Vector3.one;
            itemGUI.UpdateText();
        }
    }
}