﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FDZ.Projects.Inventory
{
    [ExecuteInEditMode]
    public class InventoryItemGUI : MonoBehaviour
    {
        public enum Corners { BottomLeft, TopLeft, TopRight, BottomRight }

        public InventoryItem item;

        public RectTransform rectTransform { get => transform as RectTransform; }

        private List<InventoryGUI> subInventories;
        private RectTransform subInventoriesHolder;

        private bool rotateAnimation;

        private Quaternion targetRotation;

        public Quaternion rotation { get => rotateAnimation ? targetRotation : transform.rotation; }

        private void LateUpdate()
        {
            if (rotateAnimation)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * InventoryGUI.ROTATION_SPEED);
                UpdateText();
                if (Quaternion.Angle(transform.rotation, targetRotation) <= .01f)
                {
                    rotateAnimation = false;
                    transform.rotation = targetRotation;
                    transform.Find("Background").gameObject.SetActive(true);
                    transform.Find("Background").gameObject.SetActive(true);
                    transform.Find("TextMask").gameObject.SetActive(true);
                }
            }
        }

        public void GlobalRotateAnimated(bool right)
        {
            rotateAnimation = true;
            var targetRotationZ = Mathf.RoundToInt(targetRotation.eulerAngles.z + (right ? -90f : 90f));
            targetRotation = Quaternion.Euler(0f, 0f, targetRotationZ);
            item.rotation = targetRotationZ;
            //Debug.Log($"InventoryItemGUI.GlobalRotateAnimated() | item.rotation = {item.rotation}");
            transform.Find("Background").gameObject.SetActive(false);
            transform.Find("TextMask").gameObject.SetActive(false);
        }

        public void UpdateSizeDelta()
        {
            rectTransform.sizeDelta = item.size * InventoryGUI.SLOT_SIZE;
        }

        public void SetTransformLocalRotationToItemRotation()
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, item.rotation);
            targetRotation = transform.rotation;
        }

        public void SetItemRotationToTransformRotation()
        {
            item.rotation = Mathf.RoundToInt(transform.eulerAngles.z);
            targetRotation = transform.rotation;
            //Debug.Log($"InventoryItemGUI.SetItemRotationToTransformRotation() | item.rotation = {item.rotation}");
        }

        public void UpdateText()
        {
            transform.Find("TextMask/BottomLeft").GetComponent<Text>().text = "";
            transform.Find("TextMask/TopLeft").GetComponent<Text>().text = "";
            transform.Find("TextMask/TopRight").GetComponent<Text>().text = item.shortName;
            transform.Find("TextMask/BottomRight").GetComponent<Text>().text = "";
        }

        public bool Contains(InventoryGUI inventoryGUI)
        {
            return item.Contains(inventoryGUI.inventory);
        }

        public void SetSubInventoriesVisibility(bool value)
        {
            if (subInventories != null)
            {
                foreach (var subInventory in subInventories)
                {
                    subInventory.gameObject.SetActive(value);
                }
            }
        }

        public void CreateSubInventories(InventoryGUI inventoryPrefab)
        {
            var subInventoriesParent = transform.Find("SubInventories") as RectTransform;
            // INIT
            {
                if (subInventories == null)
                {
                    subInventories = new List<InventoryGUI>();
                    subInventoriesHolder = new GameObject("SubInventoriesHolder", typeof(RectTransform)).transform as RectTransform;
                    subInventoriesHolder.parent = subInventoriesParent;
                    subInventoriesHolder.anchoredPosition = Vector3.zero;
                    subInventoriesHolder.localRotation = Quaternion.identity;
                    subInventoriesHolder.localScale = Vector3.one;
                }
            }
            // INSTANTIATE
            {
                foreach (var keyValuePair in item.subInventories)
                {
                    var topLeftCorner = keyValuePair.Key;
                    var inventory = keyValuePair.Value;
                    var instance = Instantiate(inventoryPrefab, transform);
                    instance.inventory = inventory;
                    instance.UpdateGUI();
                    subInventories.Add(instance);
                    var position = new Vector2
                    (
                        (topLeftCorner.x + inventory.size.x / 2f) * InventoryGUI.SLOT_SIZE,
                        (-topLeftCorner.y - inventory.size.y / 2f) * InventoryGUI.SLOT_SIZE
                    );
                    instance.transform.localPosition = position;
                }
            }
            // CALCULATE BOUNDS
            {
                var rectBounds = new Rect();
                for (int i = 0; i < subInventories.Count; i++)
                {
                    var subInventory = subInventories[i];
                    var corners = new Vector3[4];
                    subInventory.rectTransform.GetLocalCorners(corners);
                    var subInventoryRect = new Rect
                    (
                        Mathf.Abs(subInventory.transform.localPosition.x + corners[(int)Corners.TopLeft].x),
                        Mathf.Abs(subInventory.transform.localPosition.y + corners[(int)Corners.TopLeft].y),
                        subInventory.rectTransform.sizeDelta.x,
                        subInventory.rectTransform.sizeDelta.y
                    );
                    if (subInventoryRect.xMin < rectBounds.xMin || i == 0)
                    {
                        rectBounds.xMin = subInventoryRect.xMin;
                    }
                    if (subInventoryRect.xMax > rectBounds.xMax || i == 0)
                    {
                        rectBounds.xMax = subInventoryRect.xMax;
                    }
                    if (subInventoryRect.yMin < rectBounds.yMin || i == 0)
                    {
                        rectBounds.yMin = subInventoryRect.yMin;
                    }
                    if (subInventoryRect.yMax > rectBounds.yMax || i == 0)
                    {
                        rectBounds.yMax = subInventoryRect.yMax;
                    }
                }
                subInventoriesHolder.sizeDelta = rectBounds.size;
                subInventoriesHolder.transform.position += new Vector3(rectBounds.size.x / 2f, -rectBounds.size.y / 2f);
            }
            // MAKE SUB-INVENTORIES CHILDREN OF THE HOLDER
            {
                foreach (var subInventory in subInventories)
                {
                    subInventory.transform.SetParent(subInventoriesHolder);
                }
                subInventoriesHolder.transform.localPosition = Vector3.zero;
            }
            // SCALE THE SUB-INVENTORIES HOLDER TO FIT INSIDE THE ITEM
            {
                var x = subInventoriesHolder.sizeDelta.x / subInventoriesParent.rect.size.x;
                var y = subInventoriesHolder.sizeDelta.y / subInventoriesParent.rect.size.y;
                if (x > 1f || y > 1f)
                {
                    var size = Mathf.Max(x, y);
                    var scale = 1f / size;
                    subInventoriesHolder.localScale = Vector3.one * scale;
                }
            }
            // SCALE ALL THE SUB-INVENTORIES TO MAKE SOME SPACING BETWEEN THEM
            {
                foreach (var subInventory in subInventories)
                {
                    var x = (subInventory.rectTransform.sizeDelta.x + InventoryGUI.SUB_INVENTORY_SPACING) / subInventory.rectTransform.sizeDelta.x;
                    var y = (subInventory.rectTransform.sizeDelta.y + InventoryGUI.SUB_INVENTORY_SPACING) / subInventory.rectTransform.sizeDelta.y;
                    var size = Mathf.Max(x, y);
                    var scale = 1f / size;
                    subInventory.auxLocalScale = subInventory.transform.localScale = Vector3.one * scale;
                }
            }
        }
    }
}