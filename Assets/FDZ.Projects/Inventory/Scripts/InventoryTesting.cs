﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using FDZ;

namespace FDZ.Projects.Inventory.Testing
{
	public class InventoryTesting : MonoBehaviour
	{
		public static InventoryTesting singleton;

		public Canvas canvas;
		public InventoryGUI prefab_inventoryGUI;
		public InventoryItemGUI prefab_itemGUI;

		List<RaycastResult> mouseRaycastResults = new List<RaycastResult>();

		private InventoryItemGUI currentItemGUI;
		private InventoryGUI currentItemGUI_previousInventoryGUI;
		private Vector2Int currentItemGUI_positionBackup;
		private float currentItemGUI_rotationBackup;

		private InventoryGUI currentInventoryGUI;

		HashSet<InventoryItemGUI> overlaping_itemsGUIs = new HashSet<InventoryItemGUI>();
		HashSet<InventoryGUI> overlaping_inventoriesGUIs = new HashSet<InventoryGUI>();
		HashSet<InventoryGUI> zoomed_inventoriesGUIs = new HashSet<InventoryGUI>();

		private KeyCode keyZoom = KeyCode.LeftControl;
		private KeyCode keyGrab = KeyCode.Mouse0;
		private KeyCode keyPlace = KeyCode.Mouse0;
		private KeyCode keyDrop = KeyCode.Mouse1;
		private KeyCode keyRotateLeft = KeyCode.Q;
		private KeyCode keyRotateRight = KeyCode.E;

		private void Awake()
		{
			singleton = this;
			Test();
		}

		#region Test
		public InventoryGUI CreateInventory(int r, int w, int h, float s)
		{
			var inventoryGUI = Instantiate(prefab_inventoryGUI, transform);
			inventoryGUI.transform.localEulerAngles = new Vector3(0f, 0f, r);
			inventoryGUI.transform.localScale = Vector3.one * s;
			inventoryGUI.inventory = new Inventory() { size = new Vector2Int(w, h) };
			inventoryGUI.UpdateGUI();
			inventoryGUI.name = "inventory_" + inventoryGUI.GetInstanceID();
			return inventoryGUI;
		}

		public InventoryItemGUI AddItem(InventoryGUI inventoryGUI, string shortName, int w, int h)
		{
			var itemGUI = Instantiate(prefab_itemGUI);
			itemGUI.item = new InventoryItem() { shortName = shortName, size = new Vector2Int(w, h) };
			itemGUI.UpdateSizeDelta();
			itemGUI.UpdateText();
			itemGUI.name = "item_" + itemGUI.GetInstanceID();
			if (inventoryGUI.Place(itemGUI) == false)
			{
				Destroy(itemGUI.gameObject);
				return null;
			}
			itemGUI.SetTransformLocalRotationToItemRotation();
			return itemGUI;
		}

		private void Test()
		{
			InventoryGUI inventoryGUI;
			InventoryItemGUI itemGUI;
			/*******************************************************************************************/
			inventoryGUI = CreateInventory(0, 15, 15, 1);

			if ((itemGUI = AddItem(inventoryGUI, "TRig-A", 4, 3)) != null)
			{
				itemGUI.item.subInventories = new Dictionary<Vector2Int, Inventory>();
				// y = 0
				itemGUI.item.subInventories.Add(new Vector2Int(0, 0), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(1, 0), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(2, 0), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(3, 0), new Inventory() { size = new Vector2Int(1, 2) });
				// y = 2
				itemGUI.item.subInventories.Add(new Vector2Int(0, 2), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(1, 2), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(2, 2), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(3, 2), new Inventory() { size = new Vector2Int(1, 2) });
				// init gui
				itemGUI.CreateSubInventories(prefab_inventoryGUI);
			}

			if ((itemGUI = AddItem(inventoryGUI, "TRig-B", 4, 4)) != null)
			{
				itemGUI.item.subInventories = new Dictionary<Vector2Int, Inventory>();
				// y = 0
				itemGUI.item.subInventories.Add(new Vector2Int(0, 0), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(1, 0), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(2, 0), new Inventory() { size = new Vector2Int(1, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(3, 0), new Inventory() { size = new Vector2Int(1, 2) });
				// y = 2
				itemGUI.item.subInventories.Add(new Vector2Int(0, 2), new Inventory() { size = new Vector2Int(2, 2) });
				itemGUI.item.subInventories.Add(new Vector2Int(2, 2), new Inventory() { size = new Vector2Int(2, 2) });
				// y = 4
				itemGUI.item.subInventories.Add(new Vector2Int(0, 4), new Inventory() { size = new Vector2Int(1, 1) });
				itemGUI.item.subInventories.Add(new Vector2Int(1, 4), new Inventory() { size = new Vector2Int(1, 1) });
				itemGUI.item.subInventories.Add(new Vector2Int(2, 4), new Inventory() { size = new Vector2Int(1, 1) });
				itemGUI.item.subInventories.Add(new Vector2Int(3, 4), new Inventory() { size = new Vector2Int(1, 1) });
				// init gui
				itemGUI.CreateSubInventories(prefab_inventoryGUI);
			}


			if ((itemGUI = AddItem(inventoryGUI, "Box-A", 6, 4)) != null)
			{
				itemGUI.item.subInventories = new Dictionary<Vector2Int, Inventory>();
				// y = 0
				itemGUI.item.subInventories.Add(new Vector2Int(0, 0), new Inventory() { size = new Vector2Int(9, 6) });
				// init gui
				itemGUI.CreateSubInventories(prefab_inventoryGUI);
			}

			for (int i = 0; i < 4; i++)
			{
				if ((itemGUI = AddItem(inventoryGUI, "3x3", 3, 3)) != null)
				{
					itemGUI.item.subInventories = new Dictionary<Vector2Int, Inventory>();
					// y = 0
					itemGUI.item.subInventories.Add(new Vector2Int(0, 0), new Inventory() { size = new Vector2Int(3, 3) });
					// init gui
					itemGUI.CreateSubInventories(prefab_inventoryGUI);
				}
			}

			AddItem(inventoryGUI, "Thingy", 1, 2);
			AddItem(inventoryGUI, "Thingy", 1, 2);
			AddItem(inventoryGUI, "Thingy", 2, 1);
			AddItem(inventoryGUI, "Thingy", 2, 1);
			AddItem(inventoryGUI, "Thingy", 2, 2);
			AddItem(inventoryGUI, "Thingy", 2, 2);
			AddItem(inventoryGUI, "Thingy", 1, 1);
			AddItem(inventoryGUI, "Thingy", 1, 1);
			AddItem(inventoryGUI, "Thingy", 1, 1);
			AddItem(inventoryGUI, "Thingy", 1, 1);

			if ((itemGUI = AddItem(inventoryGUI, "Box-B", 2, 2)) != null)
			{
				itemGUI.item.subInventories = new Dictionary<Vector2Int, Inventory>();
				// y = 0
				itemGUI.item.subInventories.Add(new Vector2Int(0, 0), new Inventory() { size = new Vector2Int(4, 4) });
				// init gui
				itemGUI.CreateSubInventories(prefab_inventoryGUI);
			}
		}
		#endregion

		private void LateUpdate()
		{
			{
				FDZ.Utils.EventSystem_RaycastAll(Input.mousePosition, mouseRaycastResults);
				overlaping_itemsGUIs.Clear();
				overlaping_inventoriesGUIs.Clear();
				foreach (var result in mouseRaycastResults)
				{
					var itemGUI = result.gameObject.GetComponentInParent<InventoryItemGUI>();
					if (itemGUI != null)
					{
						overlaping_itemsGUIs.Add(itemGUI);
					}
					var inventoryGUI = result.gameObject.GetComponentInParent<InventoryGUI>();
					if (inventoryGUI != null)
					{
						overlaping_inventoriesGUIs.Add(inventoryGUI);
					}
				}
			}
			{
				// ZOOM OUT
				{
					while (zoomed_inventoriesGUIs.Count > 0)
					{
						var first = zoomed_inventoriesGUIs.First();
						first.zoom = false;
						zoomed_inventoriesGUIs.Remove(first);
					}
				}
				if (overlaping_inventoriesGUIs.Count > 0)
				{
					if (currentInventoryGUI != null)
					{
						currentInventoryGUI.SetBorderVisibility(false);
					}
					currentInventoryGUI = overlaping_inventoriesGUIs.First();
					zoomed_inventoriesGUIs.Add(currentInventoryGUI);
					// ZOOM IN
					{ 
						currentInventoryGUI.zoom = Input.GetKey(keyZoom);
						//if (Input.GetKeyDown(keyZoom))
						//{
						//	currentInventoryGUI.zoom = !currentInventoryGUI.zoom;
						//}
					}
					currentInventoryGUI.SetBorderVisibility(true);
					currentInventoryGUI.transform.SetAsLastSibling();
				}
				else if (currentInventoryGUI != null)
				{
					currentInventoryGUI.SetBorderVisibility(false);
					currentInventoryGUI = null;
				}
			}
			{
				if (currentItemGUI == null)
				{
					if (Input.GetKeyDown(keyGrab))
					{
						foreach (var result in mouseRaycastResults)
						{
							var itemGUI = result.gameObject.GetComponentInParent<InventoryItemGUI>();
							if (itemGUI != null && (currentInventoryGUI == null || itemGUI.Contains(currentInventoryGUI) == false))
							{
								GrabItem(itemGUI);
								break;
							}
						}
					}
				}
				else
				{
					if (Input.GetKeyDown(keyPlace) && currentInventoryGUI != null)
					{
						PlaceCurrentItem();
					}
					else if (Input.GetKeyDown(keyDrop))
					{
						DropCurrentItem();
					}
					else if (Input.GetKeyDown(keyRotateLeft))
					{
						RotateCurrentItem(false);
					}
					else if (Input.GetKeyDown(keyRotateRight))
					{
						RotateCurrentItem(true);
					}
					else
					{
						currentItemGUI.transform.position = Input.mousePosition;
						if (currentInventoryGUI != null)
						{
							currentItemGUI.transform.SetParent(currentInventoryGUI.transform);
							currentItemGUI.transform.localScale = Vector3.Lerp(currentItemGUI.transform.localScale, Vector3.one, Time.deltaTime * InventoryGUI.ZOOM_IN_SPEED);
							currentItemGUI.transform.SetParent(canvas.transform);
						}
						else
						{
							currentItemGUI.transform.SetParent(canvas.transform);
							currentItemGUI.transform.localScale = Vector3.Lerp(currentItemGUI.transform.localScale, Vector3.one, Time.deltaTime * InventoryGUI.ZOOM_OUT_SPEED);
						}
					}
				}
			}
		}

		public void RotateCurrentItem(bool right)
		{
			if (currentItemGUI == null)
			{
				Debug.LogError("Can't rotate");
			}
			//currentItemGUI.rectTransform.Rotate(new Vector3(0f, 0f, right ? -90f : 90f));
			//currentItemGUI.item.rotation = currentItemGUI.rectTransform.localEulerAngles.z;
			//currentItemGUI.UpdateText();

			//currentItemGUI.item.rotation += (right ? -90f : 90f);
			//currentItemGUI.rectTransform.rotation = Quaternion.Euler(0f, 0f, currentItemGUI.item.rotation);
			//currentItemGUI.UpdateText();

			currentItemGUI.GlobalRotateAnimated(right);
		}

		public void PlaceCurrentItem()
		{
			if (currentInventoryGUI == null)
			{
				Debug.LogError("can't place");
				return;
			}
			var position = currentInventoryGUI.WorldPositionToTileCoords(Input.mousePosition);
			var worldToLocalRotation = Quaternion.Inverse(currentInventoryGUI.transform.rotation) * currentItemGUI.rotation;
			var rotation = Mathf.RoundToInt(worldToLocalRotation.eulerAngles.z);
			var rect = InventoryItem.ComputeRect(Vector2Int.zero, rotation, currentItemGUI.item.size);
			rect.x = Mathf.RoundToInt(position.x - rect.width / 2f);
			rect.y = Mathf.RoundToInt(position.y - rect.height / 2f);
			var itemPosition = InventoryItem.GetPositionFromRect(rect, rotation);
			if (currentInventoryGUI.CanPlace(currentItemGUI, itemPosition, rotation))
			{
				currentInventoryGUI.Place(currentItemGUI, itemPosition, rotation);
				currentItemGUI.SetSubInventoriesVisibility(true);
				currentItemGUI = null;
				currentItemGUI_previousInventoryGUI = null;
			}
		}

		public void GrabItem(InventoryItemGUI itemGUI)
		{
			if (itemGUI == null)
			{
				Debug.LogError("itemGUI should not be null");
				return;
			}
			if (currentItemGUI != null)
			{
				DropCurrentItem();
			}
			currentItemGUI = itemGUI;
			currentItemGUI.SetSubInventoriesVisibility(false);
			currentItemGUI_positionBackup = currentItemGUI.item.position;
			currentItemGUI_rotationBackup = currentItemGUI.item.rotation;
			currentItemGUI_previousInventoryGUI = currentItemGUI.GetComponentInParent<InventoryGUI>();
			currentItemGUI.item.RemoveFromCurrentInventory();
			currentItemGUI.transform.SetParent(canvas.transform);
			currentItemGUI.transform.position = Input.mousePosition;
			currentItemGUI.SetItemRotationToTransformRotation();
		}

		public void DropCurrentItem()
		{
			if (currentItemGUI == null)
			{
				Debug.LogError("can't drop");
				return;
			}
			if (currentItemGUI_previousInventoryGUI.Place(currentItemGUI, currentItemGUI_positionBackup, currentItemGUI_rotationBackup) == false)
			{
				Debug.LogError("Implement this");
			}
			currentItemGUI.SetSubInventoriesVisibility(true);
			currentItemGUI = null;
			currentItemGUI_previousInventoryGUI = null;
		}
    }
}