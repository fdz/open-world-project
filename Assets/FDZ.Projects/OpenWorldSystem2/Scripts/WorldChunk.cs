﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem2
{
	[System.Serializable]
	public class WorldChunk
	{
		public WorldChunk mWorld;

		public WorldChunk(WorldChunk world)
        {
			mWorld = world;
        }
	}
}