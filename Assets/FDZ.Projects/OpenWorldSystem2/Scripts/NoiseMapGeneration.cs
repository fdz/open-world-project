﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem2
{
    public static class NoiseMapGeneration
    {
        /// <summary>
        /// Source from Sebastian Lague's tutorial: Procedural Landmass Generation
        /// </summary>
        /// <param name="mapWidth"></param>
        /// <param name="mapHeight"></param>
        /// <param name="seed"></param>
        /// <param name="scale"></param>
        /// <param name="octaves"></param>
        /// <param name="persistance"></param>
        /// <param name="lacunarity"></param>
        /// <param name="offset"></param>
        /// <param name="normalizationModeIsLocal"></param>
        /// <returns></returns>
        public static void GenerateNoiseMap(
            float[,] noiseMap,
            int seed = 0,
            float scale = 30f,
            int octaves = 4,
            float persistance = .5f,
            float lacunarity = 2,
            Vector2 offset = new Vector2(),
            bool normalizationModeIsLocal = true)
        {
            int mapWidth = noiseMap.GetLength(0);
            int mapHeight = noiseMap.GetLength(1);

            System.Random prng = new System.Random(seed);
            Vector2[] octaveOffsets = new Vector2[octaves];

            float maxPosibleHeight = 0;
            float amplitude = 1;

            for (int i = 0; i < octaves; i++)
            {
                octaveOffsets[i] = new Vector2(
                    prng.Next(-100000, 100000) + offset.x,
                    prng.Next(-100000, 100000) + offset.y);
                maxPosibleHeight += amplitude;
                amplitude *= persistance;
            }

            if (scale <= 0)
            {
                scale = 0.0001f;
            }

            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    amplitude = 1;
                    float frequency = 1;
                    float noiseHeight = 0;

                    for (int i = 0; i < octaves; i++)
                    {
                        float sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
                        float sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }

                    if (normalizationModeIsLocal)
                    {
                        if (noiseHeight > maxLocalNoiseHeight)
                        {
                            maxLocalNoiseHeight = noiseHeight;
                        }
                        else if (noiseHeight < minLocalNoiseHeight)
                        {
                            minLocalNoiseHeight = noiseHeight;
                        }
                        noiseMap[x, y] = noiseHeight;
                    }
                    else
                    {
                        //float normalizedHeight = (noiseMap[x, y] + 1) / (2f * maxPosibleHieght / 1.75f);
                        float normalizedHeight = (noiseHeight + 1) / maxPosibleHeight;
                        noiseMap[x, y] = normalizedHeight;
                    }
                }
            }

            if (normalizationModeIsLocal)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                    }
                }
            }
        }

        public static Vector2 GetMinMax(float[,] noiseMap)
        {
            int mapWidth = noiseMap.GetLength(0);
            int mapHeight = noiseMap.GetLength(1);
            float min = float.MaxValue;
            float max = float.MinValue;
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    min = Mathf.Min(noiseMap[x, y], min);
                    max = Mathf.Max(noiseMap[x, y], max);
                }
            }
            return new Vector2(min, max);
        }

        public static void Remap(float [,] noiseMap, float min, float max)
        {
            int mapWidth = noiseMap.GetLength(0);
            int mapHeight = noiseMap.GetLength(1);
            Vector2 minMax = GetMinMax(noiseMap);
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    noiseMap[x, y] = Utils.Remap(noiseMap[x, y], minMax.x, minMax.y, min, max);
                }
            }
        }

        public static void Substract(float[,] a, float[,] b)
        {
            int mapWidth = a.GetLength(0);
            int mapHeight = a.GetLength(1);
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    a[x, y] = a[x, y] - b[x, y];
                }
            }
        }

        public static void Multiply(float[,] a, float scalar)
        {
            int mapWidth = a.GetLength(0);
            int mapHeight = a.GetLength(1);
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    a[x, y] = a[x, y] * scalar;
                }
            }
        }

        public static void ApplyFalloffCircle(float[,] noiseMap, float normalziedRadius)
        {
            int mapWidth = noiseMap.GetLength(0);
            int mapHeight = noiseMap.GetLength(1);

            float maxDistance = mapWidth * mapWidth + mapHeight * mapHeight;
            float radius = maxDistance * .125f * normalziedRadius;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    float xx = x - (mapWidth - 1) * .5f;
                    float yy = y - (mapHeight - 1) * .5f;
                    float distance = xx * xx + yy * yy;
                    noiseMap[x, y] = noiseMap[x, y] * Mathf.Clamp01(1.0f - distance / radius);
                }
            }
        }

        public static void ApplyFalloffBox(float[,] noiseMap, float normalizedFalloff, AnimationCurve animCurve)
        {
            int mapWidth = noiseMap.GetLength(0);
            int mapHeight = noiseMap.GetLength(1);

            float left = mapWidth * normalizedFalloff;
            float right = mapWidth * (1 - normalizedFalloff);

            float bottom = mapHeight * normalizedFalloff;
            float top = mapHeight * (1 - normalizedFalloff);

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    noiseMap[x, y] = noiseMap[x, y] *
                        animCurve.Evaluate(x / left) *
                        animCurve.Evaluate(1.0f - (x - right) / (mapWidth - right)) *
                        animCurve.Evaluate(y / bottom) *
                        animCurve.Evaluate(1.0f - (y - top) / (mapHeight - top));
                }
            }
        }

        public static void BlendLeftRight(float[,] noiseMapLeft, float[,] noiseMapRight, float[,] noiseMapAux)
        {
            int mapWidth = noiseMapLeft.GetLength(0);
            int mapHeight = noiseMapRight.GetLength(1);

            float halfWidth = mapWidth * 0.5f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < halfWidth; x++)
                {
                    int x1 = x + (int)halfWidth;

                    float t = 1.0f - x / (float)halfWidth;
                    noiseMapRight[x, y] = Mathf.Lerp(noiseMapRight[x, y], noiseMapAux[x1, y], t);

                    t = (x1 - halfWidth) / (float)halfWidth;
                    noiseMapLeft[x1, y] = Mathf.Lerp(noiseMapLeft[x1, y], noiseMapAux[x, y], t);
                }
            }
        }

        public static void BlendBottomTop(float[,] noiseMapBottom, float[,] noiseMapTop, float[,] noiseMapAux)
        {
            int mapWidth = noiseMapBottom.GetLength(0);
            int mapHeight = noiseMapTop.GetLength(1);

            float halfHeight = mapHeight * 0.5f;

            for (int y = 0; y < halfHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    int y1 = y + (int)halfHeight;

                    float t = 1.0f - y / (float)halfHeight;
                    noiseMapTop[x, y] = Mathf.Lerp(noiseMapTop[x, y], noiseMapAux[x, y1], t);

                    t = (y1 - halfHeight) / (float)halfHeight;
                    noiseMapBottom[x, y1] = Mathf.Lerp(noiseMapBottom[x, y1], noiseMapAux[x, y], t);
                }
            }
        }

        public static Texture2D CreateTextureFromColorMap(Color[] colorMap, int width, int height)
        {
            Texture2D texture = new Texture2D(width, height);
            texture.SetPixels(colorMap);
            texture.filterMode = FilterMode.Point;
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        public static Texture2D CreateTextureFromHeightMap(float[,] heightMap, float minValue = 0, float maxValue = 1)
        {
            return CreateTextureFromHeightMap(heightMap, Color.black, Color.white, minValue, maxValue);
        }

        public static Texture2D CreateTextureFromHeightMap(float[,] heightMap, Color minColor, Color maxColor, float minValue = 0, float maxValue = 1)
        {
            int width = heightMap.GetLength(0);
            int height = heightMap.GetLength(1);
            Color[] colorMap = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float value = heightMap[x, y];
                    float t = Utils.Remap(value, minValue, maxValue, 0, 1);
                    colorMap[y * width + x] = Color.Lerp(minColor, maxColor, t);
                }
            }
            return CreateTextureFromColorMap(colorMap, width, height);
        }
    }
}