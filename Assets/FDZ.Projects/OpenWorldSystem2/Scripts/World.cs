﻿using UnityEngine;

/*
The world is an sphere, and a cube can be shaped like a sphere, 4 faces
*/

namespace FDZ.Projects.OpenWorldSystem2
{
	[System.Serializable]
	public class World
	{
        // parameters
		public int mSize = 1;
		public int mChunkSize = 1;
        public AnimationCurve mTest;
        // generated
		public float[,][,] mNoiseMaps;
        public int mWorldCellMax;

        public void Generate()
        {
			mNoiseMaps = new float[mSize, mSize][,];
            mWorldCellMax = mChunkSize * mSize - 1;
            for (int chunkY = 0; chunkY < mSize; chunkY++)
            {
                for (int chunkX = 0; chunkX < mSize; chunkX++)
                {
                    var noiseMap = mNoiseMaps[chunkX, chunkY] = new float[mChunkSize, mChunkSize];
                    NoiseMapGeneration.GenerateNoiseMap(
                        noiseMap,
						offset: new Vector2(chunkX * mChunkSize, chunkY * mChunkSize),
						normalizationModeIsLocal: false);
                    for (int localCellY = 0; localCellY < mChunkSize; localCellY++)
                    {
                        var worldCellY = chunkY * mChunkSize + localCellY;
                        var p = worldCellY / (float)mWorldCellMax;
                        p = mTest.Evaluate(p);
                        for (int localCellX = 0; localCellX < mChunkSize; localCellX++)
                        {
                            noiseMap[localCellX, localCellY] = noiseMap[localCellX, localCellY] * p;
                        }
                    }
                }
            }

        }
	}
}