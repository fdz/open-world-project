﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FDZ.Projects.OpenWorldSystem2
{
    public class Testing : MonoBehaviour
    {
#if UNITY_EDITOR
        [CustomEditor(typeof(Testing))]
        private class TestingEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                GUILayout.Space(10);
                var target = (Testing)this.target;
                if (GUILayout.Button("Randomize Seed"))
                {
                    target.seed = (int)(Random.value * 100000000);
                    target.Generate();
                }
                if (GUILayout.Button("Sort height colors"))
                {
                    bool sorted = true;
                    while (sorted)
                    {
                        sorted = false;
                        for (int i = 0; i < target.heightColors.Length - 1; i++)
                        {
                            var cur = target.heightColors[i];
                            var next = target.heightColors[i + 1];
                            if (cur.height < next.height)
                            {
                                target.heightColors[i] = next;
                                target.heightColors[i + 1] = cur;
                                sorted = true;
                            }
                        }
                    }
                    target.Generate();
                }
                if (GUILayout.Button("Generate"))
                {
                    target.Generate();
                }
            }
        }
#endif
        [System.Serializable]
        public struct HeightColor
        {
            public string name;
            [Range(0.0f, 1.0f)] public float height;
            public Color color;
        }

        public int seed = 0;

        [Space()]
        public int sizeX = 1;
        public int sizeY = 1;

        [Space()]
        [Range(0.0f, 1.0f), UnityEngine.Serialization.FormerlySerializedAs("landPcg")] public float landScalePcg;
        [Range(0.0f, 1.0f), UnityEngine.Serialization.FormerlySerializedAs("waterPcg")] public float waterScalePcg;

        [Space()]
        [Range(0.001f, 1.0f)] public float falloff = 0.5f;
        public AnimationCurve falloffCurve;

        [Space()]
        public float min;
        public float max;

        [Space()]
        public bool drawColors;
        [Range(0.01f, 1.0f)] public float waterTreshold;
        public HeightColor[] heightColors;

        private void OnValidate()
        {
            Generate();
        }

        private void Start()
        {
            Generate();
        }

        public void Generate()
        {
            Debug.Log("Generation started");

            var auxSize = (sizeX + sizeY) * .5f;

            var waterMap = new float[sizeX, sizeY];
            var waterScale = auxSize * waterScalePcg;
            NoiseMapGeneration.GenerateNoiseMap(waterMap, seed: seed + 1, normalizationModeIsLocal: false, scale: waterScale, offset: new Vector2(0f, 0f));
            //NoiseMapGeneration.ApplyFalloffBox(waterMap, falloff, falloffCurve);

            var landMap = new float[sizeX, sizeY];
            var landScale = auxSize * landScalePcg;
            NoiseMapGeneration.GenerateNoiseMap(landMap, seed: seed, normalizationModeIsLocal: false, scale: landScale, offset: new Vector2(0f, 0f));
            NoiseMapGeneration.Substract(landMap, waterMap);
            NoiseMapGeneration.ApplyFalloffBox(landMap, falloff, falloffCurve);
            //NoiseMapGeneration.Multiply(landMap, 2f);

            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    if (landMap[x, y] <= 0.0f)
                    {
                        landMap[x, y] = 0.0f;
                    }
                    else if (landMap[x, y] >= 1.0f)
                    {
                        landMap[x, y] = 1.0f;
                    }
                }
            }

            //NoiseMapGeneration.Remap(landMap, 0.0f, 1.0f);

            min = float.MaxValue;
            max = float.MinValue;
            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    min = Mathf.Min(landMap[x, y], min);
                    max = Mathf.Max(landMap[x, y], max);
                }
            }

            Color[] colorMap = new Color[sizeX * sizeY];
            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    float value = landMap[x, y];
                    for (int i = 0; i < heightColors.Length; i++)
                    {
                        if (Mathf.Clamp(value, -1.0f, 1.0f) >= Mathf.Clamp(heightColors[i].height, -1.0f, 1.0f))
                        {
                            colorMap[y * sizeX + x] = heightColors[i].color;
                            break;
                        }
                    }
                    //var i = landMap[x, y];
                    //colorMap[y * sizeX + x] = new Color(color.r * i, color.g * i, color.b * i, 1f);
                }
            }
            var texture2d = drawColors ?
                NoiseMapGeneration.CreateTextureFromColorMap(colorMap, sizeX, sizeY) :
                NoiseMapGeneration.CreateTextureFromHeightMap(landMap, 0, 1);

            // SET TEXTURE
            GetComponentInChildren<SpriteRenderer>().sprite =Sprite.Create(texture2d, new Rect(0, 0, sizeX, sizeY), Vector2.zero);

            Debug.Log("Generation ended");
        }
    }
}