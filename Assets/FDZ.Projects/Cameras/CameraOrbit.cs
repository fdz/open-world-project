﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FDZ.Attributes;
using System;

namespace FDZ.Projects.Cameras
{
    public class CameraOrbit : MonoBehaviour
    {
        public Transform target;
        public float radius;
        public bool mouseXInvert;
        public bool mouseYInvert;

        private Vector3 eulerAngles;

        private void OnValidate()
        {
            if (radius < .1f)
            {
                radius = .1f;
            }

            eulerAngles = Quaternion.LookRotation((target.position - transform.position).normalized).eulerAngles;
        }

        private void LateUpdate()
        {
            Tick();
        }

        public void Tick()
        {
            if (target == null)
            {
                return;
            }
            
            //OnValidate();

            // example of moving character relative to camera
            {
                var horizontal = Input.GetAxisRaw("Horizontal");
                var vertical = Input.GetAxisRaw("Vertical");
                var forward = new Vector3(transform.forward.x * vertical, 0f, transform.forward.z * vertical);
                var right = new Vector3(transform.right.x * horizontal, 0f, transform.forward.z * vertical);
                var direction = (forward + right).normalized;
                target.position += direction * (Time.deltaTime * 4f);
            }

            var pitch = (mouseYInvert ? -1 : 1) * Input.GetAxisRaw("Mouse Y");
            var yaw = (mouseXInvert ? -1 : 1) * Input.GetAxisRaw("Mouse X");
            var roll = -UtilsInput.GetAxis(KeyCode.Q, KeyCode.E);
            eulerAngles.x += pitch;
            eulerAngles.x = Utils.WrapAngle(eulerAngles.x);
            eulerAngles.x = Mathf.Clamp(eulerAngles.x, -89f, 89f);
            eulerAngles.x = Utils.UnwrapAngle(eulerAngles.x);
            eulerAngles.y += yaw;
            eulerAngles.z = roll * 45f;

            transform.rotation = Quaternion.Euler(eulerAngles);
            transform.position = target.position - (transform.rotation * Vector3.forward) * radius;

        } // end of func


    } // end of class
} // end of namespace