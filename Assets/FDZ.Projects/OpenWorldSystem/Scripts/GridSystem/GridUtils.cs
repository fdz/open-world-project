﻿using UnityEngine;
using System;
using Drawing;
using Unity.Collections;

namespace FDZ.Projects.OpenWorldSystem
{
    public enum GridLineSnapAngleMode
    {
        Straight,
        Diagonal,
        Any,
    }

    public enum GridOrientation
    {
        Straight = GridLineSnapAngleMode.Straight,
        Diagonal = GridLineSnapAngleMode.Diagonal,
    }

    public struct GridLine3D
    {
        public Vector3 start;
        public Vector3 end;
        public GridOrientation orientation;
        public float Length => (end - start).magnitude;
        public Vector3 Direction => (end - start).normalized;
    }

    public struct GridLine2DInt
    {
        public Vector2Int start;
        public Vector2Int end;
        public GridOrientation orientation;
    }

    public struct GridBox3D
    {
        public GridLine3D left;
        public GridLine3D right;
        public GridLine3D bottom;
        public GridLine3D top;
    }

    public struct GridBox2DInt
    {
        public GridLine2DInt left;
        public GridLine2DInt right;
        public GridLine2DInt bottom;
        public GridLine2DInt top;
    }

    public static class GridUtils
    {
        // TODO: Maybe only leave the GridLineInt and GridLineBox

        private static GridLine3D CalculateLine(Vector3 from, Vector3 to, GridLineSnapAngleMode mode)
        {
            GridLine3D line = new GridLine3D();

            Vector3 dif = (to.xoz() - from.xoz());
            Vector3 dir = dif.normalized;

            int rot = dir.x == 0 && dir.z == 0 ? 0 : (int)Quaternion.LookRotation(dir).eulerAngles.y;
            int nearestAngle; // 0, 45, 90, 135, 180, 225, 270, 315, 360
            if (mode == GridLineSnapAngleMode.Straight)
            {
                nearestAngle = ((rot + 45) / 90) * 90;
            }
            else
            {
                nearestAngle = ((rot + 23) / 45) * 45;
                if (mode == GridLineSnapAngleMode.Diagonal && Math.Abs(nearestAngle) % 90f == 0)
                {
                    nearestAngle += 45;
                }
            }

            line.orientation = Math.Abs(nearestAngle) % 90 == 0 ? GridOrientation.Straight : GridOrientation.Diagonal;

            var newDirection = Quaternion.Euler(0f, nearestAngle, 0f) * Vector3.forward;
            var newLength = dif.magnitude * Vector3.Dot(newDirection, dir);

            line.start = from;
            line.end = from + newDirection * newLength;

            return line;
        }

        public static GridLine2DInt CalculateLine(Vector2Int from, Vector2Int to, GridLineSnapAngleMode mode)
        {
            GridLine3D auxLine = GridUtils.CalculateLine(from.xoy(), to.xoy(), GridLineSnapAngleMode.Any);
            GridLine2DInt line = new GridLine2DInt();

            if (auxLine.orientation == GridOrientation.Diagonal)
            {
                // 1.0 (10) = length of a straight line
                // 1.4 (14) = length of a diagonal line (45 degrees)
                float aproxNodeCount = Mathf.Round(auxLine.Length * 10f) / 14f;
                int nodesCount = Mathf.RoundToInt(aproxNodeCount);
                auxLine.end = auxLine.start + (auxLine.end - auxLine.start).normalized * nodesCount * 1.4f;
            }

            line.start = from;
            line.end = auxLine.end.xzIntRound();
            line.orientation = auxLine.orientation;

            return line;
        }

        private static GridBox3D CalculateBox(Vector3 from, Vector3 to, GridLineSnapAngleMode mode)
        {
            GridBox3D box = new GridBox3D();

            box.left = CalculateLine(from, to, mode);
            box.bottom = CalculateLine(box.left.end, to, (GridLineSnapAngleMode)box.left.orientation);

            box.right = new GridLine3D();
            box.right.start = box.left.start + box.bottom.Direction * box.bottom.Length;
            box.right.end = box.bottom.end;
            box.right.orientation = box.left.orientation;

            box.top = new GridLine3D();
            box.top.start = box.left.start;
            box.top.end = box.right.start;
            box.top.orientation = box.bottom.orientation;

            return box;
        }

        private static void DrawLine(GridLine3D line, Color color)
        {
            Draw.ingame.Line(line.start, line.end, color);
        }


        private static GridLine3D TransformLine(GridGroup gridGroup, GridLine2DInt line)
        {
            var line3D = new GridLine3D();
            line3D.start = gridGroup.CalculatePosition(line.start, Space.World);
            line3D.end = gridGroup.CalculatePosition(line.end, Space.World);
            line3D.orientation = line.orientation;
            return line3D;
        }

        public static void DrawLine(GridGroup gridGroup, GridLine2DInt line, Color color)
        {
            DrawLine(TransformLine(gridGroup, line), color);
        }

        private static void DrawBox(GridBox3D box, Color color)
        {
            Draw.ingame.Polyline(new Vector3[] { box.left.start, box.right.start, box.right.end, box.left.end }, true, color);
        }

        public static void DrawGridNodeBorders(GridGroup group, Vector2Int gridNodeCoords)
        {
            var nodeA = group.GetNode(gridNodeCoords);
            var nodeB = group.GetNode(gridNodeCoords + new Vector2Int(1, 0));
            var nodeC = group.GetNode(gridNodeCoords + new Vector2Int(1, 1));
            var nodeD = group.GetNode(gridNodeCoords + new Vector2Int(0, 1));
            Draw.ingame.Line(nodeA.WorldPosition, nodeB.WorldPosition);
            Draw.ingame.Line(nodeB.WorldPosition, nodeC.WorldPosition);
            Draw.ingame.Line(nodeC.WorldPosition, nodeD.WorldPosition);
            Draw.ingame.Line(nodeD.WorldPosition, nodeA.WorldPosition);
        }

        public static void ForEachNode(GridGroup gridGroup, GridLine2DInt line, Action<GridNode, GridNode> action)
        {
            Vector2Int startCoords = line.start;
            Vector2Int endCoords = line.end;
            
            Vector2Int dif = endCoords - startCoords;
            Vector2Int dir = new Vector2Int(Math.Sign(dif.x), Math.Sign(dif.y));

            for (Vector2Int curCoords = startCoords; curCoords != endCoords; curCoords += dir)
            {
                var curNode = gridGroup.GetNode(curCoords);
                var nextnode = gridGroup.GetNode(curCoords + dir);
                action.Invoke(curNode, nextnode);
            }
        }

        public static void ForEachNode(GridGroup gridGroup, GridLine2DInt line, Action<GridNode> action)
        {
            Vector2Int startCoords = line.start;
            Vector2Int endCoords = line.end;

            Vector2Int dif = endCoords - startCoords;
            Vector2Int dir = new Vector2Int(Math.Sign(dif.x), Math.Sign(dif.y));

            for (Vector2Int curCoords = startCoords; curCoords != endCoords; curCoords += dir)
            {
                var curNode = gridGroup.GetNode(curCoords);
                action.Invoke(curNode);
            }
        }
    }
}