﻿using System;
using System.Threading;
using UnityEngine;
using Drawing;
using FDZ.Attributes;

namespace FDZ.Projects.OpenWorldSystem
{
    // TODO: Clean class and make the functions take Coords instead of Positions
    public class Grid : MonoBehaviour
    {
        public static readonly Vector2Int[] DIRECTIONS = new Vector2Int[]
        {
            new Vector2Int(0, 1),
            new Vector2Int(0, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(-1, -1),
            new Vector2Int(-1, 1),
            new Vector2Int(1, 0),
            new Vector2Int(1, -1),
            new Vector2Int(1, 1),
        };

        [SerializeField, ReadOnly] private bool isInitialized;
        [SerializeField, ReadOnly] private bool _hasNodes;
        [SerializeField, ReadOnly] private Vector2Int _coords;
        public GridNode[] nodes;

        /**************************************************************/

        public bool HasNodes { get => _hasNodes; private set => _hasNodes = value; }

        public GridGroup Group { get; private set; }

        public Vector2Int Coords { get => _coords; private set => _coords = value; }

        public Vector2Int Size { get; private set; }

        public float NodeSize { get; private set; }

        public Vector2Int Center { get; private set; }

        public Vector2Int CornerTopLeft { get; private set; }

        public Vector2Int CornerTopRight { get; private set; }

        public Vector2Int CornerBottomRight { get; private set; }

        public Vector2Int CornerBottomLeft { get; private set; }

        /**************************************************************/

        public void Init(GridGroup group, Vector2Int coords, Vector2Int size, float nodeSize)
        {
            if (size.x % 2 != 0 || size.y % 2 != 0)
            {
                Debug.LogError("Grid size should be even");
            }

            if (isInitialized)
            {
                return;
            }

            isInitialized = true;

            Group = group;
            Coords = coords;
            Size = size;
            NodeSize = nodeSize;

            transform.SetParent(Group.transform, true);

            transform.localPosition = new Vector3(
                Coords.x * Size.x * NodeSize,
                0f,
                Coords.y * Size.y * NodeSize);

            transform.localRotation = Quaternion.identity;
        }

        public void GenerateNodes()
        {
            if (nodes != null)
            {
                return;
            }

            HasNodes = false;

            Vector2Int coords = new Vector2Int();
            GridNode node;
            //GridNode neighbour;

            nodes = new GridNode[Size.x * Size.y];

            for (int i = 0, y = 0; y < Size.y; y++)
            {
                for (int x = 0; x < Size.x; x++, i++)
                {
                    coords.Set(x, y);

                    node = new GridNode(this, i, coords, CalculatePosition(coords, Space.Self));
                    nodes[i] = node;

                    //for (int j = 0; j < DIRECTIONS.Length; j++)
                    //{
                    //    neighbour = GetNodeFromCoords(coords + DIRECTIONS[j]);
                    //    if (neighbour != null)
                    //    {
                    //        neighbour.neighbourgs.Add(node);
                    //        node.neighbourgs.Add(neighbour);
                    //    }
                    //}
                }
            }

            Center = new Vector2Int(Size.x / 2, Size.y / 2);
            CornerTopLeft = new Vector2Int(0, Size.y);
            CornerTopRight = new Vector2Int(Size.x, Size.y);
            CornerBottomRight = new Vector2Int(Size.x, 0);
            CornerBottomLeft = new Vector2Int(0, 0);

            HasNodes = true;
        }

        public void DrawNodes(Color color)
        {
            if (HasNodes == false)
            {
                return;
            }

            foreach (var node in nodes)
            {
                Draw.ingame.CrossXZ(node.WorldPosition, .25f, color);
            }
        }

        public void DrawBorders2(Color color)
        {
            if (isInitialized == false || HasNodes == false)
            {
                return;
            }

            using (Draw.ingame.InLocalSpace(transform))
            {
                for (int y = 0; y < Size.y - 1; y++)
                {
                    Draw.ingame.Line(GetNode(0, y).localPosition, GetNode(0, y + 1).localPosition, color);
                    //Draw.ingame.Line(GetNode(Size.x - 1, y).localPosition, GetNode(Size.x - 1, y + 1).localPosition, color);
                }
                for (int x = 0; x < Size.x - 1; x++)
                {
                    Draw.ingame.Line(GetNode(x, 0).localPosition, GetNode(x + 1, 0).localPosition, color);
                    //Draw.ingame.Line(GetNode(x, Size.y - 1).localPosition, GetNode(x + 1, Size.y - 1).localPosition, color);
                }
            }

        }

        public void DrawBorders(Color color)
        {
            if (isInitialized == false)
            {
                return;
            }

            using (Draw.ingame.InLocalSpace(transform))
            {
                Draw.ingame.Line(
                    CalculatePosition(CornerBottomLeft, Space.Self),
                    CalculatePosition(CornerTopLeft, Space.Self),
                    color);
                Draw.ingame.Line(
                    CalculatePosition(CornerBottomLeft, Space.Self),
                    CalculatePosition(CornerBottomRight, Space.Self),
                    color);
                Draw.ingame.Line(
                    CalculatePosition(CornerTopLeft, Space.Self),
                    CalculatePosition(CornerTopRight, Space.Self),
                    color);
                Draw.ingame.Line(
                    CalculatePosition(CornerTopRight, Space.Self),
                    CalculatePosition(CornerBottomRight, Space.Self),
                    color);
                Draw.ingame.WireRectangleXZ(
                    CalculatePosition(Center, Space.Self) + Vector3.right * NodeSize / 2 + Vector3.forward * NodeSize / 2,
                    NodeSize,
                    color);
            }
        }

        public GridNode GetNode(int x, int y)
        {
            if (InBounds(x, y) && nodes != null)
            {
                return nodes[y * Size.x + x];
            }
            return null;
        }

        public GridNode GetNode(Vector2Int coords)
        {
            if (InBounds(coords) && nodes != null)
            {
                return nodes[coords.y * Size.x + coords.x];
            }
            return null;
        }

        public bool InBounds(int x, int y)
        {
            return
                x >= 0 && x < Size.x &&
                y >= 0 && y < Size.y;
        }

        public bool InBounds(Vector2Int coords)
        {
            return
                coords.x >= 0 && coords.x < Size.x &&
                coords.y >= 0 && coords.y < Size.y;
        }

        public Vector2Int CalculateLocalNodeCoords(Vector3 position, Space fromSpace)
        {
            if (fromSpace == Space.World)
            {
                position = transform.InverseTransformPoint(position);
            }
            return new Vector2Int(
                Mathf.FloorToInt(position.x / NodeSize),
                Mathf.FloorToInt(position.z / NodeSize));
        }

        public Vector3 CalculatePosition(Vector2Int localNodeCoords, Space toSpace)
        {
            Vector3 position = new Vector3(
                localNodeCoords.x * NodeSize,
                0f,
                localNodeCoords.y * NodeSize);
            if (toSpace == Space.World)
            {
                position = transform.TransformPoint(position);
            }
            return position;
        }

        public bool Instersects(Vector2Int lineStart, Vector2Int lineEnd, out Vector2 intersection, out float fraction)
        {
            bool intersects = AABB.IntersectsLine(
                new AABB(CornerBottomLeft.xoy(), CornerTopRight.xoy()),
                lineStart.xoy(),
                lineEnd.xoy(),
                out Vector3 intersectionV3,
                out fraction);
            intersection = intersectionV3.xz();
            return intersects;
        }

    } // end of class
} // end of namespace