﻿using UnityEngine;
using Drawing;
using System.Collections.Generic;
using FDZ.Attributes;
using System;
using System.Net;

namespace FDZ.Projects.OpenWorldSystem
{
    public class GridGroup_Test_B : MonoBehaviour
    {
        [Header("Info")]
        [SerializeField, ReadOnly] private Vector3 mousePosition1;
        [SerializeField, ReadOnly] private Vector2Int globalNodeCoords1;
        private GridNode node1;

        [Space]
        [SerializeField, ReadOnly] private Vector3 mousePosition2;
        [SerializeField, ReadOnly] private Vector2Int globalNodeCoords2;
        private GridNode node2;

        private bool lockOrientation;
        private GridLineSnapAngleMode currentSnapAngleMode;
        private GridLineSnapAngleMode currentSnapAngleModeAux;

        private bool mousePosition1_isValid;

        private void Update()
        {
            var world = GetComponent<OpenWorld>();
            var gridGroup = world.MainGridGroup;

            var cam = Camera.main;
            var camRay = cam.ScreenPointToRay(Input.mousePosition);
            var hit = Physics.Raycast(camRay, out RaycastHit mouseHitInfo, float.MaxValue, -1, QueryTriggerInteraction.Ignore);

            if (hit == false)
            {
                return;
            }

            lockOrientation = Input.GetKey(KeyCode.LeftShift);
            if (lockOrientation == false)
            {
                currentSnapAngleMode = GridLineSnapAngleMode.Any;
                currentSnapAngleModeAux = GridLineSnapAngleMode.Any;
            }

            mousePosition2 = mouseHitInfo.point;
            globalNodeCoords2 = gridGroup.CalculateGlobalNodeCoords(mousePosition2, Space.World);
            node2 = gridGroup.GetNode(globalNodeCoords2);

            if (Input.GetMouseButtonDown(0))
            {
                mousePosition1 = mousePosition2;
                globalNodeCoords1 = gridGroup.CalculateGlobalNodeCoords(mousePosition1, Space.World);
                node1 = gridGroup.GetNode(globalNodeCoords1);
                mousePosition1_isValid = true;

                var step = .05f;

                //world.FlattenTerrainQuadHeight(globalNodeCoords1);
                //var h = world.GetTerrainVertexHeight(globalNodeCoords1);
                //if (Input.GetKey(KeyCode.R))
                //{
                //    h += step;
                //}
                //if (Input.GetKey(KeyCode.F))
                //{
                //    h -= step;
                //}
                //h = Mathf.Round(h / step) * step;
                //world.SetTerrainVertexHeight(globalNodeCoords1, h);
                //world.SetTerrainQuadHeight(globalNodeCoords1, h);
            }

            if (Input.GetKeyDown(KeyCode.H))
            {
                Vector2Int chunkCoords = gridGroup.CalculateGridCoords(globalNodeCoords2);
                Vector2Int localNodeCoords2 = gridGroup.CalculateLocalNodeCoords(globalNodeCoords2);
                int triangle = (localNodeCoords2.y * gridGroup.GridSize.x + localNodeCoords2.x) * 6;
                OpenWorldChunk chunk = world.GetChunk(chunkCoords);
                chunk.terrain.meshDataLODs[0].MakeHole(triangle);
                chunk.terrain.ReassignMeshData();
            }
            else if (Input.GetKeyDown(KeyCode.J))
            {
                Vector2Int chunkCoords = gridGroup.CalculateGridCoords(globalNodeCoords2);
                Vector2Int localNodeCoords2 = gridGroup.CalculateLocalNodeCoords(globalNodeCoords2);
                int triangle = (localNodeCoords2.y * gridGroup.GridSize.x + localNodeCoords2.x) * 6;
                OpenWorldChunk chunk = world.GetChunk(chunkCoords);
                chunk.terrain.meshDataLODs[0].RemoveAllQuadHoles();
                chunk.terrain.ReassignMeshData();
            }

            if (Input.GetMouseButton(0) && mousePosition1_isValid)
            {
                var line = GridUtils.CalculateLine(globalNodeCoords1, globalNodeCoords2, currentSnapAngleMode);
                if (!lockOrientation)
                {
                    currentSnapAngleMode = (GridLineSnapAngleMode)line.orientation;
                }

                GridUtils.ForEachNode(gridGroup, line, (curNode, nextNode) =>
                {
                    if (nextNode != null)
                        Draw.ingame.Line(curNode.WorldPosition, nextNode.WorldPosition);
                });
            }
            else
            {
                mousePosition1_isValid = false;
            }

            if (node2 != null)
            {
                using (Draw.ingame.InLocalSpace(gridGroup.transform))
                {
                    Draw.ingame.CrossXZ(node2.WorldPosition, gridGroup.NodeSize / 2);
                }
            }

            GridUtils.DrawGridNodeBorders(gridGroup, globalNodeCoords2);
        }
    } // class
} // namespace