﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Drawing;

namespace FDZ.Projects.OpenWorldSystem
{
    // Change this class to be a GridGroup
    // Essentially a World would be end up being a GridGroup and the same for a Boat

    public class GridGroup : MonoBehaviour
    {
        private Vector2Int gridSize;
        private Vector2Int gridSizeHalf;
        private float nodeSize;
        private float nodeSizeDiagonal;

        private Dictionary<Vector2Int, Grid> gridsDictionary = new Dictionary<Vector2Int, Grid>();

        /**************************************************************/

        public Vector2Int GridSize => gridSize;

        public Vector2Int GridSizeHalf => gridSizeHalf;

        public float NodeSize => nodeSize;

        public float NodeSizeDiagonal => nodeSizeDiagonal;

        /**************************************************************/

        public bool IsInitialized { get; private set; }

        public void Init(Vector2Int pGridSize, float pNodeSize)
        {
            if (IsInitialized)
            {
                return;
            }
            IsInitialized = true;

            gridSize = pGridSize;
            gridSize.x = Utils.RoundToEvenInt(gridSize.x);
            gridSize.y = Utils.RoundToEvenInt(gridSize.y);
            gridSizeHalf = gridSize / 2;

            nodeSize = pNodeSize;
            nodeSize = Mathf.Max(nodeSize, .1f);
            nodeSizeDiagonal = Mathf.Sqrt(nodeSize * nodeSize + nodeSize * nodeSize);
        }

        /**************************************************************/
        public Grid Create(Vector2Int gridCoords)
        {
            if (gridsDictionary.ContainsKey(gridCoords))
            {
                return null;
            }
            var grid = new GameObject($"Grid ({gridCoords.x}, {gridCoords.y})").AddComponent<Grid>();
            grid.Init(this, gridCoords, gridSize, nodeSize);
            gridsDictionary.Add(gridCoords, grid);
            return grid;
        }

        public Grid GetGrid(Vector2Int coords)
        {
            gridsDictionary.TryGetValue(coords, out Grid grid);
            return grid;
        }

        public void RemoveAndDestroyGrid(Vector2Int coords)
        {
            if (gridsDictionary.TryGetValue(coords, out Grid grid))
            {
                gridsDictionary.Remove(coords);
                Destroy(grid.gameObject);
            }
        }

        /**************************************************************/

        public GridNode GetNode(Vector2Int globalNodeCoords)
        {
            Vector2Int gridCoords = CalculateGridCoords(globalNodeCoords);
            Grid grid = GetGrid(gridCoords);
            if (grid == null)
            {
                return null;
            }
            Vector2Int nodeCoords = CalculateLocalNodeCoords(grid.Coords, globalNodeCoords);
            return grid.GetNode(nodeCoords);
        }

        /**************************************************************/

        public void DrawNodes(Color color)
        {
            foreach (var keyValuePair in gridsDictionary)
            {
                keyValuePair.Value.DrawNodes(color);
            }
        }

        public void DrawBorders(Color color)
        {
            foreach (var keyValuePair in gridsDictionary)
            {
                keyValuePair.Value.DrawBorders(color);
            }
        }

        /**************************************************************/

        public Vector2Int CalculateGridCoords(Vector2Int globalNodeCoords)
        {
            ;
            return new Vector2Int(
                Mathf.FloorToInt((float)globalNodeCoords.x / gridSize.x),
                Mathf.FloorToInt((float)globalNodeCoords.y / gridSize.y));
        }

        public Vector2Int CalculateGridCoords(Vector3 position, Space fromSpace)
        {
            return CalculateGridCoords(CalculateGlobalNodeCoords(position, fromSpace));
        }

        public Vector2Int CalculateGlobalNodeCoords(Vector3 position, Space fromSpace)
        {
            if (fromSpace == Space.World)
            {
                position = transform.InverseTransformPoint(position);
            }
            return new Vector2Int(
                Mathf.FloorToInt(position.x / nodeSize),
                Mathf.FloorToInt(position.z / nodeSize));
        }

        public Vector2Int CalculateLocalNodeCoords(Vector2Int gridCoords, Vector2Int globalNodeCoords)
        {
            return new Vector2Int(
                globalNodeCoords.x - gridCoords.x * gridSize.x,
                globalNodeCoords.y - gridCoords.y * gridSize.y);
        }

        public Vector2Int CalculateLocalNodeCoords(Vector2Int globalNodeCoords)
        {
            return CalculateLocalNodeCoords(CalculateGridCoords(globalNodeCoords), globalNodeCoords);
        }

        public Vector3 CalculatePosition(Vector2Int globalNodeCoords, Space toSpace)
        {
            Vector3 position = new Vector3(
                globalNodeCoords.x * nodeSize,
                0f,
                globalNodeCoords.y * nodeSize);
            if (toSpace == Space.World)
            {
                position = transform.TransformPoint(position);
            }
            return position;
        }

        /**************************************************************/

        public void ForEachGridInLine(Vector2Int from, Vector2Int to, Action<Grid> doSomething)
        {
            var min = Vector2Int.Min(from, to);
            var max = Vector2Int.Max(from, to);
            for (int y = min.y; y <= max.y; y++)
            {
                for (int x = min.x; x <= max.x; x++)
                {
                    if (gridsDictionary.TryGetValue(new Vector2Int(x, y), out Grid grid))
                    {
                        if (grid.HasNodes)
                        {
                            doSomething.Invoke(grid);
                        }
                    }
                }
            }
        }

        public void ForEachGridInBox(Vector2Int r1, Vector2Int r2, Vector2Int r3, Vector2Int r4, Action<Grid> doSomething)
        {
            var min = Vector2Int.Min(r1, r2);
            min = Vector2Int.Min(min, r3);
            min = Vector2Int.Min(min, r4);

            var max = Vector2Int.Max(r1, r2);
            max = Vector2Int.Max(max, r3);
            max = Vector2Int.Max(max, r4);

            for (int y = min.y; y <= max.y; y++)
            {
                for (int x = min.x; x <= max.x; x++)
                {
                    if (gridsDictionary.TryGetValue(new Vector2Int(x, y), out Grid grid))
                    {
                        if (grid.HasNodes)
                        {
                            doSomething.Invoke(grid);
                        }
                    }
                }
            }
        }
    } // class
} // namespace