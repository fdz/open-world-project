﻿using System.Linq;
using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
    public class GridObjectPlacer : MonoBehaviour
    {
        public GridGroup gridGroup;

        private GridObjectPlacerTool curTool;
        private GridObjectPlacerTool[] tools;
        private int index;

        private void Awake()
        {
            tools = GetComponents<GridObjectPlacerTool>();
            SetCurrentTool(tools[index]);
        }

        private void Update()
        {
            float axis = UtilsInput.GetAxisDown(KeyCode.Comma, KeyCode.Period);
            if (axis != 0)
            {
                index += System.Math.Sign(axis);
                if (index < 0)
                {
                    index = tools.Length - 1;
                }
                else if (index >= tools.Length)
                {
                    index = 0;
                }
                SetCurrentTool(tools[index]);
            }

            Camera cam = Camera.main;
            Ray camRay = cam.ScreenPointToRay(Input.mousePosition);
            bool mouseHit = Physics.Raycast(camRay, out RaycastHit mouseHitInfo, float.MaxValue, -1, QueryTriggerInteraction.Ignore);
            if (mouseHit)
            {
                curTool.Tick(mouseHitInfo, gridGroup);
            }
        }

        private void SetCurrentTool(GridObjectPlacerTool tool)
        {
            if (tool != null && tool != curTool)
            {
                if (curTool != null)
                {
                    curTool.Deselect();
                }
                curTool = tool;
                curTool.Select();
            }
        }
    } // class
} // namespace