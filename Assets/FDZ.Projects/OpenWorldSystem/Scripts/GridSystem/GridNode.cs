﻿using UnityEngine;
using System.Collections.Generic;

namespace FDZ.Projects.OpenWorldSystem
{
    public class GridNode
    {
        public readonly Grid grid;
        public readonly int index;
        public readonly Vector2Int coords;
        public bool enabled;
        public List<object> objects;
        public HashSet<GridNode> neighbourgs;
        //public Dictionary<GridNode, GridNodeConnetcion> neighbourgs;

        public Vector3 localPosition;

        public Vector3 WorldPosition
        {
            get => grid.transform.TransformPoint(localPosition);
        }

        public GridNode(Grid grid, int index, Vector2Int coords, Vector3 localPosition)
        {
            this.grid = grid;
            this.index = index;
            this.coords = coords;
            this.localPosition = localPosition;
            objects = new List<object>();
            neighbourgs = new HashSet<GridNode>();
        }
    }
}