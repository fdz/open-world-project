﻿using UnityEngine;
using Drawing;
using System.Collections.Generic;
using System.Linq;
using FDZ.Attributes;

namespace FDZ.Projects.OpenWorldSystem
{
	public abstract class GridObjectPlacerTool : MonoBehaviour
    {
        [SerializeField, ReadOnly] private bool isSelected;
        private List<GameObject> instances = new List<GameObject>();
        private bool cancel;
        private bool place;

        protected Vector3 mousePositionStart;
        protected Vector3 mousePositionEnd;
        protected GridGroup currentGridGroup;

        protected virtual void OnSelected() { }
        protected virtual void OnDeselected() { }
        
        protected virtual void OnCancel() { }
        protected virtual void OnPlace() { }

        protected virtual void OnJustPressed() { }
        protected virtual void OnJustReleased() { }
        protected virtual void WhilePressed() { }
        protected virtual void WhileReleased() { }

        public void Select()
        {
            isSelected = true;
            Cancel();
            OnSelected();
        }

        public void Deselect()
        {
            isSelected = false;
            OnDeselected();
        }

        private void Cancel()
        {
            if (!cancel)
            {
                cancel = true;
                place = false;
                DestroyAllPrefabsInstances();
                OnCancel();
            }
        }

        private void Place()
        {
            if (place)
            {
                place = false;
                OnPlace();
                KeepPrefabInstances();
            }
        }

        // TODO: auto get the grid for open world stuff
        public void Tick(RaycastHit mouseHitInfo, GridGroup gridGroup)
        {
            currentGridGroup = gridGroup;

            if (cancel)
            {
                if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
                {
                    cancel = false;
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButton(1))
            {
                Cancel();
            }
            
            if (!cancel)
            {
                mousePositionEnd = mouseHitInfo.point;
                if (Input.GetMouseButtonDown(0))
                {
                    mousePositionStart = mousePositionEnd;
                    OnJustPressed();
                    place = true;
                }
                else if (Input.GetMouseButton(0))
                {
                    WhilePressed();
                    place = true;
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    OnJustReleased();
                    Place();
                }
                else
                {
                    WhileReleased();
                    Place();
                }
            }
        }

        protected void KeepPrefabInstances()
        {
            instances.Clear();
        }

        protected GameObject InstantiatePrefab(GameObject original)
        {
            GameObject instance = Instantiate(original);
            instances.Add(instance);
            return instance;
        }

        protected void DestroyAllPrefabsInstances()
        {
            while (instances.Count != 0)
            {
                GameObject instance = instances.First();
                instances.Remove(instance);
                Destroy(instance);
            }
        }
    } // class
} // namespace