﻿using Drawing;
using System;
using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
    public class GridObjectPlacerTool_Walls : GridObjectPlacerTool
    {
        ////////////////////////////////////////////////////////////////////////////////////
        /*
        public GameObject prefabStraight;
        public GameObject prefabDiagonal;

        GridLineSnapAngleMode curSnapAngleMode;
        GridLineSnapAngleMode auxSnapAngleMode;

        private Vector2Int startCoords;
        private Vector2Int endCoords;
        private Vector2Int prevEndCoords;

        protected override void OnPlace()
        {
            PlaceInLine(startCoords, endCoords);
        }

        protected override void WhilePressed()
        {
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                curSnapAngleMode = GridLineSnapAngleMode.Any;
                auxSnapAngleMode = GridLineSnapAngleMode.Any;
            }

            GridLine mouseLine = GridUtils.CalculateLine(mousePositionStart, mousePositionEnd, auxSnapAngleMode);

            GridLineInt mouseLineSnap = currentGridGroup.CalculateLine(
                currentGridGroup.CalculateGlobalNodeCoords(mousePositionStart, Space.World),
                currentGridGroup.CalculateGlobalNodeCoords(mousePositionEnd, Space.World),
                curSnapAngleMode);

            startCoords = mouseLineSnap.start;
            endCoords = mouseLineSnap.end;

            //GridUtils.DrawLine(mouseLine, Color.cyan);
            GridUtils.DrawLine(currentGridGroup.TransformLine(mouseLineSnap), Color.cyan);

            if (curSnapAngleMode == GridLineSnapAngleMode.Any)
            {
                curSnapAngleMode = (GridLineSnapAngleMode)mouseLineSnap.orientation;
                auxSnapAngleMode = (GridLineSnapAngleMode)mouseLine.orientation;
            }

            currentGridGroup.ForEachGridInLine(
                currentGridGroup.CalculateGridCoords(mouseLine.start, Space.World),
                currentGridGroup.CalculateGridCoords(mouseLine.end, Space.World),
            (grid) =>
            {
                grid.DrawBorders(Color.cyan);
            });

            if (prevEndCoords != endCoords)
            {
                DestroyAllPrefabsInstances();
                PlaceInLine(startCoords, endCoords);
            }

            prevEndCoords = endCoords;
        }

        private void PlaceInLine(Vector2Int startGlobalNodeCoords, Vector2Int endGlobalNodeCoords)
        {
            Vector2Int direction = (endGlobalNodeCoords - startGlobalNodeCoords);

            if (direction == Vector2Int.zero)
            {
                return;
            }

            direction.x = Math.Sign(direction.x);
            direction.y = Math.Sign(direction.y);

            bool isStraight = (Mathf.Abs(direction.x) + Mathf.Abs(direction.y)) == 1;
            GameObject prefab = isStraight ? prefabStraight : prefabDiagonal;

            for (
                Vector2Int fromGlobalNodeCoords = startGlobalNodeCoords, toGlobalNodeCoords = fromGlobalNodeCoords + direction;
                fromGlobalNodeCoords != endGlobalNodeCoords;
                fromGlobalNodeCoords += direction, toGlobalNodeCoords += direction)
            {
                Grid g1 = currentGridGroup.GetGrid(currentGridGroup.CalculateGridCoords(fromGlobalNodeCoords));
                Grid g2 = currentGridGroup.GetGrid(currentGridGroup.CalculateGridCoords(toGlobalNodeCoords));

                if (g1 == null || g2 == null)
                {
                    continue;
                }

                Vector2Int g1_fromNodeCoords = currentGridGroup.CalculateLocalNodeCoords(g1.Coords, fromGlobalNodeCoords);
                Vector2Int g2_toNodeCoords = currentGridGroup.CalculateLocalNodeCoords(g2.Coords, toGlobalNodeCoords);

                if (!g1.InBounds(g1_fromNodeCoords) || !g2.InBounds(g2_toNodeCoords))
                {
                    continue;
                }

                Vector2Int g1_toNodeCoords = currentGridGroup.CalculateLocalNodeCoords(g1.Coords, toGlobalNodeCoords);

                GameObject instance = InstantiatePrefab(prefab);
                instance.SetActive(true);
                instance.transform.SetParent(g1.transform, true);

                Vector3 fromPosition = g1.CalculatePosition(g1_fromNodeCoords, Space.Self);
                Vector3 toPosition = g1.CalculatePosition(g1_toNodeCoords, Space.Self);

                instance.transform.localPosition = fromPosition;
                instance.transform.localRotation = Quaternion.LookRotation((toPosition - fromPosition).xoz().normalized);
            }
        }
        */
        ////////////////////////////////////////////////////////////////////////////////////
        /*
        public GameObject prefabStraight;
        public GameObject prefabDiagonal;

        private Vector2Int mouseCoordsStart;
        private Vector2Int mouseCoordsEnd;

        private Vector2Int endCoords;
        private GridOrientation lineOrientation;

        private void Awake()
        {
            PrefabPoolManager.Register(prefabStraight);
            PrefabPoolManager.Register(prefabDiagonal);
        }

        protected override void OnSelected()
        {
            endCoords = Vector2Int.one * int.MaxValue;
        }

        protected override void OnDeselected()
        {
            StoreAllPrefabInstances();
        }

        protected override void OnCancel()
        {
            StoreAllPrefabInstances();
        }

        protected override void OnMouseLeftButtonDown(Vector3 mousePosition)
        {
            mouseCoordsStart = World.Instance.PositionToCoords(mousePosition);
        }

        protected override void OnMouseLeftButtonUp(Vector3 mousePosition)
        {
            KeepPrefabInstances();
            endCoords = Vector2Int.one * int.MaxValue;
        }

        protected override void OnMouseLeftButtonBeingPressed(Vector3 mousePosition)
        {
            mouseCoordsEnd = World.Instance.PositionToCoords(mousePosition);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                BoxPlacement();
            }
            else
            {
                LinePlacement();
            }
        }

        private void BoxPlacement()
        {
            World.Instance.GetNodesAtBoxCorners(
                mouseCoordsStart,
                mouseCoordsEnd,
                (GridGetLineMode)lineOrientation,
                out GridOrientation boxOrientation,
                out Vector2Int topLeftCorner,
                out Vector2Int bottomLeftCorner,
                out Vector2Int bottomRightCorner,
                out Vector2Int topRightCorner);

            //Debug.Log((topLeftCorner == topRightCorner || topLeftCorner == bottomLeftCorner) ? "Box -> Line" : "Box");
            Draw.ingame.Line(World.Instance.CoordsToPosition(topLeftCorner), World.Instance.CoordsToPosition(topRightCorner), Color.red);
            Draw.ingame.Line(World.Instance.CoordsToPosition(topLeftCorner), World.Instance.CoordsToPosition(bottomLeftCorner), Color.red);
            Draw.ingame.Line(World.Instance.CoordsToPosition(topRightCorner), World.Instance.CoordsToPosition(bottomRightCorner), Color.red);
            Draw.ingame.Line(World.Instance.CoordsToPosition(bottomLeftCorner), World.Instance.CoordsToPosition(bottomRightCorner), Color.red);

            if (bottomRightCorner != endCoords)
            {
                StoreAllPrefabInstances();

                if (topLeftCorner == topRightCorner || topLeftCorner == bottomLeftCorner)
                {
                    PlaceInLine(topLeftCorner, bottomLeftCorner);
                }
                else
                {
                    PlaceInLine(topLeftCorner, topRightCorner);
                    PlaceInLine(topLeftCorner, bottomLeftCorner);
                    PlaceInLine(topRightCorner, bottomRightCorner);
                    PlaceInLine(bottomLeftCorner, bottomRightCorner);
                }
            }

            endCoords = bottomRightCorner;
        }

        private void LinePlacement()
        {
            Vector2Int lineEndingCoordsAux = World.Instance.GetCoordAtLineEnd(
                mouseCoordsStart,
                mouseCoordsEnd,
                GridGetLineMode.Any,
                out lineOrientation);

            //Debug.Log("Line");
            Draw.ingame.Line(World.Instance.CoordsToPosition(mouseCoordsStart), World.Instance.CoordsToPosition(lineEndingCoordsAux), Color.red);

            if (lineEndingCoordsAux != endCoords)
            {
                StoreAllPrefabInstances();
                if (lineEndingCoordsAux != mouseCoordsStart)
                {
                    PlaceInLine(mouseCoordsStart, lineEndingCoordsAux);
                }
            }

            endCoords = lineEndingCoordsAux;
        }

        private void PlaceInLine(Vector2Int startCoords, Vector2Int endCoords)
        {
            Vector2Int direction = (endCoords - startCoords);

            if (direction == Vector2Int.zero)
            {
                return;
            }

            direction.x = Math.Sign(direction.x);
            direction.y = Math.Sign(direction.y);

            bool isStraight = (Mathf.Abs(direction.x) + Mathf.Abs(direction.y)) == 1;
            GameObject prefab = isStraight ? prefabStraight : prefabDiagonal;

            for (Vector2Int curCoords = startCoords; curCoords != endCoords; curCoords += direction)
            {
                GridNode curNode = World.Instance.GetNode(curCoords);
                GridNode nextNode = World.Instance.GetNode(curCoords + direction);
                if (curNode != null && nextNode != null)
                {
                    GameObject instance = GetPrefabInstance(prefab);
                    instance.SetActive(true);
                    instance.transform.position = curNode.WorldPosition;
                    instance.transform.forward = (nextNode.WorldPosition - curNode.WorldPosition).x0z().normalized;
                    //instance.transform.SetParent(grid.transform, true);
                }
            }
        }
        */
    } // class
} // namespace