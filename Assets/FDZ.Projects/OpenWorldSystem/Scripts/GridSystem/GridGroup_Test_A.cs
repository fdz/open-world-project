﻿using UnityEngine;
using Drawing;
using System.Collections.Generic;
using FDZ.Attributes;
using System;
using System.Net;

namespace FDZ.Projects.OpenWorldSystem
{
    public class GridGroup_Test_A : MonoBehaviour
    {
        [Header("Info")]
        [SerializeField, ReadOnly] private Vector3 mousePosition1;
        [SerializeField, ReadOnly] private Vector3 mousePosition2;
        [Space]
        [SerializeField, ReadOnly] private Vector2Int globalNodeCoords;
        [SerializeField, ReadOnly] private Vector2Int gridCoords;
        [Space]
        [SerializeField, ReadOnly] private Vector2Int localNodeCoordsWithGroup;
        [SerializeField, ReadOnly] private Vector2Int localNodeCoordsWithGrid;
        [SerializeField, ReadOnly] private Vector2Int foundNodeCoords;

        private bool lockOrientation;
        private GridLineSnapAngleMode currentSnapAngleMode;
        private GridLineSnapAngleMode currentSnapAngleModeAux;

        private void Update()
        {
            var gridGroup = GetComponent<OpenWorld>().MainGridGroup;

            var cam = Camera.main;
            var camRay = cam.ScreenPointToRay(Input.mousePosition);
            var hit = Physics.Raycast(camRay, out RaycastHit mouseHitInfo, float.MaxValue, -1, QueryTriggerInteraction.Ignore);

            if (hit == false)
            {
                return;
            }

            lockOrientation = Input.GetKey(KeyCode.LeftShift);
            if (lockOrientation == false)
            {
                currentSnapAngleMode = GridLineSnapAngleMode.Any;
                currentSnapAngleModeAux = GridLineSnapAngleMode.Any;
            }

            mousePosition2 = mouseHitInfo.point;
            if (Input.GetMouseButtonDown(0))
            {
                mousePosition1 = mousePosition2;
            }

            globalNodeCoords = gridGroup.CalculateGlobalNodeCoords(mousePosition2, Space.World);

            using (Draw.ingame.InLocalSpace(gridGroup.transform))
            {
                Draw.ingame.CrossXZ(gridGroup.transform.InverseTransformPoint(mousePosition2), .5f, Color.cyan);
                //Draw.ingame.CrossXZ(gridGroup.CalculatePosition(globalNodeCoords, Space.Self), gridGroup.NodeSize * .5f, Color.red);
            }

            gridCoords = gridGroup.CalculateGridCoords(globalNodeCoords);
            var gridAux = gridGroup.GetGrid(gridCoords);
            if (gridAux != null)
            {
                localNodeCoordsWithGroup = gridGroup.CalculateLocalNodeCoords(gridCoords, globalNodeCoords);
                localNodeCoordsWithGrid = gridAux.CalculateLocalNodeCoords(mousePosition2, Space.World);

                using (Draw.ingame.InLocalSpace(gridAux.transform))
                {
                    var node = gridAux.GetNode(localNodeCoordsWithGrid);
                    if (node != null)
                    {
                        foundNodeCoords = node.coords;
                        Draw.ingame.CrossXZ(node.localPosition, .5f, Color.red);
                    }
                }
            }

            if (Input.GetMouseButton(0))
            {
                Vector2Int globalNodeCoords1 = gridGroup.CalculateGlobalNodeCoords(mousePosition1, Space.World);
                Vector2Int globalNodeCoords2 = gridGroup.CalculateGlobalNodeCoords(mousePosition2, Space.World);

                GridLine2DInt line = GridUtils.CalculateLine(globalNodeCoords1, globalNodeCoords2, currentSnapAngleMode);
                GridUtils.DrawLine(gridGroup, line, Color.red);

                if (currentSnapAngleMode == GridLineSnapAngleMode.Any)
                {
                    currentSnapAngleMode = (GridLineSnapAngleMode)line.orientation;
                    currentSnapAngleModeAux = (GridLineSnapAngleMode)line.orientation;
                }
            }
        }
    } // class
} // namespace