﻿using Drawing;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
	public class GridObjectPlacerTool_FloorSquares : GridObjectPlacerTool
    {
        /*
        public GameObject currentPrefab;

        private Vector2Int mouseCoordsStart;
        private Vector2Int mouseCoordsEnd;

        private List<GridNode> selectedNodes = new List<GridNode>();
        private GridNode prevLastNode;

        private void Awake()
        {
            PrefabPoolManager.Register(currentPrefab);
        }

        protected override void OnSelected()
        {
        }

        protected override void OnDeselected()
        {
            if (currentPrefab != null)
            {
                StoreAllPrefabInstances();
            }
            prevLastNode = null;
        }

        protected override void OnCancel()
        {
            if (currentPrefab != null)
            {
                StoreAllPrefabInstances();
            }
        }

        protected override void OnMouseLeftButtonDown(Vector3 mousePosition)
        {
            //mouseCoordsStart = grid.CoordsFromWorldPosition(mousePosition);
        }

        protected override void OnMouseLeftButtonUp(Vector3 mousePosition)
        {
            KeepPrefabInstances();
            prevLastNode = null;
        }

        protected override void OnMouseLeftButtonBeingPressed(Vector3 mousePosition)
        {
            mouseCoordsEnd = grid.CoordsFromWorldPosition(mousePosition);
            selectedNodes.Clear();
            GridNode curLastNode;
            grid.GetNodesInRectangle(selectedNodes, mouseCoordsStart, mouseCoordsEnd, false);
            //if (selectedNodes.Count == 0)
            //{
            //    curLastNode = grid.GetNodeFromCoords(mouseCoordsStart);
            //    if (curLastNode != null)
            //    {
            //        selectedNodes.Add(curLastNode);
            //    }
            //}
            var ns = new List<GridNode>();
            grid.GetNodesInRectangle2(ns, mouseCoordsStart, mouseCoordsEnd, out bool isStraightLine);
            foreach (var n in ns)
            {
                Draw.CrossXZ(n.WorldPosition, grid.NodeSize / 4);
            }
            return;
            if (selectedNodes.Count != 0)
            {
                curLastNode = selectedNodes.Last();
            }
            else
            {
                curLastNode = null;
            }
            if (curLastNode != prevLastNode)
            {
                StoreAllPrefabInstances();
                for (int i = 0; i < selectedNodes.Count; i++)
                {
                    GridNode curNode = selectedNodes[i];
                    GameObject instance = GetPrefabInstance(currentPrefab);
                    instance.SetActive(true);
                    instance.transform.SetParent(grid.transform, true);
                    instance.transform.localPosition = curNode.localPosition;
                    instance.transform.localRotation = Quaternion.identity;
                }
            }
            prevLastNode = curLastNode;
        }
        */
    } // class
} // namespace