﻿using System.Linq;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
    public class OpenWorldChunk
    {
        private static List<OpenWorldChunk> chunksToBeGenerated = new List<OpenWorldChunk>();
        private static List<OpenWorldChunk> chunksBeingGenerated = new List<OpenWorldChunk>();

        public static void UpdateGeneration()
        {
            if (chunksBeingGenerated.Count != 0)
            {
                var removed = true;
                while (removed)
                {
                    removed = false;
                    foreach (var chunk in chunksBeingGenerated)
                    {
                        if (chunk.IsGenerated)
                        {
                            chunksBeingGenerated.Remove(chunk);
                            chunk.ApplyHeightMaps();
                            chunk.terrain.ReassignMeshData();
                            removed = true;
                            break;
                        }
                    }
                }
            }

            if (chunksToBeGenerated.Count != 0 && chunksBeingGenerated.Count < 10)
            {
                var chunk = chunksToBeGenerated[0];
                Thread thread = new Thread(chunk.Generate);
                thread.Start();
                if (thread.ThreadState != ThreadState.Running)
                {
                    Debug.LogWarning($"Thread not started! Generating mesh data on primary thread! yucks!");
                    chunk.Generate();
                }
                chunksToBeGenerated.Remove(chunk);
                chunksBeingGenerated.Add(chunk);
            }
        }

        /***************************************************************************************************************/

        public readonly OpenWorld owner;
        public readonly Vector2Int coords;
        public readonly Grid grid;
        public readonly TerrainChunk terrain;

        public readonly float[,] baseTerrainHeightMap;
        public readonly float[,] additiveTerrainHeightMap;

        public int terrainLODIndex;

        private bool isQueuedForGeneration;

        public bool IsActive { get; private set; }

        public bool IsGenerated { get; private set; }

        public OpenWorldChunk(OpenWorld owner, Vector2Int coords)
        {
            this.owner = owner;
            this.coords = coords;

            baseTerrainHeightMap = new float[owner.TerrainNoiseParams.width, owner.TerrainNoiseParams.height];
            additiveTerrainHeightMap = new float[owner.TerrainNoiseParams.width, owner.TerrainNoiseParams.height];

            grid = owner.MainGridGroup.GetGrid(coords);
            if (grid == null)
            {
                grid = owner.MainGridGroup.Create(coords);
            }
            SetActive(false);

            terrain = TerrainChunk.CreateGameObject(owner.TerrainMaterial);
            terrain.transform.SetParent(grid.transform);
            terrain.transform.localPosition = Vector3.zero;
            terrain.transform.localRotation = Quaternion.identity;
            terrain.Init(owner.MainGridGroup.NodeSize, owner.MainGridGroup.GridSize.x, owner.terrainLODs_innerCellsPerLine);
        }

        public void StartGeneration()
        {
            if (!isQueuedForGeneration)
            {
                isQueuedForGeneration = true;
                chunksToBeGenerated.Add(this);
            }
        }

        public void Destroy()
        {
            owner.MainGridGroup.RemoveAndDestroyGrid(grid.Coords);
        }

        public void SetActive(bool value)
        {
            IsActive = value;
            grid.gameObject.SetActive(value);
        }

        private float timer;

        public void UpdateTerrain()
        {
            if (terrain.CurrentMeshDataIndex != terrainLODIndex)
            {
                if (terrain.meshDataLODs[terrainLODIndex].isValid)
                {
                    terrain.AssignMeshData(terrainLODIndex);
                }
                else
                {
                    for (int i = terrainLODIndex + 1; i < terrain.meshDataLODs.Length; i++)
                    {
                        if (terrain.CurrentMeshDataIndex == i)
                        {
                            break;
                        }

                        if (terrain.meshDataLODs[i].isValid)
                        {
                            terrain.AssignMeshData(i);
                            break;
                        }
                    }
                }
            }

            timer += Time.deltaTime;
            if (timer >= .5f)
            {
                timer = 0;

                Vector2Int neighbourDir;
                TerrainChunk neighbourChunk;
                
                neighbourDir = TerrainChunk.NEIGHBOUR_RIGHT;
                neighbourChunk = owner.GetChunk(coords + neighbourDir)?.terrain;
                if (neighbourChunk != null)
                {
                    terrain.SetNeighbour(neighbourDir, neighbourChunk);
                    //neighbourChunk.SetNeighbour(-neighbourDir, terrain);
                }

                neighbourDir = TerrainChunk.NEIGHBOUR_LEFT;
                neighbourChunk = owner.GetChunk(coords + neighbourDir)?.terrain;
                if (neighbourChunk != null)
                {
                    terrain.SetNeighbour(neighbourDir, neighbourChunk);
                    //neighbourChunk.SetNeighbour(-neighbourDir, terrain);
                }

                neighbourDir = TerrainChunk.NEIGHBOUR_TOP_CENTER;
                neighbourChunk = owner.GetChunk(coords + neighbourDir)?.terrain;
                if (neighbourChunk != null)
                {
                    terrain.SetNeighbour(neighbourDir, neighbourChunk);
                    //neighbourChunk.SetNeighbour(-neighbourDir, terrain);
                }

                neighbourDir = TerrainChunk.NEIGHBOUR_BOTTOM_CENTER;
                neighbourChunk = owner.GetChunk(coords + neighbourDir)?.terrain;
                if (neighbourChunk != null)
                {
                    terrain.SetNeighbour(neighbourDir, neighbourChunk);
                    //neighbourChunk.SetNeighbour(-neighbourDir, terrain);
                }

                terrain.AlingEdgesToNeighbours();
                terrain.ReassignMeshData();
            }
        }

        private void Generate()
        {
            for (int i = terrain.meshDataLODs.Length - 1; i >= 0; i--)
            {
                terrain.GenerateMeshData(i);
            }

            if (grid.HasNodes == false)
            {
                grid.GenerateNodes();
            }

            var newTerrainNoiseParams = owner.TerrainNoiseParams;
            newTerrainNoiseParams.offset = grid.Coords * (newTerrainNoiseParams.width - 1);
            newTerrainNoiseParams.curve = new AnimationCurve(newTerrainNoiseParams.curve.keys);

            TerrainGenerationUtils.GenerateNoiseMap(baseTerrainHeightMap, newTerrainNoiseParams);

            ApplyHeightMaps();

            IsGenerated = true;
        }

        public float GetAdditiveHeightMap(int x, int y)
        {
            return additiveTerrainHeightMap[x, y];
        }

        public void SetAddivetHeightMap(int x, int y, float height)
        {
            additiveTerrainHeightMap[x, y] = height;
        }

        public void ApplyHeightMaps()
        {
            int size = baseTerrainHeightMap.GetLength(0);
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    var height = baseTerrainHeightMap[x, y] + additiveTerrainHeightMap[x, y];
                    terrain.SetHeight(x, y, height);
                    if (grid.InBounds(x, y))
                    {
                        grid.GetNode(x, y).localPosition.y = height;
                    }
                }
            }
        }

    } // class
} // namespace