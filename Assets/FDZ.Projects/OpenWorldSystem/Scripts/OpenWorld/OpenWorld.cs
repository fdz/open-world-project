﻿using FDZ.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FDZ.Projects.OpenWorldSystem
{
    public class OpenWorld : MonoBehaviour
    {
#if UNITY_EDITOR
        public static List<int> CalculatePosibleInnerCellsPerLine(int cellsPerLine)
        {
            var l = new List<int>();
            for (int i = 2; i <= cellsPerLine; i++)
            {
                if (cellsPerLine % i == 0)
                {
                    l.Add(i);
                }
            }
            return l;
        }


        [CustomEditor(typeof(OpenWorld))]
        private class OpenWorldEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                EditorGUILayout.Separator();

                var target = this.target as OpenWorld;

                var terrainsLODs_foldoutValue = EditorPrefs.GetBool("FDZ_OpenWorldProject_OpenWorldEditor_terrainLODs_foldoutValue", false);
                terrainsLODs_foldoutValue = EditorGUILayout.Foldout(terrainsLODs_foldoutValue, "Terrain LODs");
                EditorPrefs.SetBool("FDZ_OpenWorldProject_OpenWorldEditor_terrainLODs_foldoutValue", terrainsLODs_foldoutValue);

                if (terrainsLODs_foldoutValue)
                {
                    EditorGUI.indentLevel++;
                    {
                        SerializedProperty terrainLODs = serializedObject.FindProperty("terrainLODs");
                        EditorGUI.BeginChangeCheck();
                        {
                            var innerCellsPerLineOptionsValues = CalculatePosibleInnerCellsPerLine(target.mainGridSize).ToArray();
                            var innerCellsPerLineOptionsDisplayed = innerCellsPerLineOptionsValues.Select(v => v.ToString()).ToArray();

                            float columnA_sizePercent = .5f;
                            float columnB_sizePercent = .5f;
                            float columnB_offset = 22;

                            // HEADER
                            EditorGUILayout.BeginHorizontal();
                            {
                                var rect = EditorGUILayout.GetControlRect();
                                var rectWidth = rect.width;

                                // COLUMN A
                                rect.width = rectWidth * columnA_sizePercent;
                                EditorGUI.LabelField(rect, "Range");

                                // COLUMN B
                                rect.x += rect.width - columnB_offset;
                                rect.width = rectWidth * columnB_sizePercent;
                                EditorGUI.LabelField(rect, "Inner Cells Per Line");
                            }
                            EditorGUILayout.EndHorizontal();

                            // ELEMENTS
                            for (int i = 0; i < terrainLODs.arraySize; i++)
                            {
                                EditorGUILayout.BeginHorizontal();
                                {
                                    var rect = EditorGUILayout.GetControlRect();
                                    var rectWidth = rect.width;

                                    SerializedProperty terrainLOD = terrainLODs.GetArrayElementAtIndex(i);
                                    SerializedProperty range = terrainLOD.FindPropertyRelative("range");
                                    SerializedProperty cells = terrainLOD.FindPropertyRelative("innerCellsPerLine");

                                    // COLUMN A
                                    rect.width = rectWidth * columnA_sizePercent;
                                    range.intValue = EditorGUI.IntSlider(rect, range.intValue, 0, target.loadRange);

                                    // COLUMN B
                                    rect.x += rect.width - columnB_offset;
                                    rect.width = rectWidth * columnB_sizePercent;
                                    cells.intValue = EditorGUI.IntPopup(rect, cells.intValue, innerCellsPerLineOptionsDisplayed, innerCellsPerLineOptionsValues);

                                    // COLUMN C
                                    rect.x += rect.width + 4;
                                    rect.width = 20;
                                    if (GUI.Button(rect, "X"))
                                    {
                                        terrainLODs.DeleteArrayElementAtIndex(i);
                                        break;
                                    }
                                }
                                EditorGUILayout.EndHorizontal();
                            }
                            // ADD BUTTON
                            var buttonRect = EditorGUI.IndentedRect(EditorGUILayout.GetControlRect());
                            buttonRect.width = 90;
                            if (GUI.Button(buttonRect, "Add"))
                            {
                                terrainLODs.InsertArrayElementAtIndex(terrainLODs.arraySize);
                            }
                            buttonRect.x += buttonRect.width;
                            if (GUI.Button(buttonRect, "Sort"))
                            {
                                target.terrainLODs = (from tl in target.terrainLODs orderby tl.range select tl).ToArray();
                            }
                        }
                        if (EditorGUI.EndChangeCheck())
                        {
                            serializedObject.ApplyModifiedProperties();
                        }
                    }
                    EditorGUI.indentLevel--;
                }
            }
        }
#endif

        [Serializable]
        public struct OpenWorldTerrainLOD
        {
            public int range; // between 0 and loadRange
            public int innerCellsPerLine; // between 0 and mainGridSize - 2
        }

        [DontEditInPlayMode] public bool setMaterialColor;

        [SerializeField, DontEditInPlayMode] private int loadRange = 1;
        [SerializeField, DontEditInPlayMode] private int mainGridSize = 10;
        [SerializeField, DontEditInPlayMode] private float mainNodeSize = 1f;

        [Space]
        [SerializeField, DontEditInPlayMode] private Material terrainMaterial = null;
        [SerializeField, DontEditInPlayMode] private NoiseMapGenerationParameters terrainNoiseParams;

        [Space]
        public Transform viewerTransform;
        public Vector3 viewerPosition;

        [Space]
        [SerializeField, ReadOnly] private Vector2Int curCoords;
        [SerializeField, ReadOnly] private Vector2Int prevCoords;

        /****************************************************/

        [SerializeField, HideInInspector] public OpenWorldTerrainLOD[] terrainLODs;
        [HideInInspector] public int[] terrainLODs_innerCellsPerLine;

        /****************************************************/

        private Dictionary<Vector2Int, OpenWorldChunk> chunksDictionary = new Dictionary<Vector2Int, OpenWorldChunk>();
        private List<OpenWorldChunk> currentChunks = new List<OpenWorldChunk>();
        private List<OpenWorldChunk> previousChunks = new List<OpenWorldChunk>();

        /****************************************************/

        public GridGroup MainGridGroup { get; private set; }

        public Material TerrainMaterial => terrainMaterial;

        public NoiseMapGenerationParameters TerrainNoiseParams => terrainNoiseParams;

        public int MainGridSize => mainGridSize;

        /****************************************************/

        private void Awake()
        {
            terrainLODs = (from tl in terrainLODs orderby tl.range select tl).ToArray();
            terrainLODs_innerCellsPerLine = (from tl in terrainLODs select tl.innerCellsPerLine).ToArray();

            loadRange = Mathf.Max(loadRange, 1);

            // TODO: grid for objects and pathfinding
            var auxGridSize = mainGridSize * 2;
            var auxNodeSize = mainNodeSize * .5f;

            var mainGridGroupGO = new GameObject("MainGridGroup");
            mainGridGroupGO.transform.SetParent(transform, false);

            MainGridGroup = mainGridGroupGO.AddComponent<GridGroup>();
            MainGridGroup.Init(new Vector2Int(mainGridSize, mainGridSize), mainNodeSize);
            mainGridSize = MainGridGroup.GridSize.x;
            mainNodeSize = MainGridGroup.NodeSize;

            terrainNoiseParams.width = mainGridSize + 1;
            terrainNoiseParams.height = mainGridSize + 1;
            terrainNoiseParams.normalizationMode = NoiseNormalizationMode.Global;

            LoadChunks();
        }

        private void FixedUpdate()
        {
            if (viewerTransform != null && viewerTransform.parent == transform)
            {
                viewerPosition = viewerTransform.localPosition;
            }

            var globalNodeCoords = MainGridGroup.CalculateGlobalNodeCoords(viewerPosition, Space.Self);
            var newCoords = MainGridGroup.CalculateGridCoords(globalNodeCoords);
            if (curCoords.x != newCoords.x || curCoords.y != newCoords.y)
            {
                //Debug.Log($"Viewer moved to other chunk");
                prevCoords = curCoords;
                curCoords = newCoords;
                LoadChunks();
            }
            
            OpenWorldChunk.UpdateGeneration();
            foreach (var chunk in currentChunks)
            {
                chunk.UpdateTerrain();
            }
        }

        private void LateUpdate()
        {
            //foreach (var chunk in currentChunks)
            //{
            //    chunk.grid.DrawBorders2(Color.red.GetWithAlpha(.5f));
            //}
        }

        private void LoadChunks()
        {
            float maxDistance = Mathf.Sqrt(loadRange * loadRange * 2);

            previousChunks.Clear();
            previousChunks.AddRange(currentChunks);
            currentChunks.Clear();

            void Load(Vector2Int coords, int lodIndex)
            {
                int xDif = Mathf.Abs(curCoords.x - coords.x);
                int yDif = Mathf.Abs(curCoords.y - coords.y);
                if (xDif <= terrainLODs[lodIndex].range && yDif <= terrainLODs[lodIndex].range)
                {
                    OpenWorldChunk chunk = GetChunk(coords);
                    
                    if (chunk == null)
                    {
                        chunk = CreateChunk(coords);
                    }

                    chunk.StartGeneration();

                    if (!currentChunks.Contains(chunk))
                    {
                        currentChunks.Add(chunk);
                        previousChunks.Remove(chunk);
                        chunk.terrainLODIndex = lodIndex;
                    }
                }
            }

            Load(curCoords, 0);

            for (int lodIndex = 0; lodIndex < terrainLODs.Length; lodIndex++)
            {
                for (int y = 1; y <= loadRange; y++)
                {
                    for (int x = 1; x <= loadRange; x++)
                    {
                        Load(new Vector2Int(curCoords.x, curCoords.y - y), lodIndex);
                        Load(new Vector2Int(curCoords.x, curCoords.y + y), lodIndex);
                        Load(new Vector2Int(curCoords.x - x, curCoords.y), lodIndex);
                        Load(new Vector2Int(curCoords.x + x, curCoords.y), lodIndex);
                        Load(new Vector2Int(curCoords.x - x, curCoords.y - y), lodIndex);
                        Load(new Vector2Int(curCoords.x - x, curCoords.y + y), lodIndex);
                        Load(new Vector2Int(curCoords.x + x, curCoords.y - y), lodIndex);
                        Load(new Vector2Int(curCoords.x + x, curCoords.y + y), lodIndex);
                    }
                }
            }

            foreach (var chunk in previousChunks)
            {
                chunk.SetActive(false);
                //chunksDictionary.Remove(chunk.coords);
                //chunk.Destroy();
            }

            foreach (var chunk in currentChunks)
            {
                chunk.SetActive(true);
            }
        }

        public OpenWorldChunk GetChunk(Vector2Int coords)
        {
            chunksDictionary.TryGetValue(coords, out OpenWorldChunk openWorldChunk);
            return openWorldChunk;
        }

        private OpenWorldChunk CreateChunk(Vector2Int coords)
        {
            if (chunksDictionary.ContainsKey(coords))
            {
                return null;
            }
            var chunk = new OpenWorldChunk(this, coords);
            chunksDictionary.Add(coords, chunk);
            return chunk;
        }

        public float Terraforming_GetAdditiveHeight(Vector2Int globalNodeCoords)
        {
            var gridCoords = MainGridGroup.CalculateGridCoords(globalNodeCoords);
            var localNodeCoords = MainGridGroup.CalculateLocalNodeCoords(gridCoords, globalNodeCoords);
            var chunk = GetChunk(gridCoords);
            if (chunk == null)
            {
                chunk = CreateChunk(gridCoords);
            }
            return chunk.GetAdditiveHeightMap(localNodeCoords.x, localNodeCoords.y);
        }

        public void Terraforming_SetAdditiveHeight(HashSet<OpenWorldChunk> chunks, Vector2Int globalNodeCoords, float height)
        {
            var gridCoords = MainGridGroup.CalculateGridCoords(globalNodeCoords);
            var localNodeCoords = MainGridGroup.CalculateLocalNodeCoords(gridCoords, globalNodeCoords);
            var chunk = GetChunk(gridCoords);

            chunk.SetAddivetHeightMap(localNodeCoords.x, localNodeCoords.y, height);
            chunks.Add(chunk);

            if (localNodeCoords.x == 0)
            {
                var neighbourChunk = GetChunk(gridCoords + Vector2Int.left);
                if (neighbourChunk != null)
                {
                    neighbourChunk.SetAddivetHeightMap(mainGridSize, localNodeCoords.y, height);
                    chunks.Add(neighbourChunk);
                }
            }

            if (localNodeCoords.y == 0)
            {
                var neighbourChunk = GetChunk(gridCoords + Vector2Int.down);
                if (neighbourChunk != null)
                {
                    neighbourChunk.SetAddivetHeightMap(localNodeCoords.x, mainGridSize, height);
                    chunks.Add(neighbourChunk);
                }
            }

            if (localNodeCoords.x == 0 && localNodeCoords.y == 0)
            {
                var neighbourChunk = GetChunk(gridCoords + Vector2Int.down + Vector2Int.left);
                if (neighbourChunk != null)
                {
                    neighbourChunk.SetAddivetHeightMap(mainGridSize, mainGridSize, height);
                    chunks.Add(neighbourChunk);
                }
            }
        }
    } // class
} // namespace