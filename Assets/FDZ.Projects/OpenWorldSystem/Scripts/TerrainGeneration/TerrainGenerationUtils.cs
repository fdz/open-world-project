﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
    public enum NoiseNormalizationMode
    {
        Local,
        Global
    }

    [System.Serializable]
    public struct NoiseMapGenerationParameters
    {
        public int width; // 10
        public int height; // 10
        public int seed;
        public float scale; // 30f
        public int octaves; // 4
        public float persistance; // .5f
        public float lacunarity; // 2
        public NoiseNormalizationMode normalizationMode;
        public Vector2 offset;
        public AnimationCurve curve;
        public float multiplier; // 5f
        public bool useStep;
        public float step;
    }

    public static class TerrainGenerationUtils
    {
        public static void GenerateNoiseMap(float[,] noiseMap, NoiseMapGenerationParameters p)
        {
            System.Random prng = new System.Random(p.seed);
            Vector2[] octaveOffsets = new Vector2[p.octaves];

            float maxPosibleHeight = 0;
            float amplitude = 1;

            for (int i = 0; i < p.octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + p.offset.x;
                float offsetY = prng.Next(-100000, 100000) + p.offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPosibleHeight += amplitude;
                amplitude *= p.persistance;
            }

            if (p.scale <= 0)
            {
                p.scale = 0.0001f;
            }

            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;

            float halfWidth = p.width / 2f;
            float halfHeight = p.height / 2f;

            for (int y = 0; y < p.height; y++)
            {
                for (int x = 0; x < p.width; x++)
                {
                    amplitude = 1;
                    float frequency = 1;
                    float noiseHeight = 0;

                    for (int i = 0; i < p.octaves; i++)
                    {
                        float sampleX = (x - halfWidth + octaveOffsets[i].x) / p.scale * frequency;
                        float sampleY = (y - halfHeight + octaveOffsets[i].y) / p.scale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= p.persistance;
                        frequency *= p.lacunarity;
                    }

                    if (p.normalizationMode == NoiseNormalizationMode.Global)
                    {
                        //float normalizedHeight = (noiseMap[x, y] + 1) / (2f * maxPosibleHieght / 1.75f);
                        float normalizedHeight = (noiseHeight + 1) / maxPosibleHeight;
                        noiseMap[x, y] = normalizedHeight;
                    }
                    else
                    {
                        if (noiseHeight > maxLocalNoiseHeight)
                        {
                            maxLocalNoiseHeight = noiseHeight;
                        }
                        else if (noiseHeight < minLocalNoiseHeight)
                        {
                            minLocalNoiseHeight = noiseHeight;
                        }
                        noiseMap[x, y] = noiseHeight;
                    }
                }
            }

            if (p.normalizationMode == NoiseNormalizationMode.Local)
            {
                for (int y = 0; y < p.height; y++)
                {
                    for (int x = 0; x < p.width; x++)
                    {
                        noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                    }
                }
            }

            for (int y = 0; y < p.height; y++)
            {
                for (int x = 0; x < p.width; x++)
                {
                    noiseMap[x, y] = p.curve.Evaluate(noiseMap[x, y]);
                    noiseMap[x, y] *= p.multiplier;
                    if (p.useStep)
                    {
                        noiseMap[x, y] = Mathf.Floor(noiseMap[x, y] / p.step) * p.step;
                    }
                }
            }
        }

        public static float[,] GenerateNoiseMap(NoiseMapGenerationParameters p)
        {
            float[,] noiseMap = new float[p.width, p.height];
            GenerateNoiseMap(noiseMap, p);
            return noiseMap;
        }
    }
}