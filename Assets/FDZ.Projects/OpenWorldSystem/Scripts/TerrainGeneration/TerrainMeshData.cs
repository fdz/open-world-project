﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace FDZ.Projects.OpenWorldSystem
{
	public class TerrainMeshData
    {
        public bool isValid;
        public Vector3[] vertices;
        public int[] triangles;
        public Vector2[] uv;
        public Vector3[] normals;
        public int cellsPerLine;
        public int verticesPerLine;
        public int vertexStep;

        /****************************************************************************************/

        private Dictionary<int, int[]> quadHoles = new Dictionary<int, int[]>();

        /****************************************************************************************/

        public void MakeHole(int ti)
        {
            if (!quadHoles.ContainsKey(ti))
            {
                var aux = quadHoles[ti] = new int[4];

                aux[0] = triangles[ti];
                aux[1] = triangles[ti + 1];
                aux[2] = triangles[ti + 2];
                aux[3] = triangles[ti + 5];

                triangles[ti] = 0;
                triangles[ti + 1] = 0;
                triangles[ti + 2] = 0;
                triangles[ti + 3] = 0;
                triangles[ti + 4] = 0;
                triangles[ti + 5] = 0;
            }
        }

        public void RemoveAllQuadHoles()
        {
            while (quadHoles.Count != 0)
            {
                int ti = quadHoles.First().Key;
                RemoveQuadHole(ti);
            }
        }

        public void RemoveQuadHole(int ti)
        {
            if (quadHoles.ContainsKey(ti))
            {
                var vis = quadHoles[ti];
                triangles[ti] = vis[0];
                triangles[ti + 1] = triangles[ti + 4] = vis[1];
                triangles[ti + 2] = triangles[ti + 3] = vis[2];
                triangles[ti + 5] = vis[3];
                quadHoles.Remove(ti);
            }
        }

        public void ReverseTrianglesWindingOrder(int ti)
        {
            // b---d
            // | \ |
            // a---c

            // Facing Up
            // a -> b -> c
            // c -> b -> d

            // Facing Down
            // d -> b -> c
            // c -> b -> a

            int a = triangles[ti];
            int d = triangles[ti + 5];

            triangles[ti] = d;
            triangles[ti + 5] = a;
        }

        /****************************************************************************************/

        public Vector3[] CalculateNormals()
        {
            Vector3[] vertexNormals = CalculateNormalsUnnormalized();
            for (int i = 0; i < vertexNormals.Length; i++)
            {
                vertexNormals[i].Normalize();
            }
            return vertexNormals;
        }

        public Vector3[] CalculateNormalsUnnormalized()
        {
            Vector3[] vertexNormals = new Vector3[vertices.Length];
            int triangleCount = triangles.Length / 3;
            for (int i = 0; i < triangleCount; i++)
            {
                int normalTriangleIndex = i * 3;
                int vertexIndexA = triangles[normalTriangleIndex];
                int vertexIndexB = triangles[normalTriangleIndex + 1];
                int vertexIndexC = triangles[normalTriangleIndex + 2];
                Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
                vertexNormals[vertexIndexA] += triangleNormal;
                vertexNormals[vertexIndexB] += triangleNormal;
                vertexNormals[vertexIndexC] += triangleNormal;
            }
            return vertexNormals;
        }

        public Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC)
        {
            Vector3 pointA = vertices[indexA];
            Vector3 pointB = vertices[indexB];
            Vector3 pointC = vertices[indexC];
            Vector3 sideAB = pointB - pointA;
            Vector3 sideAC = pointC - pointA;
            return Vector3.Cross(sideAB, sideAC).normalized;
        }

        public bool IsValidCoord(int x, int y)
        {
            return x % vertexStep == 0 && y % vertexStep == 0;
        }
    }
}