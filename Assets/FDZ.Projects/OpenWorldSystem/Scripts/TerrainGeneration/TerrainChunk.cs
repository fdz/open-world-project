﻿using UnityEngine;
using System.Threading;
using System;
using System.Collections.Generic;
using FDZ.Attributes;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
using Drawing;
#endif

namespace FDZ.Projects.OpenWorldSystem
{
    public class TerrainChunk : MonoBehaviour
    {
        public static Vector2Int NEIGHBOUR_TOP_LEFT = new Vector2Int(-1, 1);
        public static Vector2Int NEIGHBOUR_TOP_CENTER = new Vector2Int(0, 1);
        public static Vector2Int NEIGHBOUR_TOP_RIGHT = new Vector2Int(1, 1);
        public static Vector2Int NEIGHBOUR_LEFT = new Vector2Int(-1, 0);
        public static Vector2Int NEIGHBOUR_RIGHT = new Vector2Int(1, 0);
        public static Vector2Int NEIGHBOUR_BOTTOM_LEFT = new Vector2Int(-1, -1);
        public static Vector2Int NEIGHBOUR_BOTTOM_CENTER = new Vector2Int(0, -1);
        public static Vector2Int NEIGHBOUR_BOTTOM_RIGHT = new Vector2Int(1, -1);

        public static TerrainChunk CreateGameObject(Material material)
        {
            var terrainGO = new GameObject("Terrain");
            terrainGO.AddComponent<MeshFilter>();
            terrainGO.AddComponent<MeshRenderer>().sharedMaterial = material;
            terrainGO.AddComponent<MeshCollider>();
            return terrainGO.AddComponent<TerrainChunk>();
        }

        /******************************************************/

        [SerializeField, ReadOnly] private int _currentMeshDataIndex;

        private Mesh mesh;
        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;
        private MeshCollider meshCollider;
        private float cellSize = .5f;
        private float[,] heightMap;

        [HideInInspector] public TerrainMeshData[] meshDataLODs;

        public readonly Dictionary<Vector2Int, TerrainChunk> neighbours = new Dictionary<Vector2Int, TerrainChunk>(8);

        public bool IsInitialized { get; private set; }

        public bool HasMesh { get; private set; }

        public int CurrentMeshDataIndex { get => _currentMeshDataIndex; private set => _currentMeshDataIndex = value; }

        private void Awake()
        {
            neighbours.Add(NEIGHBOUR_TOP_LEFT, null);
            neighbours.Add(NEIGHBOUR_TOP_CENTER, null);
            neighbours.Add(NEIGHBOUR_TOP_RIGHT, null);
            neighbours.Add(NEIGHBOUR_LEFT, null);
            neighbours.Add(NEIGHBOUR_RIGHT, null);
            neighbours.Add(NEIGHBOUR_BOTTOM_LEFT, null);
            neighbours.Add(NEIGHBOUR_BOTTOM_CENTER, null);
            neighbours.Add(NEIGHBOUR_BOTTOM_RIGHT, null);
        }

        public void SetNeighbour(Vector2Int dir, TerrainChunk terrain)
        {
            neighbours[dir] = terrain;
            terrain.neighbours[-dir] = this;
        }

        public TerrainChunk GetNeighbour(Vector2Int dir)
        {
            return neighbours[dir];
        }

        public void Init(float cellSize, int cellsPerLine, int[] meshDataLODs_cellsPerLine)
        {
            if (IsInitialized)
            {
                Debug.LogError("Terrain is already initialized");
                return;
            }

            if (TryGetComponent(out meshFilter) == false)
            {
                Debug.LogError($"Missing component: {typeof(MeshFilter)}");
                return;
            }

            if (TryGetComponent(out meshRenderer) == false)
            {
                Debug.LogError($"Missing component: {typeof(MeshRenderer)}");
                return;
            }

            if (TryGetComponent(out meshCollider) == false)
            {
                Debug.LogError($"Missing component: {typeof(MeshCollider)}");
                return;
            }

            this.cellSize = cellSize;

            if (meshDataLODs_cellsPerLine == null || meshDataLODs_cellsPerLine.Length == 0)
            {
                Debug.LogError("Can't initialzie Terrain, lods_innerCellsPerLine array is not valid");
            }

            meshDataLODs = new TerrainMeshData[meshDataLODs_cellsPerLine.Length];
            for (int i = 0; i < meshDataLODs_cellsPerLine.Length; i++)
            {
                meshDataLODs[i] = new TerrainMeshData();
                meshDataLODs[i].cellsPerLine = meshDataLODs_cellsPerLine[i];
                meshDataLODs[i].verticesPerLine = meshDataLODs_cellsPerLine[i] + 1;
                meshDataLODs[i].vertexStep = meshDataLODs[0].cellsPerLine / meshDataLODs[i].cellsPerLine;
            }

            heightMap = new float[meshDataLODs[0].verticesPerLine, meshDataLODs[0].verticesPerLine];

            CurrentMeshDataIndex = -1;

            mesh = new Mesh();
            mesh.name = "ProceduralTerrain";
            meshFilter.mesh = mesh;

            IsInitialized = true;
        }

        public void AssignMeshData(int index)
        {
            CurrentMeshDataIndex = index;
            var meshData = meshDataLODs[index];

            mesh.Clear();

            mesh.vertices = meshData.vertices;
            mesh.triangles = meshData.triangles;
            //mesh.normals = meshData.normals;
            mesh.RecalculateNormals();

            mesh.RecalculateTangents();
            mesh.RecalculateBounds();

            if (index == 0)
            {
                meshCollider.enabled = true;
                //if (meshCollider.sharedMesh == null)
                {
                    meshCollider.cookingOptions = MeshColliderCookingOptions.CookForFasterSimulation | MeshColliderCookingOptions.UseFastMidphase;
                    meshCollider.sharedMesh = mesh;
                }
            }
            else
            {
                meshCollider.enabled = false;
            }

            HasMesh = true;
        }

        public void ReassignMeshData()
        {
            if (CurrentMeshDataIndex < 0)
            {
                CurrentMeshDataIndex = meshDataLODs.Length - 1;
            }
            AssignMeshData(CurrentMeshDataIndex);
        }

        public void GenerateMeshData(int i)
        {
            if (IsInitialized == false)
            {
                Debug.LogError("Can't generate Terrain.meshData, call Terrain.Init() first");
                return;
            }

            TerrainMeshData meshData = meshDataLODs[i];
            int cellsPerLine = meshDataLODs[i].cellsPerLine;
            int verticesPerLine = meshDataLODs[i].verticesPerLine;

            int vi = 0;
            int ti = 0;

            void AddTriangles(int a, int b, int c, int d)
            {
                meshData.triangles[ti] = a;
                meshData.triangles[ti + 1] = meshData.triangles[ti + 4] = b;
                meshData.triangles[ti + 2] = meshData.triangles[ti + 3] = c;
                meshData.triangles[ti + 5] = d;
                ti += 6;
            }

            meshData.vertices = new Vector3[verticesPerLine * verticesPerLine];
            meshData.triangles = new int[(cellsPerLine) * (cellsPerLine) * 6];

            int maxVerticesPerLine = meshDataLODs[0].verticesPerLine;
            for (int y = 0; y < maxVerticesPerLine; y += meshData.vertexStep)
            {
                for (int x = 0; x < maxVerticesPerLine; x += meshData.vertexStep)
                {
                    meshData.vertices[vi] = new Vector3(x * cellSize, 0, y * cellSize);
                    if (y < maxVerticesPerLine - 1 && x < maxVerticesPerLine - 1)
                    {
                        AddTriangles(vi, vi + verticesPerLine, vi + 1, vi + verticesPerLine + 1);
                    }
                    vi++;
                }
            }

            meshData.normals = meshData.CalculateNormals();

            //Debug.Log($"Terrain.meshData generated: {vi} / {meshData.vertices.Length} vertices, {ti} / {meshData.triangles.Length} triangles");

            if (vi != meshData.vertices.Length)
            {
                Debug.LogError($"Terrain generated a different amount of vertices than excepted, {ti} / {meshData.vertices.Length} vertices");
            }

            if (ti != meshData.triangles.Length)
            {
                Debug.LogError($"Terrain generated a different amount of triangles than excepted, {ti} / {meshData.triangles.Length} triangles");
            }

            meshData.isValid = true;
        }

        public void SetHeight(int x, int y, float height)
        {
            heightMap[x, y] = height;
            SetVertexHeight(x, y, height);
        }

        private void SetVertexHeight(int x, int y, float height)
        {
            foreach (TerrainMeshData meshData in meshDataLODs)
            {
                if (meshData.IsValidCoord(x, y))
                {
                    meshData.vertices[(y / meshData.vertexStep) * meshData.verticesPerLine + (x / meshData.vertexStep)].y = height;
                }
            }
        }

        public void AlingEdgesToNeighbours()
        {
            if (CurrentMeshDataIndex < 0 || !meshDataLODs[CurrentMeshDataIndex].isValid || !meshDataLODs[0].isValid)
            {
                return;
            }

            TerrainMeshData currentMeshData = meshDataLODs[CurrentMeshDataIndex];

            TerrainMeshData maxCurrentMeshData = meshDataLODs[0];
            int maxVerticesPerLine = maxCurrentMeshData.verticesPerLine;

            TerrainChunk neighbourRightChunk = neighbours[NEIGHBOUR_RIGHT];
            TerrainChunk neighbourLeftChunk = neighbours[NEIGHBOUR_LEFT];
            TerrainChunk neighbourTopCenterChunk = neighbours[NEIGHBOUR_TOP_CENTER];
            TerrainChunk neighbourBottomCenterChunk = neighbours[NEIGHBOUR_BOTTOM_CENTER];

            for (int xy = 0; xy < maxVerticesPerLine; xy += currentMeshData.vertexStep)
            {
                if (neighbourLeftChunk != null)
                {
                    LerpHeight(neighbourLeftChunk, 0, xy, true);
                }
                if (neighbourRightChunk != null)
                {
                    LerpHeight(neighbourRightChunk, maxVerticesPerLine - 1, xy, true);
                }
                if (neighbourTopCenterChunk != null)
                {
                    LerpHeight(neighbourTopCenterChunk, xy, maxVerticesPerLine - 1, false);
                }
                if (neighbourBottomCenterChunk != null)
                {
                    LerpHeight(neighbourBottomCenterChunk, xy, 0, false);
                }
            }

            /**************************************/

            void LerpHeight(TerrainChunk neighbour, int x, int y, bool vertical)
            {
                if (neighbour.CurrentMeshDataIndex == CurrentMeshDataIndex)
                {
                    SetVertexHeight(x, y, heightMap[x, y]);
                }
                else if (neighbour.CurrentMeshDataIndex >= 0 && CurrentMeshDataIndex < neighbour.CurrentMeshDataIndex)
                {
                    TerrainMeshData neighbourMeshData = neighbour.meshDataLODs[neighbour.CurrentMeshDataIndex];
                    float xy = (vertical ? y : x) / (float)neighbourMeshData.vertexStep;
                    int start = neighbourMeshData.vertexStep * ((int)xy);
                    int end = neighbourMeshData.vertexStep * (((int)xy) + 1);
                    if (end < maxVerticesPerLine)
                    {
                        float startHeight = heightMap[(vertical ? x : start), (vertical ? start : y)];
                        float endHeight   = heightMap[(vertical ? x : end),   (vertical ? end : y)];
                        float height = Mathf.Lerp(startHeight, endHeight, xy - (int)xy);
                        SetVertexHeight(x, y, height);
                    }
                }
            }

            /**************************************/

        } // AlingEdgesToNeighbours()

    } // class
} // namespace