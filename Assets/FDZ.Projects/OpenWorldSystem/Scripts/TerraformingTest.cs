﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
	public class TerraformingTest : MonoBehaviour
	{
        public AnimationCurve heightCurve;

        private OpenWorld w;
        private GridGroup gg;

        private Camera cam;
        
        private bool isLeftMouseButtonPressed;

        private void Start()
        {
            w = GetComponent<OpenWorld>();
            gg = w.MainGridGroup;
            cam = Camera.main;
        }

		private void Update()
        {
            isLeftMouseButtonPressed = Input.GetMouseButton(0);
        }

        private void FixedUpdate()
        {

            if (!isLeftMouseButtonPressed)
            {
                return;
            }

            var camRay = cam.ScreenPointToRay(Input.mousePosition);
            var hit = Physics.Raycast(camRay, out RaycastHit hitInfo, float.MaxValue, -1, QueryTriggerInteraction.Ignore);
            
            if (!hit)
            {
                return;
            }

            var position = hitInfo.point;
            var globalNodeCoords = gg.CalculateGlobalNodeCoords(position, Space.World);

            var chunks = new System.Collections.Generic.HashSet<OpenWorldChunk>();

            var distance = 10f;
            var range = (int)distance + 1;
            var heightIncrement = w.MainGridGroup.NodeSize * 2 * Time.deltaTime;

            for (int y = -range; y <= range; y++)
            {
                for (int x = -range; x <= range; x++)
                {
                    var coords = new Vector2Int(globalNodeCoords.x + x, globalNodeCoords.y + y);
                    var d = (coords - globalNodeCoords).magnitude;
                    var t = 1 - Mathf.Sqrt(x * x + y * y) / distance;
                    
                    if (t <= 0)
                    {
                        continue;
                    }
                    
                    t = heightCurve.Evaluate(t);

                    var height = w.Terraforming_GetAdditiveHeight(coords);
                    w.Terraforming_SetAdditiveHeight(chunks, coords, height + heightIncrement * t);
                }
            }

            foreach (var c in chunks)
            {
                if (c.IsGenerated)
                {
                    c.ApplyHeightMaps();
                    c.terrain.ReassignMeshData();
                }
            }
        }

    } // class
} // namespace