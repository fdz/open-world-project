﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FDZ.Projects.OpenWorldSystem
{
    public class PrefabPoolManager : MonoBehaviour
    {
#if UNITY_EDITOR
        [CustomEditor(typeof(PrefabPoolManager))]
        private class PrefabPoolManagerEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                GUI.enabled = false;
                //EditorGUILayout.IntField("types", pools.Count);
                //EditorGUILayout.IntField("countInPool", countInPool);
                //EditorGUILayout.IntField("countPopped", countPopped);
                //EditorGUILayout.IntField("countTotal", countTotal);
                foreach (var dicItem in pools)
                {
                    var prefab = dicItem.Key;
                    var pool = dicItem.Value;
                    EditorGUILayout.LabelField(prefab.name);
                    EditorGUI.indentLevel++;
                    {
                        EditorGUILayout.LabelField($"In Pool: {pool.CountInPool}");
                        EditorGUILayout.LabelField($"Popped: {pool.CountPopped}");
                        EditorGUILayout.LabelField($"Total: {pool.CountTotal}");
                    }
                    EditorGUI.indentLevel--;
                    EditorGUILayout.Space();
                }
                GUI.enabled = true;
            }
        }
#endif
        public class PrefabPool
        {
            private readonly GameObject prefab;
            private readonly Stack<GameObject> pool;
            private readonly HashSet<GameObject> popped;

            public int CountInPool { get => pool.Count; }
            public int CountPopped { get => popped.Count; }
            public int CountTotal { get => pool.Count + popped.Count; }

            public PrefabPool(GameObject prefab)
            {
                this.prefab = prefab;
                pool = new Stack<GameObject>();
                popped = new HashSet<GameObject>();
            }

            public void StorePrefabInstance(GameObject instance)
            {
                pool.Push(instance);

                popped.Remove(instance);
            }

            public GameObject GetPrefabInstance()
            {
                GameObject instance;

                if (pool.Count != 0)
                {
                    instance = pool.Pop();
                }
                else
                {
                    instance = GameObject.Instantiate(prefab);
                }

                popped.Add(instance);

                return instance;
            }
        }

        #region Singleton
        private static PrefabPoolManager _instance;
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                gameObject.SetActive(false);
            }
        }
        #endregion

        private static Dictionary<GameObject, PrefabPool> pools = new Dictionary<GameObject, PrefabPool>();

        public static bool Register(GameObject prefab)
        {
            if (prefab == null || pools.ContainsKey(prefab))
            {
                return false;
            }
            pools.Add(prefab, new PrefabPool(prefab));
            return true;
        }

        public static GameObject GetPrefabInstance(GameObject prefab)
        {
            if (prefab == null)
            {
                return null;
            }
            GameObject instance = null;
            if (pools.TryGetValue(prefab, out PrefabPool pool))
            {
                instance = pool.GetPrefabInstance();
            }
            return instance;
        }

        public static void StorePrefabInstance(GameObject prefab, GameObject instance)
        {
            if (prefab == null || instance == null)
            {
                return;
            }
            if (pools.TryGetValue(prefab, out PrefabPool pool))
            {
                instance.transform.SetParent(_instance.transform, true);
                instance.gameObject.SetActive(false);
                pool.StorePrefabInstance(instance);
            }
        }
    } // class
} // namespace