﻿using Drawing;
using FDZ.Attributes;
using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
	public class CameraController : MonoBehaviour
	{
		public bool forceInput;
		public Vector3 forceMove;
		public bool forceSprint;
		[Space]
		public KeyCode keyMoveLeft = KeyCode.A;
		public KeyCode keyMoveRight = KeyCode.D;
		[Space]
		public KeyCode keyMoveDown = KeyCode.F;
		public KeyCode keyMoveUp = KeyCode.R;
		[Space]
		public KeyCode keyMoveBack = KeyCode.S;
		public KeyCode keyMoveForward = KeyCode.W;
		[Space]
		public KeyCode keyMoveSprint = KeyCode.LeftShift;
		public KeyCode keyEnableRotation = KeyCode.Space;
		[Space]
		public float moveSpeedNormal = 6f;
		public float moveSpeedSprint = 12f;
		[Space]
		[SerializeField, ReadOnly] private float x;
		[SerializeField, ReadOnly] private float y;
		[SerializeField, ReadOnly] private float z;

		private void LateUpdate()
        {
			if (Input.GetKey(keyEnableRotation))
            {
				Cursor.lockState = CursorLockMode.Locked;
				float mX = Input.GetAxisRaw("Mouse X");
				float mY = Input.GetAxisRaw("Mouse Y");
				Vector3 eulerAngles = transform.eulerAngles;
				eulerAngles.x += -mY;
				eulerAngles.y += mX;
				eulerAngles.x = Utils.WrapAngle(eulerAngles.x);
				if (Mathf.Abs(eulerAngles.x) >= 89)
                {
					eulerAngles.x = 89 * System.Math.Sign(eulerAngles.x);
                }
				transform.eulerAngles = eulerAngles;
            }
			else
			{
				Cursor.lockState = CursorLockMode.None;
			}

			x = UtilsInput.GetAxis(keyMoveLeft, keyMoveRight);
			y = UtilsInput.GetAxis(keyMoveDown, keyMoveUp);
			z = UtilsInput.GetAxis(keyMoveBack, keyMoveForward);

			bool sprint = Input.GetKey(keyMoveSprint);

			if (forceInput)
            {
				x = Mathf.Clamp(forceMove.x, -1, 1);
				y = Mathf.Clamp(forceMove.y, -1, 1);
				z = Mathf.Clamp(forceMove.z, -1, 1);
				sprint = forceSprint;
			}

			float speed = sprint ? moveSpeedSprint : moveSpeedNormal;
			Vector3 direction = (
				x * transform.right +
				y * Vector3.up +
				z * transform.forward.xoz().normalized
			).normalized;

			transform.Translate(direction * (speed * Time.deltaTime), Space.World);
		}
    }
}