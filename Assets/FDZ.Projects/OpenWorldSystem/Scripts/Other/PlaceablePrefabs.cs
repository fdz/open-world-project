﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldSystem
{
	public class PlaceablePrefabs : MonoBehaviour
    {
#if UNITY_EDITOR
        [UnityEditor.CustomEditor(typeof(PlaceablePrefabs))]
        private class PrefabPoolManagerEditor : UnityEditor.Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                var target = this.target as PlaceablePrefabs;
                if (GUILayout.Button("Set Children Position"))
                {
                    target.SetChildrenPosition();
                }
            }
        }
#endif
        public float separation = 1;

        private void SetChildrenPosition()
        {
            for (int x = 0; x < transform.childCount; x++)
            {
                var child = transform.GetChild(x);
                child.localPosition = new Vector3(x * separation, 0);
            }
        }
    }
}