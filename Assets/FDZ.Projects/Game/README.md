# TUTORIALS
https://catlikecoding.com/unity/tutorials/movement/

# OBJECTIVE
Trying to do a generic character controller. With good handling of steps and slopes.

# References
https://assetstore.unity.com/packages/tools/physics/kinematic-character-controller-99131
https://forum.unity.com/threads/released-kinematic-character-controller.497979/#post-3262218
http://www.00jknight.com/blog/unity-character-controller
https://docs.unity3d.com/ScriptReference/Physics.ClosestPoint.html
https://forum.unity.com/threads/we-need-a-way-to-detect-and-resolve-collision-hits-manually.379699/
	https://roystanross.wordpress.com/downloads/
https://gitlab.com/MichaelDoyon/motor-controller

# MILESTONE N-1
- [ ] Movement on any surface
- [ ] Player WASD
- [ ] Player click
- [ ] NPC follow
- [ ] NPC waypoints

# BACKLOG | Things to be made into milestones
- [ ] Jumping
- [ ] Ledge grabbing
- [ ] Ledge climbing
- [ ] Ledge movement
- [ ] Root motion actions
- [ ] Climbing: BOTW, AC, Mirror's Edge, Tomb Raider, Uncharted

# MOD SUPPORT
https://www.google.com/search?hl=en&sxsrf=ALeKk00pXHzF6j8BrpEr5sc3NjobJRO8Sg%3A1592132594135&ei=8gPmXvLsB4LM5OUP6--ZOA&q=unity+c%23+dll+mod&oq=unity+c%23+dll+mod&gs_lcp=CgZwc3ktYWIQAzIICCEQFhAdEB4yCAghEBYQHRAeMggIIRAWEB0QHjIICCEQFhAdEB4yCAghEBYQHRAeMggIIRAWEB0QHjIICCEQFhAdEB4yCAghEBYQHRAeOgQIABBHOgIIADoGCAAQFhAeOggIABAWEAoQHjoECCMQJzoFCAAQkQI6BQgAEMsBOgcIABAUEIcCUOKlAVjavAFg8b0BaABwAXgAgAGiAYgBqAmSAQQxMS4ymAEAoAEBqgEHZ3dzLXdpeg&sclient=psy-ab&ved=0ahUKEwjyrdWNlIHqAhUCJrkGHet3BgcQ4dUDCAw&uact=5
https://forum.unity.com/threads/solved-inserting-dll-into-built-game.638671/
https://www.google.com/search?hl=en&q=C%23%20compile%20at%20runtime
https://www.codeproject.com/Tips/715891/Compiling-Csharp-Code-at-Runtime?fid=1852388&df=90&mpp=25&sort=Position&spc=Relaxed&prof=True&view=Normal&fr=23
https://www.google.com/search?hl=en&q=unity%20load%20fbx%20at%20runtime

# LOAD MODELS AT RUNTIME
https://assetstore.unity.com/packages/tools/modeling/trilib-model-loader-package-91777
https://www.google.com/search?hl=en&q=unity%20import%20fbx%20at%20runtime