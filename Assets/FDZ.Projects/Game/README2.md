
# THINGS THAT I WOULD LIKE TO HAVE
- Sealth
- Gravity curve for jump
- CHARACTER ACTIONS
    - Dark Souls: Shield + Speer = Special Attack
    - Directional Melee Attacks
    - Equip two weapons at a time and use them indepently
    - Aim down sight
    - Throw melee weapons
    - Light melee attack
    - Heavy melee attack
    - Kicking
    - Dash
    - Roll
    - Jump
    - Ladder (this can be used for on poles and rain gutters)
    - Ledge grabbing
    - Vaulting
    - Swimming
    - Standing, crouching, proning
    - Dark Souls: equip any weapon as one handed or two handed
    - Rainbow Six Siege: Leaning Left and Right
    - Escape From Tarkov: Shooting from cover
    - Walk alongside a wall and peak?
- VEHICLES
    - What about attacking when on a vehicle?
    - Horses
    - 4 wheeled (cars, vans, trucks, etc)
    - 2 wheeled (bikes, bycicles)
    - skates
    - Helicopters
    - VEHICLES THAT I WOULD THE CHARACTER TO BE ABLE TO WALK ON TOP OF THEM WHILE THEY MOVE:
        - Trains
        - Planes
        - Boats (for house boats like Sims 3)

# BASIC
W,A,S,D         Move            ...
Shift           Sprint          Cancel Aim
Alt             Dash            ...
Ctrl            Aim             Option for toggle

# MELEE 1
Click           Light Attack
Click And hold  Heavy Attack
Double Click    Pierce Attack

# MELEE 2
Control attack directions with mouse like Exanima or Mount and Blade?

# MOUSE
Click
Double Click
Hold
Click, Hold