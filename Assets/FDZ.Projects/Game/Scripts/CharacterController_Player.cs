﻿using Drawing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class CharacterController_Player : MonoBehaviour
    {
        public CameraController cameraController;
        public Character character;

        private void Start()
        {
            character.input = new CharacterInput();
            character.input.strafe = true;
        }

        private void Update()
        {
            HandleInput();
            character.Tick();
        }

        private void FixedUpdate()
        {
            character.FixedTick();
            character.input.jump = false;
        }

        private void LateUpdate()
        {
            character.LateTick();
        }

        private void HandleInput()
        {
            var input = character.input;

            input.aim = Input.GetMouseButton(1);
            input.fire = Input.GetMouseButtonDown(0);

            input.sprint = Input.GetKey(KeyCode.LeftShift);
            input.moveDirection = UtilsInput.GetMoveDirectionFromMainCamera();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                input.jump = true;
            }

            if (Input.GetKeyDown(KeyCode.Mouse2))
            {
                input.strafe = !input.strafe;
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                if (input.groundedState == CharacterGroundedState.Standing)
                {
                    input.groundedState = CharacterGroundedState.Crouching;
                }
                else if (input.groundedState == CharacterGroundedState.Crouching)
                {
                    input.groundedState = CharacterGroundedState.Standing;
                }
            }

            // V1
            //var p = new Vector2(Input.mousePosition.x - Screen.width / 2, Input.mousePosition.y - Screen.height / 2);
            //strafeDirection = Quaternion.Euler(0f, camCtrl.currentEulerAngles.y, 0f) * new Vector3(p.x, 0f, p.y).normalized;

            // V2 for topdown camera
            // https://docs.unity3d.com/ScriptReference/Plane.Raycast.html
            var cam = cameraController.GetComponent<Camera>();
            var plane = new Plane(Vector3.up, -cameraController.FollowPosition.y);
            var ray = cam.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.nearClipPlane));
            if (plane.Raycast(ray, out float hitDistance))
            {
                var hitPoint = ray.GetPoint(hitDistance);
                input.strafeDirection = (hitPoint - cameraController.FollowPosition).xoz().normalized;
                //Draw.WireSphere(hitPoint, .1f, Color.red);
                //Draw.Line(cameraController.FollowPosition, hitPoint, Color.red);
            }

            // V2 for orbit
            //var dir = camCtrl.transform.forward;
            //dir.y = 0f;
            //dir.Normalize();
            //strafeDirection = dir;

            character.input = input;
        }
    }
}
