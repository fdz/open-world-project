﻿using FDZ.Attributes;
using UnityEditor;
using UnityEngine;

namespace FDZ.Projects.Game
{
	public class CharacterMovement_Grounded : CharacterMovement
	{
		public float speedStanding = 8f;
        public float speedCrouching = 8f;
        public float jumpSpeed = 20f;
        public float dragAcceleration = 2f; // aka friction
        public float dragDeacceleration = 12f; // aka friction

        [SerializeField, ReadOnly] private bool isGrounded;
        [SerializeField, ReadOnly] private float drag;

        private CharacterController cc;
        private Quaternion lookRotation;

        private void Awake()
        {
			cc = GetComponent<CharacterController>();
        }

		public override void FixedTick(float deltaTime)
        {
            var groundState = Owner.input.sprint ? CharacterGroundedState.Standing : Owner.input.groundedState;
            var targetSpeed = 0f;

            if (groundState == CharacterGroundedState.Standing)
            {
                targetSpeed = speedStanding;
            }
            else if (groundState == CharacterGroundedState.Crouching)
            {
                targetSpeed = speedCrouching;
            }    

            var moveDirection = Owner.IsDead ? Vector3.zero : Owner.input.moveDirection.xoz().normalized;
            var acceleration = targetSpeed * dragAcceleration;

            Owner.velocity += moveDirection * acceleration * deltaTime;
            Owner.velocity += Physics.gravity * deltaTime;

            if (Owner.input.jump)
            {
                Owner.velocity.y = jumpSpeed;
            }

            var collisions = cc.Move(Owner.velocity * deltaTime);
            isGrounded = collisions.HasFlag(CollisionFlags.Below);
            if (isGrounded)
            {
                Owner.velocity.y = 0f;
            }

            // ANIMATIONS
            var d = transform.InverseTransformDirection(Owner.input.moveDirection);
            Owner.Anim.SetFloat("horizontal", d.x, .15f, deltaTime);
            Owner.Anim.SetFloat("vertical",   d.z, .15f, deltaTime);
            Owner.Anim.SetBool("isGrounded", isGrounded);
            Owner.Anim.SetInteger("groundedState", (int)groundState);

            // DRAG
            drag = moveDirection.sqrMagnitude != 0f ? dragAcceleration : dragDeacceleration;
            Owner.velocity -= Owner.velocity.xoz() * (drag * deltaTime);

            // ROTATE
            var lookDirection = Owner.IsDead ? Vector3.zero : Owner.input.strafe ? Owner.input.strafeDirection.xoz().normalized : moveDirection;
            if (lookDirection.sqrMagnitude != 0f)
            {
                lookRotation = Quaternion.LookRotation(lookDirection);
            }
            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, deltaTime * rotationSpeed);

        }
	}
}