﻿using FDZ.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class Character : MonoBehaviour
    {
        public Vector3 velocity;
        public Transform gunShootPoint;
        public CharacterInput input;

        public CharacterAction actionPrimary = new CharacterAction_MeleeAttack();
        public CharacterAction actionSecondary = new CharacterAction_Shoot();

        public float healthMax;
        [ReadOnly] private float healthCurrent;

        public bool IsAlive { get => healthCurrent > 0f; }
        public bool IsDead { get => healthCurrent <= 0f; }

        public CharacterMovement CurrentMovementState { get; private set; }

        public Animator Anim { get; private set; }

        private CharacterMovement_Grounded movementState_Grounded;
        private CharacterMovement_Dead movementState_Dead;

        private void Awake()
        {
            Anim = GetComponentInChildren<Animator>();
            
            movementState_Grounded = GetComponent<CharacterMovement_Grounded>();
            movementState_Grounded.Init(this);

            movementState_Dead = GetComponent<CharacterMovement_Dead>();
            movementState_Dead.Init(this);

            ChangeMovementState(movementState_Grounded);

            healthCurrent = healthMax;
        }

        public void Tick()
        {
            actionSecondary.Update(this);
            actionPrimary?.Update(this);
        }

        public void FixedTick()
        {
            CurrentMovementState.FixedTick(Time.deltaTime);
        }

        public void LateTick()
        {
            CurrentMovementState.LateTick(Time.deltaTime);
        }

        public void ApplyDamage(float damage)
        {
            if (damage > 0f && healthCurrent > 0f)
            {
                healthCurrent -= damage;
                if (healthCurrent <= 0f)
                {
                    Debug.Log($"{name} died");
                    Anim.CrossFade("Standing Death Backward 01", .25f);
                    ChangeMovementState(movementState_Dead);
                }
            }
        }

        private void ChangeMovementState(CharacterMovement newState)
        {
            CurrentMovementState = newState;
            CurrentMovementState.Begin();
        }
    }
}