﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class SimpleCameraInput : MonoBehaviour
    {
        public bool invertX;
        public bool invertY;
        public Vector2 sensitivity = Vector2.one;
        public bool setLockModeOnEnable;
        public CursorLockMode cursorLockMode;

        SimpleCamera simpleCamera;
        bool hasFocus;

        private void Awake()
        {
            simpleCamera = GetComponent<SimpleCamera>();
        }
        private void OnEnable()
        {
            if (setLockModeOnEnable)
            {
                Cursor.lockState = cursorLockMode;
                hasFocus = true;
            }
        }

        private void LateUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cursor.lockState = CursorLockMode.None;
                hasFocus = false;
            }

            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(0))
            {
                Cursor.lockState = cursorLockMode;
                hasFocus = true;
            }

            if (hasFocus)
            {
                var mouseDelta = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
                simpleCamera.eulerAngles.x += mouseDelta.y * sensitivity.y * (invertY ? -1f : 1f);
                simpleCamera.eulerAngles.y += mouseDelta.x * sensitivity.x * (invertX ? -1f : 1f);
            }
        }

        private void OnMouseDrag()
        {
            Debug.Log("dragged");
        }
    }
}