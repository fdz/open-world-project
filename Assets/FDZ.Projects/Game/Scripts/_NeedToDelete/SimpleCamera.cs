﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    // TODO: Camera Clipping
    // https://www.youtube.com/playlist?list=PL4CCSwmU04MjDqjY_gdroxHe85Ex5Q6Dy
    // https://catlikecoding.com/unity/tutorials/movement/orbit-camera/

    public enum UpdateMode
    {
        None,
        Update,
        LateUpdate,
        FixedUpdate,
    }

    [ExecuteInEditMode]
    public class SimpleCamera : MonoBehaviour
    {
        [SerializeField] private Transform pivot = null;
        [SerializeField] private Transform cam = null;

        [Space]
        public float followSpeed;
        public Transform target;

        [Space]
        public UpdateMode updateMode;
        public Vector2 offset;
        public Vector2 eulerAngles;

        private void Update()
        {
            if (updateMode == UpdateMode.Update && Application.isPlaying)
            {
                UpdateFollowTarget(target.position);
            }
        }

        private void FixedUpdate()
        {
            if (updateMode == UpdateMode.FixedUpdate && Application.isPlaying)
            {
                UpdateFollowTarget(target.position);
            }
        }

        private void LateUpdate()
        {
            if (Application.isEditor && Application.isPlaying == false && target != null && pivot != null && cam != null)
            {
                transform.position = target.position;

                pivot.localPosition = new Vector3(0f, offset.y, 0f);
                cam.localPosition = new Vector3(0f, 0f, offset.x);

                transform.eulerAngles = new Vector3(0f, eulerAngles.y, 0f);
                pivot.localEulerAngles = new Vector3(eulerAngles.x, 0f, 0f);
            }
            if (updateMode == UpdateMode.LateUpdate && Application.isPlaying)
            {
                UpdateFollowTarget(target.position);
            }
        }

        public void UpdateFollowTarget(Vector3 position)
        {
            if (target == null || pivot == null || cam == null)
            {
                return;
            }

            transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * followSpeed);

            pivot.localPosition = new Vector3(0f, offset.y, 0f);
            cam.localPosition = new Vector3(0f, 0f, offset.x);

            transform.eulerAngles = new Vector3(0f, eulerAngles.y, 0f);
            pivot.localEulerAngles = new Vector3(eulerAngles.x, 0f, 0f);
        }
    }
}
