﻿using Drawing;
using FDZ.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class MovementKinematicFloating : MonoBehaviourGizmos
    {
        [Space]
        public Vector3 input_movement;
        public bool input_jump;
        [SerializeField, ReadOnly] private Transform movingPlatform;
        private Transform previousMovingPlatform;
        private Vector3 movingPlatform_previousPosition;
        private Quaternion movingPlatform_previousRotation;
        [Space]
        public float acceleration = 15f;
        public float gravity = -20f;
        public float friction = 3f;
        public float jumpForce = 10f;
        [Space]
        [ReadOnly] public bool isGrounded;
        [ReadOnly] public bool isOnSteepGround;
        [ReadOnly] public bool isJumping;
        public LayerMask collisionsLayerMask = -1; // -1 = everything, 0 = none
        public float skinWidth = .1f;
        public bool pushDynamicRigidbodies;
        public float ground_maxAngle = 45f;
        [Space]
        public int ground_circleRayCount = 20;
        public float ground_circleCastOriginY = 1f;
        public float ground_circleCastRadius = 1f;
        public float ground_circleCastDistance = 1f;
        [Space]
        public float ground_forwardRayCastDistance = 2f;
        public float ground_forwardRayCastOffset = .1f;
        [Space]
        [SerializeField, Range(0f, 1f), DontEditInPlayMode] private float ground_castDistanceMultiplier = 1f;
        [SerializeField] private float ground_castDistanceMultiplierAcceleration = 8f;
        [SerializeField] private float ground_castMinDistance = .5f;

        private Vector3 velocity;
        private Collider[] overlapResults = new Collider[16];
        private Vector3 ground_point;
        private Vector3 ground_normal;

        public Rigidbody Rb { get; private set; }

        public CapsuleCollider Capsule { get; private set; }

        public float GroundCastDistance { get => Mathf.Max(ground_castMinDistance, ground_circleCastDistance * ground_castDistanceMultiplier);  }

        public override void DrawGizmos()
        {
            if (Rb == null)
            {
                Rb = GetComponent<Rigidbody>();
            }
            if (Capsule == null)
            {
                Capsule = GetComponent<CapsuleCollider>();
            }
            Draw.WireSphere(Rb.position, .025f);
            Draw.Ray(Rb.position, velocity.normalized, Color.green);
            if (isGrounded)
            {
                Draw.Ray(ground_point, ground_normal, Color.blue);
            }
            Draw.Ray(
                Rb.position + Vector3.up * ground_circleCastOriginY,
                Vector3.down * GroundCastDistance,
                Color.red);
            Draw.Ray(
                Rb.position + Vector3.up * ground_circleCastOriginY + transform.forward * ground_forwardRayCastOffset,
                Vector3.down * GroundCastDistance,
                Color.red);
            var center = Rb.position + Vector3.up * (ground_circleCastOriginY);
            var angle = 360 / ground_circleRayCount;
            for (int i = 0; i <= ground_circleRayCount; i++)
            {
                var point = Utils.Vector3RotateAroundPoint(center + Vector3.forward * ground_circleCastRadius, center, Quaternion.Euler(0f, angle * i, 0));
                Draw.Ray(
                    point,
                    Vector3.down * GroundCastDistance,
                    Color.red);
            }
        }

        private void Awake()
        {
            Rb = GetComponent<Rigidbody>();
            Rb.isKinematic = true;
            Rb.interpolation = RigidbodyInterpolation.Interpolate;
            Rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
            /****************************************************/
            Capsule = GetComponent<CapsuleCollider>();
            /****************************************************/
            ground_castDistanceMultiplier = 0f;
        }

        private void FixedUpdate()
        {
            bool Raycast(Vector3 atPosition, out RaycastHit rayHit, float distance)
            {
                return Physics.Raycast(
                    atPosition,
                    Vector3.down,
                    out rayHit,
                    distance,
                    collisionsLayerMask,
                    QueryTriggerInteraction.Ignore);
            };
            Vector3 moveDirection = new Vector3(input_movement.x, 0f, input_movement.z).normalized;
            Vector3 nextPosition = Rb.position;
            Quaternion nextRotation = Rb.rotation;
            if (movingPlatform != null)
            {
                if (movingPlatform == previousMovingPlatform)
                {
                    var rotationDifference =
                        Quaternion.AngleAxis(Utils.QuaternionSingedAngle(
                            movingPlatform_previousRotation,
                            movingPlatform.rotation),
                            movingPlatform.up);
                    nextPosition =
                        (movingPlatform.position - movingPlatform_previousPosition) +
                        Utils.Vector3RotateAroundPoint(
                            nextPosition,
                            movingPlatform.position,
                            rotationDifference);
                    nextRotation = Quaternion.Euler(0f, rotationDifference.eulerAngles.y, 0f) * nextRotation;
                }
                movingPlatform_previousRotation = movingPlatform.rotation;
                movingPlatform_previousPosition = movingPlatform.position;
            }
            previousMovingPlatform = movingPlatform;
            movingPlatform = null;
            bool wasGrounded = isGrounded;
            isGrounded = false;
            isOnSteepGround = false;
            var wasJumping = isJumping;
            if (input_jump)
            {
                input_jump = false;
                velocity.y = jumpForce;
                isJumping = true;
                ground_castDistanceMultiplier = 0f;
            }
            else
            {
                var centerPoint = nextPosition + Vector3.up * (ground_circleCastOriginY);
                var angle = 360 / ground_circleRayCount;
                var hitCount = 0;
                RaycastHit rayHit;
                ground_point = Vector3.zero;
                ground_normal = Vector3.zero;
                for (int i = 0; i <= ground_circleRayCount; i++)
                {
                    var point = Utils.Vector3RotateAroundPoint(centerPoint + Vector3.forward * ground_circleCastRadius, centerPoint, Quaternion.Euler(0f, angle * i, 0));
                    if (Raycast(point, out rayHit, ground_circleCastDistance))
                    {
                        isGrounded = true;
                        ground_point += rayHit.point;
                        ground_normal += rayHit.normal;
                        hitCount++;
                        var colliderTags = rayHit.collider.GetComponent<Tags>();
                        if (colliderTags != null && colliderTags.Has("moving_platform"))
                        {
                            movingPlatform = rayHit.collider.transform;
                        }
                    }
                }
                if (isGrounded)
                {
                    ground_point /= hitCount;
                    ground_normal /= hitCount;
                    if (wasGrounded == false && nextPosition.y >= ground_point.y && centerPoint.y - ground_point.y > GroundCastDistance)
                    {
                        isGrounded = false;
                    }
                }
            }
            if (isGrounded)
            {
                velocity -= velocity * Time.deltaTime * friction;
                if(wasGrounded == false)
                {
                    velocity.y = 0;
                }
                nextPosition.y = ground_point.y;
                bool flatten = true;
                var centerPoint = new Vector3(nextPosition.x, nextPosition.y + ground_circleCastOriginY, nextPosition.z);
                if (Raycast(
                        centerPoint,
                        out RaycastHit hitCenter,
                        GroundCastDistance)
                    && Raycast(
                        centerPoint + new Vector3(velocity.x, 0f, velocity.z).normalized * ground_forwardRayCastOffset,
                        out RaycastHit hitForward,
                        GroundCastDistance)
                    && Raycast(
                        centerPoint + moveDirection * ground_forwardRayCastOffset,
                        out RaycastHit hitForwardAux,
                        GroundCastDistance))
                {
                    var newDir = (hitForward.point - hitCenter.point).normalized;
                    var newDirAngle = 90 - Vector3.Angle(newDir, Vector3.up);
                    if (Mathf.Abs(newDirAngle) <= ground_maxAngle)
                    {
                        flatten = false;
                        velocity = newDir * velocity.magnitude;
                        velocity += (hitForwardAux.point - hitCenter.point).normalized * Time.deltaTime * acceleration;
                    }
                    else
                    {
                        isOnSteepGround = true;
                    }
                }
                if (flatten) // this is useful if the character is about to fall
                {
                    velocity = new Vector3(velocity.x, 0f, velocity.z).normalized * velocity.magnitude;
                    velocity += moveDirection * Time.deltaTime * acceleration;
                }
                nextPosition = new Vector3(nextPosition.x, nextPosition.y, nextPosition.z) + velocity * Time.deltaTime;
                ground_castDistanceMultiplier = 1f;
            }
            else
            {
                if (wasGrounded)
                {
                    ground_castDistanceMultiplier = 0f;
                }
                velocity -= new Vector3(velocity.x, 0f, velocity.z) * Time.deltaTime * friction;
                velocity += moveDirection * Time.deltaTime * acceleration;
                velocity += Vector3.down * Time.deltaTime * gravity;
                nextPosition += velocity * Time.deltaTime;
                ground_castDistanceMultiplier += Time.deltaTime * ground_castDistanceMultiplierAcceleration;
                ground_castDistanceMultiplier = Mathf.Clamp01(ground_castDistanceMultiplier);
            }
            int overlapsCount =
                Physics.OverlapSphereNonAlloc(
                    nextPosition,
                    Capsule.center.y + Capsule.radius + Capsule.height,
                    overlapResults,
                    collisionsLayerMask,
                    QueryTriggerInteraction.Ignore);
            if (overlapsCount > 1)
            {
                if (pushDynamicRigidbodies == false)
                {
                    Capsule.radius += skinWidth;
                }
                for (int i = 0; i < overlapsCount; i++)
                {
                    var otherCollider = overlapResults[i];
                    if (otherCollider != Capsule &&
                        Physics.ComputePenetration(
                            Capsule,
                            nextPosition,
                            nextRotation,
                            otherCollider,
                            otherCollider.transform.position,
                            otherCollider.transform.rotation,
                            out Vector3 direction,
                            out float distance))
                    {
                        var otherRb = otherCollider.GetComponent<Rigidbody>();
                        if (pushDynamicRigidbodies && otherRb != null && otherRb.isKinematic == false)
                        {
                            nextPosition += direction * (distance / (1 + otherRb.mass));
                            velocity -= velocity * Time.deltaTime * otherRb.mass;
                        }
                        else
                        {
                            nextPosition += direction * distance;
                            velocity -= Vector3.Project(velocity, -direction);
                        }
                    }
                }
                if (pushDynamicRigidbodies == false)
                {
                    Capsule.radius -= skinWidth;
                }
            }
            Rb.MovePosition(nextPosition);
            Rb.MoveRotation(nextRotation);
        }
    }
}