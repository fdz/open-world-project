﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
	[ExecuteInEditMode]
	public class SimpleCameraTargets : MonoBehaviour
	{
		[System.Serializable]
		public struct Target
		{
			public Transform transform;
			public UpdateMode updateMode;
		}

		public KeyCode keyCodePrevious = KeyCode.Comma;
		public KeyCode keyCodeNext = KeyCode.Period;
		public int currentTarget;
		public Target[] targets;

		private void Awake()
		{
			if (Application.isPlaying)
			{
				Array.ForEach(targets, x => x.transform.gameObject.SetActive(false));
			}
		}

		private void Update()
		{
			if (targets == null || targets.Length == 0)
			{
				return;
			}

			if (Input.GetKeyDown(keyCodeNext))
			{
				currentTarget++;
				if (Application.isPlaying)
				{
					GetComponent<SimpleCamera>().target.gameObject.SetActive(false);
				}
			}
			if (Input.GetKeyDown(keyCodePrevious))
			{
				currentTarget--;
				if (Application.isPlaying)
				{
					GetComponent<SimpleCamera>().target.gameObject.SetActive(false);
				}
			}

			if (currentTarget < 0)
			{
				currentTarget = targets.Length - 1;
			}
			else if (currentTarget >= targets.Length)
			{
				currentTarget = 0;
			}

			GetComponent<SimpleCamera>().target = targets[currentTarget].transform;
			GetComponent<SimpleCamera>().updateMode = targets[currentTarget].updateMode;

			if (Application.isPlaying)
			{
				GetComponent<SimpleCamera>().target.gameObject.SetActive(true);
			}
		}
	}
}