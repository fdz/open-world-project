﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class MovementKinematicFloating_Player : MonoBehaviour
	{
		public MovementKinematicFloating character;

        private void Update()
        {
            character.input_movement = UtilsInput.GetMoveDirectionFromMainCamera();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                character.input_jump = true;
            }
        }
	}
}