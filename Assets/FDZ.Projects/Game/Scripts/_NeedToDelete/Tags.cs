﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
	public class Tags : MonoBehaviour
	{
		public List<string> tags = new List<string>();

		public bool Has(string tag)
		{
			return tags.Exists(x => x == tag);
		}
	}
}