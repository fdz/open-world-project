﻿using Drawing;
using FDZ;
using FDZ.Attributes;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;

namespace FDZ.Projects.Game
{
    // TODO: Catlikecoding movement tutorial
    // https://catlikecoding.com/unity/tutorials/movement/

    // TODO: How the fuck can I avoid the slide on landing?

    // TODO: Add rootmotion

    public class CharacterMovement_Grounded_Old : CharacterMovement
    {
        // https://gamedev.stackexchange.com/questions/146738/how-to-disable-gravity-for-a-rigidbody-moving-on-a-slope/169102#169102

        public float gravityMultiplier = 1f;
        public float moveForceWalk;
        public float moveForceSprint;
        public float dragAcceleration;
        public float dragDeacceleration;
        //public float rotationSpeed = 12f;
        public float jumpImpulse;

        private Quaternion lookRotation;

        [SerializeField, ReadOnly] private bool _isGrounded;

        public bool IsGrounded { get => _isGrounded; private set => _isGrounded = value; }
        
        public Vector3 GroundNormal { get; private set; }

        public Collider GroundCollider { get; private set; }

        public Rigidbody Rb { get; private set; }
        
        public CapsuleCollider CapColl { get; private set; }

        public Transform Gfx { get; private set; }

        private void Awake()
        {
            Physics.defaultSolverIterations = 15;

            Rb = GetComponent<Rigidbody>();
            CapColl = GetComponent<CapsuleCollider>();
            Gfx = transform.GetChild(0);

            Rb.drag = 0f;
            Rb.useGravity = false;
            Rb.isKinematic = false;
            Rb.interpolation = RigidbodyInterpolation.Interpolate;
            Rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            Rb.constraints = RigidbodyConstraints.FreezeRotation;
        }

        private void OnCollisionStay(Collision collision)
        {
        }

        private void OnCollisionEnter(Collision collision)
        {
            var contanctPoints = new ContactPoint[collision.contactCount];
            collision.GetContacts(contanctPoints);
            foreach (ContactPoint cp in contanctPoints)
            {
                if (Vector3.Angle(cp.normal, -Physics.gravity.normalized) < 45f)
                {
                    if (IsGrounded == false)
                    {
                        Debug.Log("just landed");
                        //Rb.velocity = Rb.velocity.x0z();
                    }
                    IsGrounded = true;
                    GroundNormal = cp.normal;
                    GroundCollider = collision.collider;
                    return;
                }
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            if (IsGrounded && collision.collider == GroundCollider)
            {
                IsGrounded = false;
            }
        }

        public void FixedTick()
        {
            var moveDirection = Owner.input.moveDirection;
            if (IsGrounded)
            {
                moveDirection = Vector3.ProjectOnPlane(moveDirection, Vector3.up);
            }
            var force = Owner.input.sprint ? moveForceSprint : moveForceWalk;
            Rb.AddForce(moveDirection * force);

            if (IsGrounded)
            {
                //Rb.AddForce(-GroundNormal * (Physics.gravity.magnitude * gravityMultiplier), ForceMode.Force);
            }
            else
            {
                Debug.Log("falling");
                Rb.AddForce(Physics.gravity * gravityMultiplier);
            }

            var drag = Mathf.Max(0f, moveDirection.sqrMagnitude == 0f ? dragDeacceleration : dragAcceleration);
            Rb.AddForce(-Rb.velocity.xoz() * drag);

            if (Owner.input.jump && IsGrounded)
            {
                Rb.AddForce(new Vector3(0f, jumpImpulse, 0f), ForceMode.Impulse);
            }

            if (Owner.input.strafe)
            {
                var dir = new Vector3(Owner.input.strafeDirection.x, 0f, Owner.input.strafeDirection.z).normalized;
                if (dir.sqrMagnitude != 0f)
                {
                    lookRotation = Quaternion.LookRotation(dir);
                }
            }
            else
            {
                var dir = new Vector3(moveDirection.x, 0f, moveDirection.z).normalized;
                if (dir.sqrMagnitude != 0f)
                {
                    lookRotation = Quaternion.LookRotation(dir);
                }
            }

            // SET ROTATION
            //transform.rotation = lookRotation;

            // LERP ROTATION
            transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);

            var d = transform.InverseTransformDirection(Owner.input.moveDirection);
            Owner.Anim.SetFloat("horizontal", d.x, .1f, Time.deltaTime);
            Owner.Anim.SetFloat("vertical",   d.z, .1f, Time.deltaTime);
            Owner.Anim.SetBool("isGrounded", IsGrounded);

            // LERP AND TILT ROTATION
            //var rotationSpeed = this.rotationSpeed * 1f;
            //var tiltAngle = Owner.input.sprint ? 12f : 7f;
            //if (Owner.input.strafe)
            //{
            //    var inverseMoveDirection = transform.InverseTransformDirection(moveDirection);
            //    var tilt = Quaternion.Euler(inverseMoveDirection.z * tiltAngle, 0f, -inverseMoveDirection.x * tiltAngle);
            //    transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation * tilt, Time.deltaTime * rotationSpeed);
            //}
            //else
            //{
            //    var tilt = Quaternion.Euler((Mathf.Abs(moveDirection.z) + Mathf.Abs(moveDirection.x)) * tiltAngle, 0f, 0f);
            //    transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation * tilt, Time.deltaTime * rotationSpeed);
            //}

            if (false)
            //if (IsGrounded == false)
            {
                var apply = false;
                var position = Rb.position + Rb.velocity * Time.deltaTime;
                var colliders = Physics.OverlapSphere(position, 10f);
                foreach (var otherCollider in colliders)
                {
                    if (otherCollider == CapColl || otherCollider.isTrigger || !otherCollider.enabled)
                    {
                        continue;
                    }
                    var first = false;
                    while (
                        Physics.ComputePenetration(
                            CapColl, position, transform.rotation,
                            otherCollider, otherCollider.transform.position, otherCollider.transform.rotation,
                            out Vector3 direction, out float distance) &&
                        Vector3.Angle(direction, -Physics.gravity.normalized) < 45f)
                    {
                        apply = true;
                        //position.y += first ? distance : .001f;
                        position.y += .001f;
                    }
                }
                if (apply)
                {
                    Debug.Log("applied");
                    Rb.velocity = Rb.velocity.xoz();
                    Rb.position = position;
                }
            }
        }
    } // end of class
} // end of namespace 