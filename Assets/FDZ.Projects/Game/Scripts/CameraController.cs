﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    [ExecuteInEditMode]
    public class CameraController : MonoBehaviour
    {
        public bool disableInput;
        public UpdateMode updateMode;
        public Transform target;
        public Vector3 offset;
        public float radius;
        public Vector2 currentEulerAngles;
        public float sensitivity;
        public bool followLerp = true;
        public float followSpeed = 8f;

        public Vector3 FollowPosition { get; private set; }

        private bool applicacionHasFocus;
        private float targetY;

        private void Awake()
        {
        }

        private void Start()
        {
            if (target != null)
            {
                FollowPosition = target.position + offset;
            }
            targetY = currentEulerAngles.y;
        }

        private void OnApplicationFocus(bool focus)
        {
            applicacionHasFocus = focus;
        }

        private void Update()
        {
            if (Application.isPlaying)
            {
                if (disableInput == false)
                {
                    if (Input.GetMouseButton(0))
                    {
                        Cursor.lockState = CursorLockMode.Locked;
                    }
                    if (Input.GetKeyDown(KeyCode.Escape))
                    {
                        Cursor.lockState = CursorLockMode.None;
                    }
                    if (applicacionHasFocus && Cursor.lockState == CursorLockMode.Locked)
                    {
                        currentEulerAngles.x += Input.GetAxisRaw("Mouse Y") * sensitivity * (true ? -1f : 1f);
                        currentEulerAngles.y += Input.GetAxisRaw("Mouse X") * sensitivity * (false ? -1f : 1f);
                    }
                }

                targetY -= 90 * UtilsInput.GetAxisDown(KeyCode.Q, KeyCode.E);
                currentEulerAngles = 
                    Quaternion.Lerp(
                        Quaternion.Euler(currentEulerAngles),
                        Quaternion.Euler(currentEulerAngles.x, targetY, 0f),
                        Time.deltaTime * 8f)
                    .eulerAngles;
            }

            if (updateMode == UpdateMode.Update && Application.isPlaying)
            {
                Tick(Time.deltaTime);
            }
        }

        private void FixedUpdate()
        {
            if (updateMode == UpdateMode.FixedUpdate && Application.isPlaying)
            {
                Tick(Time.deltaTime);
            }
        }

        private void LateUpdate()
        {
            if (updateMode == UpdateMode.LateUpdate && Application.isPlaying || Application.isEditor && Application.isPlaying == false)
            {
                Tick(Time.deltaTime);
            }
        }

        public void Tick(float delta)
        {
            if (target == null)
            {
                return;
            }

            if (currentEulerAngles.x > 89)
            {
                currentEulerAngles.x = 89;
            }
            if (currentEulerAngles.x < -89)
            {
                currentEulerAngles.x = -89;
            }

            while (currentEulerAngles.y < -180)
            {
                currentEulerAngles.y += 360;
            }
            while (currentEulerAngles.y > 180)
            {
                currentEulerAngles.y -= 360;
            }

            if (radius <= 0.1f)
            {
                radius = 0.1f;
            }

            Vector3 targetPosition = target.position + offset;
            Vector3 lookDirection = (Quaternion.Euler(currentEulerAngles) * Vector3.forward).normalized;
            if (followLerp)
            {
                FollowPosition = Vector3.Lerp(FollowPosition, targetPosition, delta * followSpeed);
            }
            else
            {
                FollowPosition = targetPosition;
            }

            transform.SetPositionAndRotation(
                FollowPosition - lookDirection * radius,
                Quaternion.LookRotation(lookDirection));
        }
    }
}
