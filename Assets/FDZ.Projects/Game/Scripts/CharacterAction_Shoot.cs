﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class CharacterAction_Shoot : CharacterAction
    {
        const int shotsPerSecond = 3;

        private float startTime;
        private float lastShotFiredTime;
        private float finishedShootingTimer;

        public override void Update(Character owner)
        {
            if (owner.actionPrimary.IsActive || owner.IsDead)
            {
                return;
            }

            float GetCurTime() => Time.time - startTime;
            float GetShootCooldown() => 1f / shotsPerSecond;

            Vector3 shootPoint = owner.transform.position + Vector3.up * 1.45f;
            Vector3 lookDirection = owner.transform.forward;
            float shootDistance = 10f;

            if (owner.input.aim)
            {
                if (IsActive == false)
                {
                    IsActive = true;
                    startTime = Time.time;
                    lastShotFiredTime = startTime * .5f;
                    owner.CurrentMovementState.rotationSpeed = 16f;
                    finishedShootingTimer = 0f;
                }

                var timeSinceLastShotFired = Time.time - lastShotFiredTime;
                var color = timeSinceLastShotFired >= GetShootCooldown() ? new Color(1f, .5f, 0f) : Color.yellow;
                //Debug.DrawRay(shootPoint, shootDirection * shootDistance, color);
            }
            else
            {
                if (IsActive)
                {
                    if (finishedShootingTimer <= 0f)
                    {
                        IsActive = false;
                        owner.CurrentMovementState.rotationSpeed = 8f;
                    }
                    else
                    {
                        finishedShootingTimer -= Time.deltaTime;
                    }
                }
            }

            if (IsActive)
            {
                //if (GetCurTime() >= .1f) // finished aim animation
                {
                    var timeSinceLastShotFired = Time.time - lastShotFiredTime;
                    if (owner.input.fire && (timeSinceLastShotFired >= GetShootCooldown()))
                    {
                        lastShotFiredTime = Time.time;
                        owner.Anim.SetTrigger("shoot");
                        finishedShootingTimer = .1f;

                        float value = 3f;
                        float GetRandom() => Random.Range(-value, value);
                        var shootDirection = Quaternion.Euler(0f, GetRandom(), 0f) * lookDirection;
                        
                        //Debug.DrawRay(shootPoint, randomDirection * shootDistance, Color.red, .5f);

                        var hits = Physics.RaycastAll(
                            shootPoint,
                            shootDirection,
                            shootDistance,
                            -1,
                            QueryTriggerInteraction.Ignore);

                        var hitNothing = true;

                        foreach (var hit in hits)
                        {
                            var other = hit.transform.GetComponent<Character>();
                            if (other == null || other == owner || other.IsDead)
                            {
                                continue;
                            }
                            Debug.Log($"{owner.gameObject.name} shot {other.gameObject.name}");
                            ShootTrailPool.Instance.Spawn(owner.gunShootPoint.position, hit.point);
                            other.ApplyDamage(1f);
                            hitNothing = false;
                            break;
                        }

                        if (hitNothing)
                        {
                            ShootTrailPool.Instance.Spawn(owner.gunShootPoint.position, owner.gunShootPoint.position + shootDirection * shootDistance);
                        }
                    }
                }
            }

            owner.Anim.SetBool("isAiming", IsActive);
        }
    }

}