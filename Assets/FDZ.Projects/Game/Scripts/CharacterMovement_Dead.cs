﻿using UnityEngine;

namespace FDZ.Projects.Game
{
	public class CharacterMovement_Dead : CharacterMovement
    {
        public Transform root;

        private CharacterController cc;
        private Vector3 rootPreviousLocalPosition;
        private Vector3 rootDeltaPosition;

        private void Awake()
        {
            cc = GetComponent<CharacterController>();
        }

        public override void Begin()
        {
            rootPreviousLocalPosition = new Vector3(0f, root.localPosition.y, 0f);
        }

        public override void FixedTick(float deltaTime)
        {
            Owner.velocity.x = rootDeltaPosition.x / deltaTime;
            Owner.velocity.z = rootDeltaPosition.z / deltaTime;
            Owner.velocity.y += Physics.gravity.y * deltaTime;

            var collisions = cc.Move(Owner.velocity * deltaTime);

            var isGrounded = collisions.HasFlag(CollisionFlags.Below);
            if (isGrounded)
            {
                Owner.velocity.y = 0f;
            }
        }

        public override void LateTick(float deltaTime)
        {
            var rootCurrentPosition = root.localPosition.xoz();
            rootDeltaPosition = transform.rotation * (rootCurrentPosition - rootPreviousLocalPosition);
            rootPreviousLocalPosition = rootCurrentPosition;
            root.localPosition = new Vector3(0f, root.localPosition.y, 0f);
        }
    }
}