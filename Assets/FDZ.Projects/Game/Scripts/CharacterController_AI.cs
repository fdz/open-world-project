﻿using FDZ.Attributes;
using UnityEngine;
using Drawing;

namespace FDZ.Projects.Game
{
	public class CharacterController_AI : MonoBehaviour
	{
		public Character character;
        public Character enemy;
        
        [Space]
        public float visionAngle = 75f;
        [ReadOnly] public float enemyAngle;
        
        [Space]
        public float visionDistance = 15f;
        [ReadOnly] public float enemyDistance;

        [Space]
        public float visionSpotTimeTreshold = .1f;
        private float visionSpotTimer;

        private void Start()
        {
            character.input = new CharacterInput();
            character.input.strafe = true;
        }

        private void Update()
        {
            HandleInput();
            character.Tick();
        }

        private void FixedUpdate()
        {
            character.FixedTick();
            character.input.jump = false;
        }

        private void LateUpdate()
        {
            character.LateTick();
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            if (gameObject.activeInHierarchy == false || enabled == false)
                return;
            UnityEditor.Handles.color = new Color(1f, 0f, 0f, .1f);
            UnityEditor.Handles.DrawSolidArc(character.transform.position + Vector3.up * .1f, character.transform.up, character.transform.forward, visionAngle, visionDistance);
            UnityEditor.Handles.DrawSolidArc(character.transform.position + Vector3.up * .1f, character.transform.up, character.transform.forward, -visionAngle, visionDistance);
        }
#endif

        private void HandleInput()
        {
            enemyAngle = Vector2.Angle(
                character.transform.forward.xz(),
                enemy.transform.position.xz() - character.transform.position.xz());

            enemyDistance = Vector2.Distance(
                character.transform.position.xz(),
                enemy.transform.position.xz());

            if (enemyAngle > visionAngle || enemyDistance > visionDistance)
            {
                visionSpotTimer = visionSpotTimeTreshold;
                character.input.aim = false;
                character.input.fire = false;
                return;
            }

            if (visionSpotTimer > 0f)
            {
                visionSpotTimer -= Time.deltaTime;
                return;
            }

            var input = character.input;

            var dif = enemy.transform.position - character.transform.position;
            var dir = dif.normalized;
            var dis = dif.magnitude;

            input.strafeDirection = dir;

            if (dis > 1.465f)
            {
                //input.moveDirection = dir;
            }
            else
            {
                input.moveDirection = Vector3.zero;
            }

            if (dis > 3f)
            {
                input.aim = true;
                input.fire = true;
            }
            else
            {
                input.aim = false;
                input.fire = true;
            }

            character.input = input;
        }
    }
}