﻿using UnityEngine;

namespace FDZ.Projects.Game
{
    [System.Serializable]
    public struct CharacterInput
    {

        //public enum SpeedStates { Walk, Jog, Sprint }
        //public SpeedStates speedState;
        public bool aim;
        public bool fire;
        public bool jump;
        public bool dash;
        public bool strafe;
        public bool sprint;
        public CharacterGroundedState groundedState;
        public Vector3 moveDirection;
        public Vector3 strafeDirection;
    }
}