﻿using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
    public class CharacterAction_MeleeAttack : CharacterAction
    {
        // TODO: Remove hardcoding of animations
        // TODO: Define what types of 

        public struct Clip
        {
            public string name;
            public float duration;
            public float damageStartTime;
            public float damageEndTime;
        }

        private Clip currentClip;

        private Clip clip1 = new Clip()
        {
            name = "Melee1",
            duration = 0.917f,
            damageStartTime = 0.15f,
            damageEndTime = 0.5f,
        };

        private Clip clip2 = new Clip()
        {
            name = "Melee2",
            duration = 1.150f,
            damageStartTime = 0.15f,
            damageEndTime = 0.6f,
        };

        private bool inAnimation;
        private float startTime;
        private int currentState = 0;
        private HashSet<Character> hitCharacters = new HashSet<Character>();

        public override void Update(Character owner)
        {
            if (owner.actionSecondary.IsActive || owner.IsDead)
            {
                return;
            }

            float GetCurTime() => Time.time - startTime;

            // START ATTACKING
            if (IsActive == false && owner.input.fire) // TODO: Hardcoded input detection, this does not work for AI, and also, doesn't work if I want weapons to be equipped in any hand
            {
                IsActive = true;
                inAnimation = true;
                owner.CurrentMovementState.rotationSpeed = 16f;

                if (currentState == 1)
                {
                    currentClip = clip1;
                    if (GetCurTime() >= currentClip.duration)
                    {
                        currentState = 0;
                    }
                }

                if (currentState == 0)
                {
                    currentClip = clip1;
                    owner.Anim.CrossFade(currentClip.name, .1f);
                }
                else
                {
                    currentClip = clip2;
                    owner.Anim.CrossFade(currentClip.name, .1f);
                }

                startTime = Time.time;

                currentState++;
                if (currentState > 1)
                {
                    currentState = 0;
                }
            }

            if (inAnimation && GetCurTime() >= currentClip.duration)
            {
                inAnimation = false;
                owner.CurrentMovementState.rotationSpeed = 8f;
            }

            if (IsActive)
            {
                owner.input.sprint = false;
            }

            // END ATTACK
            if (IsActive && GetCurTime() >= currentClip.damageEndTime)
            {
                IsActive = false;
                hitCharacters.Clear();
            }

            // DURING ATTACK
            if (IsActive && GetCurTime() >= currentClip.damageStartTime)
            {
                owner.CurrentMovementState.rotationSpeed = 0f;

                // having a min and max angle could use for doing swing punches
                var attackMinAngle = -75f;
                var attackMaxAngle = 75f;
                var attackDistance = 2f;

                var characters = GameObject.FindObjectsOfType<Character>();
                foreach (var other in characters)
                {
                    if (other == owner ||
                        other.IsDead ||
                        1.8f < Mathf.Abs(owner.transform.position.y - other.transform.position.y)  ||
                        attackDistance < Vector2.Distance(owner.transform.position.xz(), other.transform.position.xz()) ||
                        hitCharacters.Contains(other))
                    {
                        continue;
                    }

                    // if angle between characters, damage the other
                    var currentAngle = Vector2.SignedAngle(
                        owner.transform.forward.xz(),
                        other.transform.position.xz() - owner.transform.position.xz());

                    if (currentAngle > attackMinAngle && currentAngle < attackMaxAngle)
                    {
                        hitCharacters.Add(other);
                        Debug.Log($"{owner.gameObject.name} melee hit {other.gameObject.name}");
                        other.ApplyDamage(1f);
                    }
                }
            }
        }
    }
}