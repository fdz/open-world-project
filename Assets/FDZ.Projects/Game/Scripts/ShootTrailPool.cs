﻿using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.Game
{
	public class ShootTrailPool : MonoBehaviour
	{
		private struct Helper
        {
			public LineRenderer lineRenderer;
			public float startTime;
			public float duration;
        }

		public static ShootTrailPool Instance { get; private set; }

		private GameObject prefab;
		private List<Helper> helpers = new List<Helper>();
		private List<LineRenderer> pool = new List<LineRenderer>();

		private void Awake()
        {
			if (Instance != null)
            {
				Destroy(this);
				return;
            }
			Instance = this;
			prefab = transform.GetChild(0).gameObject;
			prefab.SetActive(false);
		}

		private void Update()
        {
			var i = 0;
			while (i < helpers.Count)
			{
				var helper = helpers[i];
				var normalizedTime = (Time.time - helper.startTime) / helper.duration;
				var alpha = 1 - normalizedTime;
				if (alpha < 0)
				{
					helper.lineRenderer.gameObject.SetActive(false);
					helpers.RemoveAt(i);
					pool.Add(helper.lineRenderer);
				}
				else
				{
					alpha = Mathf.Max(alpha, 0f);
					helper.lineRenderer.startColor = new Color(
						helper.lineRenderer.startColor.r,
						helper.lineRenderer.startColor.g,
						helper.lineRenderer.startColor.b,
						alpha);
					helper.lineRenderer.endColor = new Color(
						helper.lineRenderer.startColor.r,
						helper.lineRenderer.startColor.g,
						helper.lineRenderer.startColor.b,
						alpha * .35f);
					helper.lineRenderer.SetPosition(1, Vector3.Lerp(helper.lineRenderer.GetPosition(1), helper.lineRenderer.GetPosition(0), normalizedTime));
					i++;
				}
			}
        }

		public void Spawn(Vector3 start, Vector3 end, float duration = .15f)
        {
			LineRenderer ln = pool.Count == 0 ? null : pool[0];
			if (ln == null)
            {
				ln = Instantiate(prefab, transform).GetComponent<LineRenderer>();
            }
			else
            {
				pool.RemoveAt(0);
            }
			ln.gameObject.SetActive(true);
			ln.transform.position = start;
			ln.SetPosition(0, end);
			ln.SetPosition(1, start);
			helpers.Add(new Helper() { lineRenderer = ln, startTime = Time.time, duration = duration });
        }
	}
}