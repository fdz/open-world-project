﻿using UnityEngine;

namespace FDZ.Projects.Game
{
    public abstract class CharacterAction
    {
        public bool IsActive { get; protected set; }

        public abstract void Update(Character owner);
    }
}