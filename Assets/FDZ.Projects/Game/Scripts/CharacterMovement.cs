﻿using UnityEngine;

namespace FDZ.Projects.Game
{
    public abstract class CharacterMovement : MonoBehaviour
    {
        public float rotationSpeed = 8f;

        public Character Owner { get; private set; }

        public virtual void Init(Character character)
        {
            Owner = character;
        }

        public virtual void Begin() { }

        public virtual void FixedTick(float deltaTime) { }

        public virtual void LateTick(float deltaTime) { }
    }
}