﻿using UnityEngine;

namespace FDZ.Projects.Game
{
    public enum CharacterGroundedState
    {
        Standing = 0,
        Crouching = 1,
    }
}