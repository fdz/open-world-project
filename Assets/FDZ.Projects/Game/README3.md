# Games I should play
- Grim Dawn
- Survivalist
- Dungeon Siege
- Neverwinter Nights: Enhanced Edition
- Arx Fatalis
- Baldurs Gate
- Icewind Dale II
- Prince of qin
## Strategy
- Age Of Empires 2
- Total war
## Tactics
- Shadow tactics
## Top down actions
- Battlerite
- Hotline miami and similars
## Life Simulators
- The Sims
## Colony sims
- RimWorld
- Judgment: Apocalypse Survival Simulation
- Hearthstone

# General objectives?
- Character
    - [x] Root motion
    - Rainbow Six Siege Style Movement
        - Ground movement states
            - [x] Standing
            - [x] Crouching
            - [ ] Proning
        - Speeds
            - [ ] Walking
            - [ ] Sprinting (shift)
            - [ ] Slow Walking (alt)
    - Combat
        - Bashing
        - Dashing
        - Rolling
        - Blocking
        - Leaning
        - Knockback
        - Stunning
        - Grabbing?
            - Hitting
            - Choking
            - Hostage
        - Combat Melee
            - Light attack
            - Heavy attack
            - Throw weapon <--- Same as throwing projectile
        - Combat Ranged
            - Raycasted: bullets
            - Projectiles: arrows, bombs, fireballs, etc
            - Aiming
    - Climbing
    - Vaulting
- Kidnapping
- Use the complex tetris inventory gui
- Vehicles
- Hittables
    - Breaking objects to gather resources
    - Hitting characters
    - Walls = stunned like darksouls
- AI
    - Sealth
    - Pikcup corpses
    - Drag corpes
    - clean blood to not alert enemies

# Line Of Sight
https://forum.unity.com/threads/free-gpu-line-of-sight-field-of-view.257324/
https://github.com/byronknoll/visibility-polygon-js
https://www.youtube.com/watch?v=rQG9aUWarwE
https://ncase.me/sight-and-light/
https://forum.unity.com/threads/free-gpu-line-of-sight-field-of-view.257324/

- https://forum.unity.com/threads/how-to-render-a-true-cone-of-sight.91630/
    - https://docs.unity3d.com/Manual/class-Projector.html

- https://answers.unity.com/questions/947748/true-cone-of-sight-commandos.html <-------
    - https://www.youtube.com/watch?v=EOW_sBsoYyY&feature=youtu.be
    - https://github.com/joscanper/unity_coneofsightfx
    - he says that is a system similar to decals

# Fog Of War
https://andrewhungblog.wordpress.com/2018/06/23/implementing-fog-of-war-in-unity/

# Projectors
- Blob shadows under player and rpg spells effects
    - https://www.youtube.com/watch?v=oZX5UAru3CY
    - https://www.youtube.com/watch?v=hQcZA3dYGxg

# Decals
https://www.youtube.com/watch?v=8dejKSbADqE