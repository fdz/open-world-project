﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.SoundClipManager
{
	[CreateAssetMenu(fileName = "SoundClipArray", menuName = "FDZ/ScriptableObjects/SoundClipArray", order = 1)]
	public class SoundClipArray : SoundClip
	{
		[SerializeField] SoundClipSingle[] clips = null;

		public SoundClipSingle GetRandomClip()
		{
			return clips[(int)Random.Range(0, clips.Length)];
		}

		public override void PlayAtPoint(Vector3 position)
		{
			GetRandomClip().PlayAtPoint(position);
		}
	} // end of class
} // end of namespace
