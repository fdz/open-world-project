﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.SoundClipManager
{
	public class SoundClipManager : MonoBehaviour
	{
		[System.Serializable]
		public class SoundClipItem
		{
			public string key;
			public SoundClip clip;
		}

		[SerializeField] SoundClipItem[] clips = null;

		public SoundClip Get(string key)
		{
			return System.Array.Find(clips, item => item.key == key)?.clip;
		}

		public void PlayAtSelf(string key)
		{
			PlayAtPoint(key, transform.position);
		}

		public void PlayAtPoint(string key, Vector3 position)
		{
			var clip = Get(key);
			if (clip == null)
			{
				Debug.LogWarning(gameObject.name + ".AudioClipManager could not play sound " + key + ", maybe you misspelled the key or you didn't assign a clip");
			}
			else
			{
				clip.PlayAtPoint(position);
			}
		}
	} // end of class
} // end of namespace
