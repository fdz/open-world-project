﻿using UnityEngine;

namespace FDZ.Projects.Testng
{
    /// <summary>
    /// https://gamedev.stackexchange.com/a/116427
    /// </summary>
    public class ChangeAcceleration : MonoBehaviour
    {
        public Transform targetTr;
        public float acceleration = 1f;
        public float deceleration = 1f;
        public float topSpeed = 3f;
        public float velocity;

        private void Update()
        {
            var dt = Time.deltaTime;
            var target = targetTr.position;

            Debug.DrawLine(transform.position, target, Color.green);

            var delta = target - transform.position;
            var distance = delta.magnitude;
            var decelDistance = velocity * velocity / (2 * deceleration);

            // i used this in the godot rts game project thingy for having the speed depend on the rotation but it not affecting the direction of the movement
            // the idea is to eliminate ugly lerps, so this shouldn't be used, but i think that maybe sets a god example of how have rotation in to account
            //var rads = Vector3.Angle(transform.forward, delta.normalized) * Mathf.Deg2Rad;
            //var rotMod = Mathf.Clamp01(1 - rads);
            //if (rads > float.Epsilon)
            //{
            //    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(delta.normalized), Time.deltaTime * 8f);
            //}
            //var topSpeed = this.topSpeed * rotMod;

            // then we almost reached it, skip the rest
            if (distance < velocity * dt)
            {
                velocity = 0;
                transform.position = target;
                return;
            }

            // This would work, if the dt is a constant, however it will vary from a frame to another, so we have to add a check before the code:
            // we are still far, continue accelerating (if possible)
            if (distance > decelDistance)
            {
                velocity = Mathf.Min(velocity + acceleration * dt, topSpeed);
            }
            // we are about to reach the target, let's start decelerating.
            else
            {
                velocity = Mathf.Max(velocity - deceleration * dt, 0);
            }

            // 2D
            // // the target is stationary, you can calculate the cosangle and sinangle beforehand,
            // // otherwise you will need to calculate them on every frame.. for reference:
            // var angle = Mathf.Atan2(deltaY, deltaX);
            // var cosangle = Mathf.Cos(angle);
            // var sinangle = Mathf.Sin(angle);
            // p.x += velocity * cosangle * dt;
            // p.y += velocity * sinangle * dt;
            // transform.position = p;

            // 3D
            transform.position += delta.normalized * velocity * dt;
        }
    }
}