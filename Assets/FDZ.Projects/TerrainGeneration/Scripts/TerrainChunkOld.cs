﻿using UnityEngine;
using FDZ.Attributes;

namespace FDZ.Projects.TerrainGeneration
{
    [ExecuteInEditMode()]
    public class TerrainChunkOld : MonoBehaviour
    {
        [System.Serializable]
        public struct TerrainType
        {
            //public string name;
            [Range(0, 1)] public float height;
            public Color color;
        }

        /// <summary>
        /// Why 240?
        /// 65025 (255 ^ 2) is the max amount of vertices supported by unity meshses
        /// 240 is the closest number to that that is divisible by the numbers >= 1 and <= 6
        /// those ranges of numbers allow
        /// </summary>
        public const int MAP_CHUNK_SIZE = 240;

        [Range(0, 6)] public int levelOfDetail;
        public float cellsize = 1f;

        public TerrainType[] regions;

        [Header("NOISE")]
        public bool useNoiseMap;
        public int noiseMapSeed = 0;
        public float noiseMapScale = 27.5f;
        public int noiseMapOctaves = 4;
        [Range(0, 1)] public float noiseMapPersistance = .5f;
        public float noiseMapLacunarity = 2;
        public Vector2 noiseMapOffset;
        public Material baseMaterial;

        [Header("NOISE HEIGHT")]
        public float heightMultiplier = 1;
        public AnimationCurve heightCurve;

        private Renderer _textureRender;
        private Renderer TextureRenderer
        {
            get
            {
                if (_textureRender == null)
                {
                    _textureRender = GetComponent<Renderer>();
                }
                return _textureRender;
            }
        }

        private void OnEnable()
        {
            GenerateGridMesh();
        }

        /// <summary>
        /// This function is called when the script is loaded or a value is changed in the inspector.
        /// </summary>
        private void OnValidate()
        {
            if (cellsize <= 0)
            {
                cellsize = .0001f;
            }
            // NOISE MAP
            if (noiseMapLacunarity < 1)
            {
                noiseMapLacunarity = 1;
            }
            if (noiseMapOctaves < 1)
            {
                noiseMapOctaves = 1;
            }
            if (noiseMapScale <= 0)
            {
                noiseMapScale = 0.051f;
            }
            GenerateGridMesh();
        }

        [ContextMenu("Generate grid mesh")]
        private void GenerateGridMesh()
        {
            Mesh mesh = new Mesh();
            mesh.name = "ProceduralGrid";

            //if (TextureRenderer.sharedMaterial == null)
            {
                TextureRenderer.sharedMaterial = new Material(baseMaterial);
                TextureRenderer.sharedMaterial.name += "(Copy)";
            }

            float[,] noiseMap = TerrainGenerationUtils.GenerateNoiseMap(
                MAP_CHUNK_SIZE + 1,
                MAP_CHUNK_SIZE + 1,
                noiseMapSeed,
                noiseMapScale,
                noiseMapOctaves,
                noiseMapPersistance,
                noiseMapLacunarity,
                noiseMapOffset,
                TerrainGenerationUtils.NormalizationMode.Global);
            //TextureRenderer.sharedMaterial.mainTexture = TextureFromHeightMap(noiseMap);

            Color[] colorMap = new Color[MAP_CHUNK_SIZE * MAP_CHUNK_SIZE];
            for (int y = 0; y < MAP_CHUNK_SIZE; y++)
            {
                for (int x = 0; x < MAP_CHUNK_SIZE; x++)
                {
                    float currentHeight = noiseMap[x, y];
                    for (int i = 0; i < regions.Length; i++)
                    {
                        if (currentHeight >= regions[i].height)
                        {
                            colorMap[y * MAP_CHUNK_SIZE + x] = regions[i].color;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            TextureRenderer.sharedMaterial.mainTexture = TerrainGenerationUtils.CreateTextureFromColorMap(colorMap, MAP_CHUNK_SIZE, MAP_CHUNK_SIZE);

            int step = levelOfDetail <= 0 ? 1 : levelOfDetail * 2;
            int verticesPerLine = (MAP_CHUNK_SIZE - 1) / step + 1;

            Vector3[] vertices = new Vector3[(verticesPerLine + 1) * (verticesPerLine + 1)];
            int[] triangles = new int[verticesPerLine * verticesPerLine * 6];
            Vector2[] uv = new Vector2[vertices.Length];
            //Vector4[] tangents = new Vector4[vertices.Length];
            //Vector4 tangent = new Vector4(1f, 0f, 0f, -1f); // we could set all the tangets to this if  we were to do a flat terrain

            //Debug.Log($"created vertices array, length = {vertices.Length}/{240 * 240}");

            for (int vi = 0, y = 0; y <= MAP_CHUNK_SIZE; y += step)
            {
                for (int x = 0; x <= MAP_CHUNK_SIZE; x += step, vi++)
                {
                    vertices[vi] = new Vector3(
                        x * cellsize - MAP_CHUNK_SIZE / 2f * cellsize,
                        !useNoiseMap ? 0f : noiseMap[x, y] * heightMultiplier * heightCurve.Evaluate(noiseMap[x, y]),
                        y * cellsize - MAP_CHUNK_SIZE / 2f * cellsize);

                    uv[vi] = new Vector2((float)x / MAP_CHUNK_SIZE, (float)y / MAP_CHUNK_SIZE);
                    //tangents[vi] = tangent;
                }
            }

            for (int ti = 0, vi = 0, y = 0; y < MAP_CHUNK_SIZE; y += step, vi++)
            {
                for (int x = 0; x < MAP_CHUNK_SIZE; x += step, vi++, ti += 6)
                {
                    triangles[ti] = vi;
                    triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                    triangles[ti + 4] = triangles[ti + 1] = vi + verticesPerLine + 1;
                    triangles[ti + 5] = vi + verticesPerLine + 2;
                }
            }

            mesh.vertices = vertices;
            mesh.uv = uv;
            //mesh.tangents = tangents;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();
            mesh.RecalculateBounds();

            GetComponent<MeshCollider>().sharedMesh = mesh;
            GetComponent<MeshFilter>().mesh = mesh;
        }
    }
}