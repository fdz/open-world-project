﻿using Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FDZ.Projects.TerrainGeneration
{
    public class EndlessTerrain : MonoBehaviour
    {
        //#if UNITY_EDITOR
        //        [UnityEditor.CustomEditor(typeof(EndlessTerrain))]
        //        public class EndlessTerrainEditor : UnityEditor.Editor
        //        {
        //            public override void OnInspectorGUI()
        //            {
        //                base.OnInspectorGUI();
        //            }
        //        }
        //#endif

        [System.Serializable]
        public struct HeightMapGenerationParameters
        {
            public int seed;
            public float scale;
            public int octaves;
            [Range(0, 1)] public float persistance;
            public float lacunarity;
        }

        public float viewDistance = 450;
        public Vector2 viewerPosition;
        public Material baseMaterial;
        [Range(0, 6)] public int lod = 6;

        [Space]
        public bool useFalloffMap = true;
        public int fallofMapSizeInChunks = 20;

        [Space]
        public float heightMultiplier = 1;
        public AnimationCurve heightCurve = new AnimationCurve()
        {
            keys = new Keyframe[]
            {
                new Keyframe(-1.5f, -1.5f),
                new Keyframe(-1f, -1f),
                new Keyframe(0f, 0f),
                new Keyframe(1f, 1f),
                new Keyframe(1.5f, 1.5f),
            }
        };
        public HeightMapGenerationParameters heightMapGenParams;

        [Space]
        public TerrainChunkColorRegion[] regions;

        private Dictionary<Vector2Int, TerrainChunk> terrainChunks = new Dictionary<Vector2Int, TerrainChunk>();
        private List<TerrainChunk> terrainChunksVisible = new List<TerrainChunk>();

#if UNITY_EDITOR
        private void OnValidate()
        {
            viewDistance = Mathf.Max(0f, viewDistance);
            // height map params
            heightMapGenParams.scale = Mathf.Max(.051f, heightMapGenParams.scale);
            heightMapGenParams.octaves = Mathf.Max(0, heightMapGenParams.octaves);
            heightMapGenParams.persistance = Mathf.Min(1, Mathf.Max(0, heightMapGenParams.persistance));
            heightMapGenParams.lacunarity = Mathf.Max(1, heightMapGenParams.lacunarity);
            heightMultiplier = Mathf.Max(1f, heightMultiplier);
        }
#endif

        [ContextMenu("Sort regions")]
        public void SortRegions()
        {
            bool sorted = true;
            while (sorted)
            {
                sorted = false;
                for (int i = 0; i < regions.Length - 1; i++)
                {
                    if (regions[i].height > regions[i + 1].height)
                    {
                        sorted = true;
                        var aux = regions[i + 1];
                        regions[i + 1] = regions[i];
                        regions[i] = aux;
                    }
                }
            }
        }

        private void Awake()
        {
            UpdateVisibleChunks();
        }

        private void Update()
        {
            bool update = false;
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                update = true;
                viewerPosition.x += TerrainChunk.SIZE;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                update = true;
                viewerPosition.x -= TerrainChunk.SIZE;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                update = true;
                viewerPosition.y += TerrainChunk.SIZE;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                update = true;
                viewerPosition.y -= TerrainChunk.SIZE;
            }
            if (update)
            {
                UpdateVisibleChunks();
            }
            Draw.SolidBox(new Vector3(viewerPosition.x, 200f, viewerPosition.y), Vector3.one * TerrainChunk.SIZE / 4, Color.red);
            Draw.WireBox(transform.position, Vector3.one.xoz() * TerrainChunk.SIZE * fallofMapSizeInChunks, Color.red);
        }

        private void UpdateVisibleChunks()
        {
            int currentChunkCoordX = Mathf.RoundToInt(viewerPosition.x / TerrainChunk.SIZE);
            int currentChunkCoordY = Mathf.RoundToInt(viewerPosition.y / TerrainChunk.SIZE);

            var chunksVisibleInViewDistance = Mathf.RoundToInt(viewDistance / TerrainChunk.SIZE);

            for (int yOffset = -chunksVisibleInViewDistance; yOffset <= chunksVisibleInViewDistance; yOffset++)
            {
                for (int xOffset = -chunksVisibleInViewDistance; xOffset <= chunksVisibleInViewDistance; xOffset++)
                {
                    TerrainChunk terrainChunk = null;
                    Vector2Int viewedChunkCoord = new Vector2Int(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);
                    if (terrainChunks.TryGetValue(viewedChunkCoord, out terrainChunk))
                    {
                        terrainChunksVisible.Add(terrainChunk);
                    }
                    else
                    {
                        float chunkX = viewedChunkCoord.x * TerrainChunk.SIZE;
                        float chunkY = viewedChunkCoord.y * TerrainChunk.SIZE;

                        terrainChunk = new TerrainChunk(viewedChunkCoord);
                        terrainChunks.Add(viewedChunkCoord, terrainChunk);

                        terrainChunk.CreateGameObject();
                        terrainChunk.transform.parent = transform;
                        terrainChunk.transform.localPosition = new Vector3(chunkX, 0f, chunkY);

                        terrainChunk.GenerateHeightMap(
                            heightMapGenParams.seed,
                            heightMapGenParams.scale,
                            heightMapGenParams.octaves,
                            heightMapGenParams.persistance,
                            heightMapGenParams.lacunarity,
                            heightCurve,
                            useFalloffMap,
                            fallofMapSizeInChunks);


                        terrainChunk.GenerateMeshData(heightMultiplier, lod);
                        terrainChunk.CreateMesh();
                        terrainChunk.CreateMainMaterial(baseMaterial, regions);
                    }
                }
            }

            foreach (var item in terrainChunks)
            {
                TerrainChunk curChunk = item.Value;
                curChunk.NormalsCalculateStart();
            }
            foreach (var item in terrainChunks)
            {
                TerrainChunk curChunk = item.Value;
                Vector2Int rightCoords = item.Key;
                rightCoords.x++;
                if (terrainChunks.TryGetValue(rightCoords, out TerrainChunk rightChunk))
                {
                    curChunk.NormalsCalculateRight(rightChunk);
                }
                Vector2Int topCoords = item.Key;
                topCoords.y++;
                if (terrainChunks.TryGetValue(topCoords, out TerrainChunk topChunk))
                {
                    curChunk.NormalsCalculateTop(topChunk);
                }
            }
            foreach (var item in terrainChunks)
            {
                TerrainChunk curChunk = item.Value;
                curChunk.NormalsCalculateEnd();
            }
        }
    } // end of namespace
} // end of class