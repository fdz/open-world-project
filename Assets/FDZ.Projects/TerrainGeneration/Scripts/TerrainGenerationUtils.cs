﻿using UnityEngine;

namespace FDZ.Projects.TerrainGeneration
{
    public static class TerrainGenerationUtils
    {
        public static float GetFalloffValue(int x, int y, int size)
        {
            float X = x / (float)size * 2 - 1;
            float Y = y / (float)size * 2 - 1;
            float value = Mathf.Max(Mathf.Abs(X), Mathf.Abs(Y));
            return FalloffEvaluate(value);
        }

        public static float[,] GenerateFalloffMap(int size)
        {
            float[,] map = new float[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    map[i, j] = GetFalloffValue(i, j, size);
                }
            }

            return map;
        }

        public static float FalloffEvaluate(float value)
        {
            float a = 3;
            float b = 2.2f;
            return Mathf.Pow(value, a) / (Mathf.Pow(value, a) + Mathf.Pow(b - b * value, a));
        }


        public enum NormalizationMode
        {
            Local,
            Global
        }

        public static float[,] GenerateNoiseMap(
            int mapWidth = 10, int mapHeight = 10,
            int seed = 0,
            float scale = 30f, int octaves = 4, float persistance = .5f, float lacunarity = 2,
            Vector2 offset = new Vector2(),
            NormalizationMode normalizeMode = NormalizationMode.Local)
        {
            float[,] noiseMap = new float[mapWidth, mapHeight];

            System.Random prng = new System.Random(seed);
            Vector2[] octaveOffsets = new Vector2[octaves];

            float maxPosibleHeight = 0;
            float amplitude = 1;

            for (int i = 0; i < octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + offset.x;
                float offsetY = prng.Next(-100000, 100000) + offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPosibleHeight += amplitude;
                amplitude *= persistance;
            }

            if (scale <= 0)
            {
                scale = 0.0001f;
            }

            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    amplitude = 1;
                    float frequency = 1;
                    float noiseHeight = 0;

                    for (int i = 0; i < octaves; i++)
                    {
                        float sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
                        float sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }

                    if (normalizeMode == NormalizationMode.Global)
                    {
                        //float normalizedHeight = (noiseMap[x, y] + 1) / (2f * maxPosibleHieght / 1.75f);
                        float normalizedHeight = (noiseHeight + 1) / maxPosibleHeight;
                        noiseMap[x, y] = normalizedHeight;
                    }
                    else
                    {
                        if (noiseHeight > maxLocalNoiseHeight)
                        {
                            maxLocalNoiseHeight = noiseHeight;
                        }
                        else if (noiseHeight < minLocalNoiseHeight)
                        {
                            minLocalNoiseHeight = noiseHeight;
                        }
                        noiseMap[x, y] = noiseHeight;
                    }
                }
            }

            if (normalizeMode == NormalizationMode.Local)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                    }
                }
            }

            return noiseMap;
        }

        public static Texture2D CreateTextureFromColorMap(Color[] colorMap, int width, int height)
        {
            Texture2D texture = new Texture2D(width, height);
            texture.SetPixels(colorMap);
            texture.filterMode = FilterMode.Point;
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();
            return texture;
        }

        public static Texture2D CreateTextureFromHeightMap(float[,] heightMap, float minValue = 0, float maxValue = 1)
        {
            int width = heightMap.GetLength(0);
            int height = heightMap.GetLength(1);
            Color[] colorMap = new Color[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float value = heightMap[x, y];
                    float t = Utils.Remap(value, minValue, maxValue, 0, 1);
                    colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, t);
                }
            }
            return CreateTextureFromColorMap(colorMap, width, height);
        }
    } // end of class
} // end of namespace