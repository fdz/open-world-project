﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace FDZ.Projects.TerrainGeneration
{
    public class MeshData
    {
        public Vector3[] vertices;
        public int[] triangles;
        public Vector2[] uv;
    }

    [System.Serializable]
    public struct TerrainChunkColorRegion
    {
        public float height;
        public Color color;
    }

    public class TerrainChunk
    {
        /// <summary>
        /// Why 240?
        /// 65025 (255 ^ 2) is the max amount of vertices supported by unity meshses
        /// 240 is the closest number to that that is divisible by the numbers >= 1 and <= 6
        /// those ranges of numbers allow
        /// </summary>
		public const int SIZE = 240;
        public const int SIZE_HALF = SIZE / 2;
        public const int SIZE_SQR = SIZE * SIZE;

        public float[,] heightMap;
        public Color[] colorMap;
        public MeshData meshData;
        public Mesh mesh;

        Vector3[] normals;

        public GameObject gameObject { get; private set; }
        public Transform transform { get; private set; }
        public MeshFilter meshFilter { get; private set; }
        public MeshRenderer meshRenderer { get; private set; }
        public MeshCollider meshCollider { get; private set; }

        public int verticesPerLine;
        public Vector2Int viewedChunkCoord;
        public TerrainChunk(Vector2Int viewedChunkCoord)
        {
            this.viewedChunkCoord = viewedChunkCoord;
        }

        public void GenerateHeightMap(int seed, float scale, int octaves, float persistance, float lacunarity, AnimationCurve heightCurve, bool useFalloffMap = false, int fallofMapSizeInChunks = 20)
        {
            heightMap = TerrainGenerationUtils.GenerateNoiseMap(
                SIZE + 1,
                SIZE + 1,
                seed,
                scale,
                octaves,
                persistance,
                lacunarity,
                viewedChunkCoord * SIZE,
                TerrainGenerationUtils.NormalizationMode.Global);

            int falloffSize = SIZE * fallofMapSizeInChunks;
            int halfFallofSize = falloffSize / 2;
            float chunkX = viewedChunkCoord.x * SIZE;
            float chunkY = viewedChunkCoord.y * SIZE;
            int yStart = (int)chunkY - SIZE_HALF + halfFallofSize;
            int xStart = (int)chunkX - SIZE_HALF + halfFallofSize;

            float[,] underWaterHeightMap = !useFalloffMap ? null : TerrainGenerationUtils.GenerateNoiseMap(
                SIZE + 1,
                SIZE + 1,
                seed * 2,
                scale,
                octaves,
                persistance,
                lacunarity,
                viewedChunkCoord * SIZE,
                TerrainGenerationUtils.NormalizationMode.Global);

            AnimationCurve hCurve = heightCurve;

            for (int y = 0; y <= SIZE; y++)
            {
                for (int x = 0; x <= SIZE; x++)
                {
                    if (useFalloffMap)
                    {
                        int xx = xStart + x;
                        int yy = yStart + y;
                        if (xx < 0 || xx > falloffSize || yy < 0 || yy > falloffSize)
                        {
                            // UNDERWATER TERRAIN IRREGULATIES
                            heightMap[x, y] = -underWaterHeightMap[x, y];
                        }
                        else
                        {
                            float falloff = TerrainGenerationUtils.GetFalloffValue(xx, yy, falloffSize);
                            // CIRCLE FALLOFF
                            xx -= halfFallofSize;
                            yy -= halfFallofSize;
                            falloff += TerrainGenerationUtils.FalloffEvaluate(Mathf.Sqrt(xx * xx + yy * yy) / (halfFallofSize * 1.4f));
                            // APPLY FALLOFF
                            heightMap[x, y] = Mathf.Max(0, heightMap[x, y] - falloff);
                            // UNDERWATER TERRAIN IRREGULATIES
                            heightMap[x, y] = Mathf.Lerp(heightMap[x, y], -underWaterHeightMap[x, y], falloff);
                        }
                    }
                    heightMap[x, y] = hCurve.Evaluate(heightMap[x, y]);
                }
            }
        }

        public void UpdateColorMap(TerrainChunkColorRegion[] regions)
        {
            int size = SIZE + 1;
            colorMap = new Color[size * size];
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    float currentHeight = heightMap[x, y];
                    for (int i = 0; i < regions.Length; i++)
                    {
                        if (currentHeight >= regions[i].height)
                        {
                            colorMap[y * size + x] = regions[i].color;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }

        /// <param name="levelOfDetail"> int between 0 and 6 </param>
        public void GenerateMeshData(float heightMultiplier, int levelOfDetail = 0, float scale = 1f)
        {
            int step = levelOfDetail <= 0 ? 1 : Mathf.Min(6, levelOfDetail) * 2;
            verticesPerLine = (SIZE - 1) / step + 1;

            var vertices = new Vector3[(verticesPerLine + 1) * (verticesPerLine + 1)];
            var triangles = new int[verticesPerLine * verticesPerLine * 6];
            var uv = new Vector2[vertices.Length];

            for (int ti = 0, tvi = 0, vi = 0, y = 0; y <= SIZE; y += step)
            {
                for (int x = 0; x <= SIZE; x += step, vi++)
                {
                    float h = heightMap[x, y];
                    vertices[vi] = new Vector3(
                        x * scale - SIZE / 2f * scale,
                        h * heightMultiplier,
                        y * scale - SIZE / 2f * scale);
                    uv[vi] = new Vector2((float)x / SIZE, (float)y / SIZE);
                    if (x < SIZE && y < SIZE)
                    {
                        triangles[ti] = tvi;
                        triangles[ti + 3] = triangles[ti + 2] = tvi + 1;
                        triangles[ti + 4] = triangles[ti + 1] = tvi + verticesPerLine + 1;
                        triangles[ti + 5] = tvi + verticesPerLine + 2;
                        tvi++;
                        ti += 6;
                    }
                }
                if (y < SIZE)
                {
                    tvi++;
                }
            }

            meshData = new MeshData();
            meshData.vertices = vertices;
            meshData.triangles = triangles;
            meshData.uv = uv;
        }

        public void CreateGameObject()
        {
            if (gameObject == null)
            {
                gameObject = new GameObject("TerrainChunk");
                transform = gameObject.transform;
                meshFilter = gameObject.AddComponent<MeshFilter>();
                meshRenderer = gameObject.AddComponent<MeshRenderer>();
                meshCollider = gameObject.AddComponent<MeshCollider>();
            }
        }

        public void CreateMainMaterial(Material baseMaterial, TerrainChunkColorRegion[] regions)
        {
            meshRenderer.sharedMaterial = new Material(baseMaterial);

            //meshRenderer.sharedMaterial.mainTexture = TerrainGenerationUtils.CreateTextureFromHeightMap(heightMap);

            UpdateColorMap(regions);
            meshRenderer.sharedMaterial.mainTexture = TerrainGenerationUtils.CreateTextureFromColorMap(colorMap, SIZE + 1, SIZE + 1);
        }

        public void CreateMesh()
        {
            mesh = new Mesh();
            mesh.name = "TerrainChunk";

            mesh.vertices = meshData.vertices;
            mesh.triangles = meshData.triangles;
            mesh.uv = meshData.uv;

            //mesh.RecalculateNormals();

            normals = CalculateNormals();
            mesh.normals = normals;

            //mesh.RecalculateTangents();
            mesh.RecalculateBounds();

            meshCollider.sharedMesh = mesh;
            meshFilter.mesh = mesh;
        }

        private Vector3[] CalculateNormals()
        {
            Vector3[] vertexNormals = CalculateNormalsUnnormalized();
            for (int i = 0; i < vertexNormals.Length; i++)
            {
                vertexNormals[i].Normalize();
            }
            return vertexNormals;
        }

        public Vector3[] CalculateNormalsUnnormalized()
        {
            Vector3[] vertexNormals = new Vector3[meshData.vertices.Length];
            int triangleCount = meshData.triangles.Length / 3;
            for (int i = 0; i < triangleCount; i++)
            {
                int normalTriangleIndex = i * 3;
                int vertexIndexA = meshData.triangles[normalTriangleIndex];
                int vertexIndexB = meshData.triangles[normalTriangleIndex + 1];
                int vertexIndexC = meshData.triangles[normalTriangleIndex + 2];
                Vector3 triangleNormal = SurfaceNormalFromIndices(vertexIndexA, vertexIndexB, vertexIndexC);
                vertexNormals[vertexIndexA] += triangleNormal;
                vertexNormals[vertexIndexB] += triangleNormal;
                vertexNormals[vertexIndexC] += triangleNormal;
            }
            return vertexNormals;
        }

        private Vector3 SurfaceNormalFromIndices(int indexA, int indexB, int indexC)
        {
            Vector3 pointA = meshData.vertices[indexA];
            Vector3 pointB = meshData.vertices[indexB];
            Vector3 pointC = meshData.vertices[indexC];
            Vector3 sideAB = pointB - pointA;
            Vector3 sideAC = pointC - pointA;
            return Vector3.Cross(sideAB, sideAC).normalized;
        }

        public void NormalsCalculateStart()
        {
            normals = CalculateNormalsUnnormalized();
        }

        public void NormalsCalculateRight(TerrainChunk rightChunk)
        {
            if (rightChunk.verticesPerLine != verticesPerLine)
            {
                return;
            }
            for (int y = 0; y <= verticesPerLine; y++)
            {
                int thisIndex = y * (verticesPerLine + 1) + verticesPerLine;
                int rightIndex = y * (verticesPerLine + 1);

                Vector3 rightNormal = rightChunk.normals[rightIndex];
                Vector3 normal = normals[thisIndex];

                rightChunk.normals[rightIndex] += normal;
                normals[thisIndex] += rightNormal;
            }
        }
        public void NormalsCalculateTop(TerrainChunk topChunk)
        {
            if (topChunk.verticesPerLine != verticesPerLine)
            {
                return;
            }
            for (int x = 0; x <= verticesPerLine; x++)
            {
                int thisIndex = verticesPerLine * (verticesPerLine + 1) + x;
                int topIndex = x;

                Vector3 topNormal = topChunk.normals[topIndex];
                Vector3 thisNormal = normals[thisIndex];

                topChunk.normals[topIndex] += thisNormal;
                normals[thisIndex] += topNormal;
            }
        }

        public void NormalsCalculateEnd()
        {
            for (int i = 0; i < normals.Length; i++)
            {
                normals[i].Normalize();
            }
            mesh.normals = normals;
        }
    } // end of class
} // end of namespace