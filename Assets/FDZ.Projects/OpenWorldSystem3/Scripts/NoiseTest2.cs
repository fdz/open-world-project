﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/*
https://www.reddit.com/r/proceduralgeneration/comments/2sk9be/opensimplex_noise_in_c_2d3d4d_in_under_400_lines/
    https://gist.github.com/digitalshadow/134a3a02b67cecd72181
    https://github.com/KdotJPG/OpenSimplex2/blob/master/csharp/OpenSimplex2S.cs
*/

namespace FDZ.Projects.OpenWorldSystem3
{
    public class NoiseTest2 : MonoBehaviour
    {
#if UNITY_EDITOR
        [CustomEditor(typeof(NoiseTest2)), CanEditMultipleObjects]
        public class NoiseTest2Editor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                if (GUILayout.Button("Generate"))
                {
                    foreach (var t in targets)
                    {
                        var target = t as NoiseTest2;
                        target.Generate();
                    }
                }
            }
        }
#endif

        [Space]
        public MeshRenderer positiveX;
        public MeshRenderer negativeX;
        public MeshRenderer positiveY;
        public MeshRenderer negativeY;
        public MeshRenderer positiveZ;
        public MeshRenderer negativeZ;

        [Space]
        public int size;
        public Vector3Int center = new Vector3Int(0, 0, 0);
        public bool spherize;

        [Space]
        public FastNoiseLite.NoiseType noiseType;
        public FastNoiseLite.RotationType3D rotationType3D;
        public int seed;
        [Range(0, 1)] public float frequency;
        [Range(0, 1)] public float frequencyScalar = 1;

        [Space]
        public FastNoiseLite.FractalType fractalType;
        public int fractalOctaves;
        public float fractalLacunarity;
        public float fractalGain;
        public float fractalWeightedStrength;
        public float fractalPingPongStrength;

        [Space]
        public float waterLevel = 0f;

        [Space]
        public bool autoGenerate;

        private FastNoiseLite noiseGen;
        private int halfSize;

        private void OnValidate()
        {
            if (autoGenerate)
            {
                Generate();;
            }
        }

        private void Generate()
        {
            if (noiseGen == null)
            {
                noiseGen = new FastNoiseLite();
            }

            // GENERAL
            noiseGen.SetRotationType3D(rotationType3D);
            noiseGen.SetNoiseType(noiseType);
            noiseGen.SetSeed(seed);
            noiseGen.SetFrequency(frequency * frequencyScalar);
            // FRACTAL
            noiseGen.SetFractalType(fractalType);
            noiseGen.SetFractalOctaves(fractalOctaves);
            noiseGen.SetFractalLacunarity(fractalLacunarity);
            noiseGen.SetFractalGain(fractalGain);
            noiseGen.SetFractalWeightedStrength(fractalWeightedStrength);
            noiseGen.SetFractalPingPongStrength(fractalPingPongStrength);

            // CREATE NOISE MAPS

            halfSize = size / 2;

            // https://gamedev.stackexchange.com/questions/97988/stepping-through-3d-noise-to-generate-a-cubemap-for-a-sphere

            // X
            var positiveXNoiseMap = new float[size, size];
            var negativeXNoiseMap = new float[size, size];

            // Y
            var positiveYNoiseMap = new float[size, size];
            var negativeYNoiseMap = new float[size, size];

            //Z
            var positiveZNoiseMap = new float[size, size];
            var negativeZNoiseMap = new float[size, size];

            for (int i = -halfSize; i < halfSize; ++i)
            {
                for (int j = -halfSize; j < halfSize; ++j)
                {
                    // X, i = z, j = x
                    positiveXNoiseMap[size - 1 - (j + halfSize), i + halfSize] = GenNoise(
                        j,
                        -halfSize,
                        i);

                    negativeXNoiseMap[i + halfSize, j + halfSize] = GenNoise(
                        i,
                        halfSize,
                        j);

                    // Y, i = y, j = x
                    positiveYNoiseMap[i + halfSize, size - 1 - (j + halfSize)] = GenNoise(
                        j,
                        i,
                        -halfSize);

                    negativeYNoiseMap[i + halfSize, j + halfSize] = GenNoise(
                        j,
                        i,
                        halfSize);

                    // Z, i = y, j = z
                    positiveZNoiseMap[i + halfSize, j + halfSize] = GenNoise(
                        -halfSize,
                        i,
                        j);

                    negativeZNoiseMap[j + halfSize, size - 1 - (i + halfSize)] = GenNoise(
                        halfSize,
                        j,
                        i);
                }
            }

            // CREATE TEXTURES

            // X
            positiveX.sharedMaterial.mainTexture = FDZ.Projects.OpenWorldSystem2.NoiseMapGeneration.CreateTextureFromHeightMap(positiveXNoiseMap);
            negativeX.sharedMaterial.mainTexture = FDZ.Projects.OpenWorldSystem2.NoiseMapGeneration.CreateTextureFromHeightMap(negativeXNoiseMap);

            // Y
            positiveY.sharedMaterial.mainTexture = FDZ.Projects.OpenWorldSystem2.NoiseMapGeneration.CreateTextureFromHeightMap(positiveYNoiseMap);
            negativeY.sharedMaterial.mainTexture = FDZ.Projects.OpenWorldSystem2.NoiseMapGeneration.CreateTextureFromHeightMap(negativeYNoiseMap);

            // Z
            positiveZ.sharedMaterial.mainTexture = FDZ.Projects.OpenWorldSystem2.NoiseMapGeneration.CreateTextureFromHeightMap(positiveZNoiseMap);
            negativeZ.sharedMaterial.mainTexture = FDZ.Projects.OpenWorldSystem2.NoiseMapGeneration.CreateTextureFromHeightMap(negativeZNoiseMap);
        }

        private float GenNoise(float x, float y, float z)
        {
            Vector3 point = new Vector3(x, y, z);
            Vector3 pointOnSphere = center + (spherize ? point.normalized * halfSize : point);
            float noise = noiseGen.GetNoise(pointOnSphere.x, pointOnSphere.y, pointOnSphere.z);

            //noise = (noise + 1f) * .5f;
            //if (noise <= .3f)
            //{
            //    return .0f;
            //}

            noise += waterLevel;

            return noise;
        }
    }
}