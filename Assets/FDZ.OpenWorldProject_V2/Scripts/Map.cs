using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.OpenWorldProject_V2
{
    public class Map
    {
        public readonly int chunksPerLine;
        public readonly int nodesPerLine;
        public readonly int terrainVertexPerLine;

        public readonly float nodeSpacing;
        public readonly float terrainVertexSpacing;
        public readonly int[] terrainVertexSteps;

        public readonly MapChunk[,] chunks;

        public readonly int levelGround;
        public readonly int levelsCountTotal;
        public readonly int levelsCountUnderground;
        public readonly int levelsCountOverground;

        private readonly float nodeSpacingMultByNodesPerLine;
        private readonly int chunksPerLineHalf;

        public Map(
            int _chunksPerLine,
            int _nodesPerLine,
            int _terrainVertexPerLine,
            float _nodeSpacing,
            float _terrainVertexSpacing,
            int[] _terrainVertexSteps,
            int _levelsCountUnderground,
            int _levelsCountOverground)
        {
            chunksPerLine = _chunksPerLine;
            nodesPerLine = _nodesPerLine;
            terrainVertexPerLine = _terrainVertexPerLine;
            terrainVertexSpacing = _terrainVertexSpacing;
            terrainVertexSteps = _terrainVertexSteps;
            nodeSpacing = _nodeSpacing;

            levelsCountUnderground = _levelsCountUnderground < 0 ? 0 : _levelsCountUnderground;
            levelsCountOverground = _levelsCountOverground < 0 ? 0 : _levelsCountOverground;
            levelsCountTotal = levelsCountUnderground + 1 + levelsCountOverground;
            levelGround = levelsCountUnderground;

            chunks = new MapChunk[chunksPerLine, chunksPerLine];
            for (int y = 0; y < chunksPerLine; y++)
            {
                for (int x = 0; x < chunksPerLine; x++)
                {
                    chunks[x, y] = new MapChunk(
                        this,
                        new Vector2Int(x, y),
                        nodesPerLine,
                        terrainVertexPerLine,
                        terrainVertexSpacing,
                        nodeSpacing,
                        terrainVertexSteps,
                        levelsCountUnderground,
                        levelsCountOverground);
                }
            }

            nodeSpacingMultByNodesPerLine = nodeSpacing * nodesPerLine;
            chunksPerLineHalf = chunksPerLine / 2;
        }

        #region chunks
        public Vector2Int TransformPositionToChunkCoords(Vector3 position)
        {
            return new Vector2Int(
                (int)(position.x / nodeSpacingMultByNodesPerLine + chunksPerLineHalf),
                (int)(position.z / nodeSpacingMultByNodesPerLine + chunksPerLineHalf));
        }

        public Vector2Int TransformNodeCoordsToChunkCoords(Vector3Int nodeCoords)
        {
            return new Vector2Int(
                nodeCoords.x /= nodesPerLine,
                nodeCoords.z /= nodesPerLine);
        }

        public bool AreChunkCoordsValid(Vector2Int coords)
        {
            return coords.x >= 0 && coords.x < chunksPerLine && coords.y >= 0 && coords.y < chunksPerLine;
        }

        public MapChunk GetChunk(Vector2Int coords)
        {
            return AreChunkCoordsValid(coords) ? chunks[coords.x, coords.y] : null;
        }

        public MapChunk GetChunkFromPosition(Vector3 position)
        {
            return GetChunk(TransformPositionToChunkCoords(position));
        }

        public MapChunk GetChunkFroomNodeCoords(Vector3Int nodeCoords)
        {
            return GetChunk(TransformNodeCoordsToChunkCoords(nodeCoords));
        }

        #endregion

        #region nodes

        public Vector3Int TransformPositionToNodeCoords(Vector3 position, int level)
        {
            return new Vector3Int(
                Mathf.RoundToInt(position.x / nodeSpacing + chunksPerLineHalf * nodesPerLine),
                level,
                Mathf.RoundToInt(position.z / nodeSpacing + chunksPerLineHalf * nodesPerLine));
        }

        public Node GetNodeFromCoords(Vector3Int nodeCoords)
        {
            var chunk = GetChunkFroomNodeCoords(nodeCoords);
            if (chunk == null)
            {
                return null;
            }
            nodeCoords = chunk.TransformNodeCoordsToLocal(nodeCoords);
            return chunk.grid.nodes[nodeCoords.x, nodeCoords.y, nodeCoords.z];
        }

        #endregion
    }

    public class MapChunk
    {
        public readonly Map map;
        public readonly Vector2Int coords;

        public readonly int nodesPerLine;
        public readonly int terrainVertexPerLine;

        public readonly float nodeSpacing;
        public readonly float terrainVertexSpacing;
        public readonly PlaneMeshData[] terrainsMeshData;
        public readonly float[,] terrainHeightMap;
        public readonly NodesGrid grid;

        public readonly int levelGround;
        public readonly int levelsCountTotal;
        public readonly int levelsCountUnderground;
        public readonly int levelsCountOverground;

        public readonly Vector3 localPosition;

        public Transform transform;

        public MapChunk(
            Map _map,
            Vector2Int _coords,
            int _nodesPerLine,
            int _terrainVertexPerLine,
            float _nodeSpacing,
            float _terrainVertexSpacing,
            int[] terrainVertexSteps,
            int _levelsCountUnderground,
            int _levelsCountOverground)
        {
            map = _map;
            coords = _coords;
            nodesPerLine = _nodesPerLine;
            terrainVertexPerLine = _terrainVertexPerLine;
            terrainVertexSpacing = _terrainVertexSpacing;
            nodeSpacing = _nodeSpacing;

            levelsCountUnderground = _levelsCountUnderground < 0 ? 0 : _levelsCountUnderground;
            levelsCountOverground = _levelsCountOverground < 0 ? 0 : _levelsCountOverground;
            levelsCountTotal = levelsCountUnderground + 1 + levelsCountOverground;
            levelGround = levelsCountUnderground;

            grid = new NodesGrid(map, this, new Vector3Int(nodesPerLine, levelsCountTotal, nodesPerLine), nodeSpacing, levelGround);
            terrainHeightMap = new float[terrainVertexPerLine, terrainVertexPerLine];
            terrainsMeshData = new PlaneMeshData[terrainVertexSteps.Length];
            for (int i = 0; i < terrainsMeshData.Length; i++)
            {
                terrainsMeshData[i] = new PlaneMeshData(
                    terrainVertexPerLine,
                    terrainVertexSpacing,
                    terrainVertexSteps[i]);
            }

            var a = nodeSpacing * nodesPerLine;
            var b = a * map.chunksPerLine / 2f;
            localPosition = new Vector3(
                coords.x * a - b,
                0,
                coords.y * a - b);
        }

        public Vector3Int TransformNodeCoordsToLocal(Vector3Int nodeCoords)
        {
            nodeCoords.x -= coords.x * nodesPerLine;
            nodeCoords.z -= coords.y * nodesPerLine;
            return nodeCoords;
        }

        public Vector3Int TransformNodeCoordsToGlobal(Vector3Int nodeCoords)
        {
            nodeCoords.x += coords.x * nodesPerLine;
            nodeCoords.z += coords.y * nodesPerLine;
            return nodeCoords;
        }
    }

    public enum NodePlacement
    {
        Any,
        Center,
        Corner,
        EdgeMiddle,
    }

    public class Node
    {
        public const int PER_CELL = 2;

        public readonly NodesGrid grid;
        public Vector3Int globalCoords;
        public readonly Vector3Int localCoords;
        public readonly HashSet<Node> neighbors;

        public Vector3 localPosition;

        public bool IsCorner { get { Debug.LogError("implement"); return false; } }

        public bool IsCenter => localCoords.x % PER_CELL == 0 || localCoords.z % PER_CELL == 0;

        public bool IsEdgeMiddle => IsRow || IsColumn;

        public bool IsRow => localCoords.z % PER_CELL == 0 && localCoords.x % PER_CELL != 0;

        public bool IsColumn => localCoords.x % PER_CELL == 0 && localCoords.z % PER_CELL != 0;


        public Node(
            NodesGrid _grid,
            Vector3Int _localCoords,
            Vector3 _localPosition)
        {
            grid = _grid;
            localCoords = _localCoords;
            localPosition = _localPosition;
            neighbors = new HashSet<Node>();
            if (grid.mapChunk != null)
            {
                globalCoords = grid.mapChunk.TransformNodeCoordsToGlobal(localCoords);
            }
        }
    }

    public class NodesGrid
    {
        public readonly Map map;
        public readonly MapChunk mapChunk;
        public readonly Vector3Int size;
        public readonly int mainY;
        public Node[,,] nodes;
        public float spacing;

        public Transform transform;

        public NodesGrid(
            Map _map,
            MapChunk _mapChunk,
            Vector3Int _size,
            float _spacing,
            int _mainLevel)
        {
            map = _map;
            mapChunk = _mapChunk;
            size = _size;
            mainY = _mainLevel;
            spacing = _spacing;
            nodes = new Node[size.x, size.y, size.z];
            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    for (int x = 0; x < size.x; x++)
                    {
                        var node = nodes[x, y, z] = new Node(
                            this,
                            new Vector3Int(x, y, z),
                            //new Vector3(x * spacing, (y - mainY) * spacing, z * spacing));
                            new Vector3(x * spacing, 0f, z * spacing));
                        AddNeighbor(node, x, y, z, -1, 0, 0);
                        AddNeighbor(node, x, y, z, 0, 0, -1);
                        AddNeighbor(node, x, y, z, -1, 0, -1);
                        AddNeighbor(node, x, y, z, 1, 0, -1);
                    }
                }
            }
            void AddNeighbor(Node node, int x, int y, int z, int xDir, int yDir, int zDir)
            {
                int xx = x + xDir;
                int yy = y + yDir;
                int zz = z + zDir;
                if (xx > 0 && xx < size.x &&
                    yy > 0 && yy < size.y &&
                    zz > 0 && zz < size.z)
                {
                    var newNeighbor = nodes[xx, yy, zz];
                    node.neighbors.Add(newNeighbor);
                    newNeighbor.neighbors.Add(node);
                }
            }
        }

        public Node GetNodeFromWorldPosition(Vector3 worldPosition, NodePlacement placement = NodePlacement.Any)
        {
            return GetNodeFromLocalPosition(transform.InverseTransformPoint(worldPosition), placement);
        }

        public Node GetNodeFromLocalPosition(Vector3 localPosition, NodePlacement placement = NodePlacement.Any)
        {
            int x, z;
            switch (placement)
            {
                default:
                case NodePlacement.Any:
                case NodePlacement.Corner:
                {
                    var xf = localPosition.x / spacing;
                    var zf = localPosition.z / spacing;
                    x = Mathf.RoundToInt(xf);
                    z = Mathf.RoundToInt(zf);
                    if (placement == NodePlacement.Corner)
                    {
                        x *= Node.PER_CELL;
                        z *= Node.PER_CELL;
                    }
                }
                break;
                case NodePlacement.Center:
                case NodePlacement.EdgeMiddle:
                {
                    var xf = localPosition.x / (spacing * Node.PER_CELL);
                    var zf = localPosition.z / (spacing * Node.PER_CELL);
                    var xdf = xf * Node.PER_CELL;
                    var zdf = zf * Node.PER_CELL;
                    var xdi = Mathf.RoundToInt(xf) * Node.PER_CELL;
                    var zdi = Mathf.RoundToInt(zf) * Node.PER_CELL;
                    var xd = System.Math.Sign(xdf - xdi);
                    xd = xd % Node.PER_CELL == 0 ? 1 : xd;
                    xd *= (Node.PER_CELL / 2);
                    var zd = System.Math.Sign(zdf - zdi);
                    zd = zd % Node.PER_CELL == 0 ? 1 : zd;
                    zd *= (Node.PER_CELL / 2);
                    if (placement == NodePlacement.Center)
                    {
                        x = xdi + xd;
                        z = zdi + zd;
                    }
                    else if (Mathf.Abs(xdf - xdi) > Mathf.Abs(zdf - zdi))
                    {
                        x = xdi + xd;
                        z = zdi;
                    }
                    else
                    {
                        x = xdi;
                        z = zdi + zd;
                    }
                }
                break;
            }
            if (x < 0 || x >= size.x || z < 0 || z >= size.z)
            {
                return null;
            }
            // prioritize getting node on the main level
            // useful if for now, because all nodes start with the same localPosition.y
            var mn = nodes[x, mainY, z];
            var dif = Mathf.Abs(mn.localPosition.y - localPosition.y);
            var y = mainY;
            for (int cy = 0; cy < size.y; cy++)
            {
                var node = nodes[x, cy, z];
                var aux = Mathf.Abs(node.localPosition.y - localPosition.y);
                if (aux < dif)
                {
                    dif = aux;
                    y = cy;
                }
            }
            return nodes[x, y, z];
        }
    }

    public class PlaneMeshData
    {
        public readonly float vertexSpacing;
        public readonly int vertexPerLine;
        public readonly int vertexStep;
        public readonly Vector3[] vertices;
        public readonly Vector3[] normals;
        public readonly int[] triangles;

        public PlaneMeshData(
            int _vertexPerLine,
            float _vertexSpacing,
            int _vertexStep,
            bool initNormalsArray = true)
        {
            vertexSpacing = _vertexSpacing;
            vertexPerLine = _vertexPerLine;
            vertexStep = _vertexStep;

            vertices = new Vector3[vertexPerLine * vertexPerLine];
            triangles = new int[vertices.Length * 6];
            normals = initNormalsArray ? new Vector3[vertices.Length] : null;
        }

        public void GenerateVerticesAndTriangles()
        {
            int vi = 0;
            int ti = 0;
            for (int y = 0; y < vertexPerLine; y++)
            {
                for (int x = 0; x < vertexPerLine; x++)
                {
                    int xx = x * vertexStep;
                    int yy = y * vertexStep;
                    vertices[vi] = new Vector3(xx * vertexSpacing, 0, yy * vertexSpacing);
                    if (y < vertexPerLine - 1 && x < vertexPerLine - 1)
                    {
                        triangles[ti] = vi;
                        triangles[ti + 1] = triangles[ti + 4] = vi + vertexPerLine;
                        triangles[ti + 2] = triangles[ti + 3] = vi + 1;
                        triangles[ti + 5] = vi + vertexPerLine + 1;
                        ti += 6;
                    }
                    vi++;
                }
            }
        }

        public float GetVertexY(int x, int z)
        {
            var xx = x / (float)vertexStep;
            var zz = z / (float)vertexStep;
            // has numbers after the comma/dot ?
            if (xx % 1 == 0 && zz % 1 == 0)
            {
                return vertices[(int)zz * vertexPerLine + (int)xx].y;
            }
            return float.MinValue;
        }

        public void SetVertexY(int x, int z, float value)
        {
            var xx = x / (float)vertexStep;
            var zz = z / (float)vertexStep;
            // has numbers after the comma/dot ?
            if (xx % 1 == 0 && zz % 1 == 0)
            {
                vertices[(int)zz * vertexPerLine + (int)xx].y = value;
            }
        }
    }
}