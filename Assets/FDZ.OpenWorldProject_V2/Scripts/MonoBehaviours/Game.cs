﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.OpenWorldProject_V2
{
    public class Game : MonoBehaviour
    {
        public TMPro.TextMeshProUGUI textFieldInfoUnderCursor;
        public Camera cam;
        public Material terrainMaterial;
        public float heightMultiplier = 50;
        public FastNoiseLiteParameters noiseGenerationParameters;
        public NodeObject obj;

        public static NodesGrid gridUnderMouse;

        Map map;

        private void Awake()
        {
            var camTr = cam.transform;
            var p = camTr.position;
            var e = camTr.eulerAngles;
            p.x = PlayerPrefs.GetFloat("cam.position.x", p.x);
            p.y = PlayerPrefs.GetFloat("cam.position.y", p.y);
            p.z = PlayerPrefs.GetFloat("cam.position.z", p.z);
            e.x = PlayerPrefs.GetFloat("cam.eulerAngles.x", e.x);
            e.y = PlayerPrefs.GetFloat("cam.eulerAngles.y", e.y);
            e.z = PlayerPrefs.GetFloat("cam.eulerAngles.z", e.z);
            camTr.position = p;
            camTr.eulerAngles = e;

            int chunksPerLine = 4;
            int nodesPerLine = 8;
            int vertexPerLine = nodesPerLine + 1;
            float nodesAndVertexSpacing = .91f / 2f;
            int[] terrainVertexSteps = new int[] { 1 };
            int levelsCountUnderground = 2;
            int levelsCountOverground = 2;
            map = new Map(
                chunksPerLine,
                nodesPerLine,
                vertexPerLine,
                nodesAndVertexSpacing,
                nodesAndVertexSpacing,
                terrainVertexSteps,
                levelsCountUnderground,
                levelsCountOverground);

            var noiseGen = new FastNoiseLite();
            noiseGen.SetAllParameters(noiseGenerationParameters);

            for (int cy = 0; cy < map.chunksPerLine; cy++)
            {
                for (int cx = 0; cx < map.chunksPerLine; cx++)
                {
                    var chunk = map.chunks[cx, cy];

                    // CHUNK GAME OBJECT
                    var goChunk = new GameObject();
                    goChunk.name = $"Chunk {chunk.coords}";
                    goChunk.transform.parent = transform;
                    goChunk.transform.localPosition = chunk.localPosition;
                    goChunk.transform.localRotation = Quaternion.identity;
                    var gridRef = goChunk.AddComponent<NodesGridMono>();
                    gridRef.grid = chunk.grid;

                    // ASIGN CHUNK TRANSFORM
                    chunk.transform = goChunk.transform;
                    chunk.grid.transform = chunk.transform;

                    // GENERATE MESH DATA
                    var terrainMeshData = chunk.terrainsMeshData[0];
                    terrainMeshData.GenerateVerticesAndTriangles();

                    // GENERATE NOISE AND SAVE IT TO THE HEIGHT MAP
                    for (int vy = 0; vy < chunk.terrainVertexPerLine; vy++)
                    {
                        for (int vx = 0; vx < chunk.terrainVertexPerLine; vx++)
                        {
                            var noise = noiseGen.GetNoise(
                                chunk.coords.x * (chunk.terrainVertexPerLine - 1) + vx,
                                chunk.coords.y * (chunk.terrainVertexPerLine - 1) + vy) * heightMultiplier;
                            noise = (int)(noise / (map.nodeSpacing * .5f)) * (map.nodeSpacing * .5f);

                            chunk.terrainHeightMap[vx, vy] = noise;

                            if (vx < chunk.nodesPerLine && vy < chunk.nodesPerLine)
                            {
                                for (int i = 0; i < map.levelsCountTotal; i++)
                                {
                                    chunk.grid.nodes[vx, i, vy].localPosition.y = chunk.terrainHeightMap[vx, vy];
                                }
                            }

                            terrainMeshData.SetVertexY(vx, vy, chunk.terrainHeightMap[vx, vy]);
                        }
                    }

                    // CREATE TERRAIN MESH, MESH RENDERER, ETC
                    var terrainMesh = new Mesh();
                    terrainMesh.Clear();
                    terrainMesh.vertices = terrainMeshData.vertices;
                    terrainMesh.triangles = terrainMeshData.triangles;
                    terrainMesh.RecalculateNormals();
                    
                    var goTerrain = new GameObject("Terrain Mesh");
                    goTerrain.transform.parent = goChunk.transform;
                    goTerrain.transform.localPosition = Vector3.zero;
                    goTerrain.transform.localRotation = Quaternion.identity;

                    goTerrain.AddComponent<MeshFilter>().sharedMesh = terrainMesh;
                    goTerrain.AddComponent<MeshRenderer>().sharedMaterial = terrainMaterial;
                    goTerrain.AddComponent<MeshCollider>().sharedMesh = terrainMesh;
                }
            }

            var grids = GameObject.FindObjectsOfType<NodesGridMono>();
            foreach (var gridMono in grids)
            {
                if (gridMono.createGrid)
                {
                    var spacing = map.nodeSpacing;
                    gridMono.grid = new NodesGrid(map, null, gridMono.size, map.nodeSpacing, map.levelGround);
                    gridMono.grid.transform = gridMono.transform;
                    var quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                    quad.transform.parent = gridMono.transform;
                    var quadSize = new Vector3Int(gridMono.size.x - 1, 0, gridMono.size.z - 1);
                    quad.transform.localPosition = new Vector3(quadSize.x * spacing * .5f, 0f, quadSize.z * spacing * .5f);
                    quad.transform.localRotation = Quaternion.Euler(90, 0f, 0f);
                    quad.transform.localScale = new Vector3(quadSize.x * spacing, quadSize.z * spacing, 1f);
                }
            }
        }

        private void FixedUpdate()
        {
            #region camera movement
            Cursor.lockState = CursorLockMode.None;
            if (Input.GetMouseButton(1))
            {
                Cursor.lockState = CursorLockMode.Locked;
                var camTr = cam.transform;
                camTr.position += (
                        camTr.forward * UtilsInput.GetAxis(KeyCode.S, KeyCode.W) +
                        camTr.right * UtilsInput.GetAxis(KeyCode.A, KeyCode.D) +
                        camTr.up * UtilsInput.GetAxis(KeyCode.Q, KeyCode.E)
                    ).normalized * Time.deltaTime * 16f;
                camTr.eulerAngles += new Vector3(
                    -Input.GetAxisRaw("Mouse Y"),
                    Input.GetAxisRaw("Mouse X"));
                var p = camTr.position;
                var e = camTr.eulerAngles;
                PlayerPrefs.SetFloat("cam.position.x", p.x);
                PlayerPrefs.SetFloat("cam.position.y", p.y);
                PlayerPrefs.SetFloat("cam.position.z", p.z);
                PlayerPrefs.SetFloat("cam.eulerAngles.x", e.x);
                PlayerPrefs.SetFloat("cam.eulerAngles.y", e.y);
                PlayerPrefs.SetFloat("cam.eulerAngles.z", e.z);
            }
            #endregion
            var mousePos = Input.mousePosition;
            var ray = cam.ScreenPointToRay(mousePos);
            var rayMaxDistance = cam.farClipPlane;
            Debug.DrawRay(ray.origin, ray.direction * rayMaxDistance, Color.red);
            if (Physics.Raycast(ray, out RaycastHit hitInfo, cam.farClipPlane))
            {
                Debug.DrawRay(hitInfo.point, hitInfo.normal, Color.red);
                var hitPoint = hitInfo.point;
                var coll = hitInfo.collider;
                var grid = coll.GetComponent<NodesGridMono>()?.grid;
                if (grid == null)
                {
                    grid = coll.GetComponentInParent<NodesGridMono>()?.grid;
                }
                gridUnderMouse = grid;
                //var nodeCoords = map.TransformPositionToNodeCoords(hitPoint, map.levelGround);
                //var node = map.GetNodeFromCoords(nodeCoords);
                //var chunk = map.GetChunkFromPosition(hitPoint);
                //textFieldInfoUnderCursor.text = $"point: {hitPoint}";
                //textFieldInfoUnderCursor.text += $"\nchunk: {chunk.coords}";
                //textFieldInfoUnderCursor.text += $"\nnode (l): {node.localCoords}";
                //textFieldInfoUnderCursor.text += $"\nnode (g): {node.globalCoords}";
                var node = grid.GetNodeFromWorldPosition(hitPoint, obj.placement);
                if (node != null)
                {
                    Debug.DrawRay(node.grid.transform.TransformPoint(node.localPosition), Vector3.up, Color.black);
                    if (Input.GetMouseButtonDown(0))
                    {
                        var ins = Instantiate(obj);
                        ins.gameObject.SetActive(true);
                        if (ins.Place(node, NodeObject.Rotation.R0) == false)
                        {
                            Debug.LogError("could not be placed");
                            Destroy(ins.gameObject);
                        }
                    }
                }
            }
        }
    }
}