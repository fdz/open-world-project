﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FDZ.OpenWorldProject_V2
{
    public class NodeObject : MonoBehaviour
    {
        public enum Rotation
        {
            R0 = 0,
            R90 = 90,
            R180 = 180,
            R270 = -90,
        }

        public NodePlacement placement;
        public Vector3Int size;
        public Vector3Int offset;
        public float height;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public bool Place(Node mainNode, Rotation rotation)
        {
            switch (placement)
            {
                case NodePlacement.Any:
                break;

                case NodePlacement.Center:
                {
                    rotation = 0;
                    if (mainNode.IsCenter)
                    {
                        return false;
                    }
                }
                break;

                case NodePlacement.Corner:
                {
                    if (mainNode.IsCorner == false)
                    {
                        return false;
                    }
                }
                break;

                case NodePlacement.EdgeMiddle:
                {
                    if (mainNode.IsColumn)
                    {
                        rotation = 0;
                    }
                    else if (mainNode.IsRow)
                    {
                        rotation = Rotation.R90;
                    }
                    else
                    {
                        return false;
                    }
                }
                break;
            }

            gameObject.SetActive(true);
            transform.parent = mainNode.grid.transform;
            transform.localPosition = mainNode.localPosition;
            transform.localRotation = Quaternion.Euler(0f, (int)rotation, 0);

            ForEachNode(mainNode, rotation, (node) =>
            {
                if (node.localCoords.y > mainNode.localCoords.y)
                {
                    node.localPosition.y = mainNode.localPosition.y + height;
                }
            });

            return true;
        }

        public void ForEachNode(Node mainNode, Rotation rotation, Action<Node> onNode)
        {
            RotateSizeAndOffset(rotation, out Vector3Int size, out Vector3Int offset, out Vector3Int count, out Vector3Int dir);
            ForEachNode(mainNode, offset, count, dir, onNode);
        }

        public void ForEachNode(Node mainNode, Vector3Int offset, Vector3Int count, Vector3Int dir, Action<Node> onNode)
        {
            for (int y = 0; y < count.y; y++)
            {
                for (int z = 0; z < count.z; z++)
                {
                    for (int x = 0; x < count.x; x++)
                    {
                        var worldChunk = mainNode.grid.mapChunk;
                        var world = worldChunk?.map;
                        var nodes = mainNode.grid.nodes;
                        var curOffset = new Vector3Int(dir.x * x - offset.x, y - offset.y, dir.z * z - offset.z);
                        var curNode = worldChunk != null
                            ? world.GetNodeFromCoords(mainNode.globalCoords + curOffset)
                            : nodes[mainNode.localCoords.x + curOffset.x, mainNode.localCoords.y + curOffset.y, mainNode.localCoords.z + curOffset.z];
                        onNode.Invoke(curNode);
                    }
                }
            }
        }

        public void RotateSizeAndOffset(Rotation rotation, out Vector3Int size, out Vector3Int offset, out Vector3Int count, out Vector3Int dir)
        {
            size = this.size;
            offset = this.offset;
            switch (rotation)
            {
                case Rotation.R0:
                break;

                case Rotation.R90:
                size = new Vector3Int(size.z, size.y, size.x);
                offset = new Vector3Int(offset.z, offset.y, offset.x);
                break;

                case Rotation.R180:
                size = new Vector3Int(size.x, size.y, -size.z);
                offset = new Vector3Int(offset.x, offset.y, -offset.z);
                break;

                case Rotation.R270:
                size = new Vector3Int(-size.z, size.y, size.x);
                offset = new Vector3Int(-offset.z, offset.y, offset.x);
                break;
            }
            count = new Vector3Int(Mathf.Abs(size.x), Mathf.Abs(size.y), Mathf.Abs(size.z));
            dir = new Vector3Int(Math.Sign(size.x), Math.Sign(size.y), Math.Sign(size.z));
        }
    }
}