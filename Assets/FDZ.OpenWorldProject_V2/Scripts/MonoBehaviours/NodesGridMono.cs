﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.OpenWorldProject_V2
{
	public class NodesGridMono : MonoBehaviour
	{
		public NodesGrid grid;

		public bool createGrid;
		public Vector3Int size;

        private void OnDrawGizmos()
        {
            if (grid == null || grid != Game.gridUnderMouse)
            {
                return;
            }
            foreach (var node in grid.nodes)
            {
                //if (node.localCoords.y != grid.mainY)
                //    continue;
                ////if (node.coords.x != 0 && node.coords.z != 0)
                //    continue;
                Gizmos.color = node.localCoords.x == 0 || node.localCoords.z == 0 ? Color.red : Color.yellow;
                Gizmos.DrawSphere(grid.transform.TransformPoint(node.localPosition), .05f);
            }
        }
    }
}