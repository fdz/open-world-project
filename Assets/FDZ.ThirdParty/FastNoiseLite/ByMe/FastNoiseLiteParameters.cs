﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct FastNoiseLiteParameters
{
    public int seed;

    [Space]
    public FastNoiseLite.NoiseType noiseType;
    public FastNoiseLite.RotationType3D rotationType3D;

    [Space]
    [Range(0, 1)] public float frequency;
    public float frequencyHelper;

    [Space]
    public FastNoiseLite.FractalType fractalType;
    public int fractalOctaves;
    public float fractalLacunarity;
    public float fractalGain;
    public float fractalWeightedStrength;
    public float fractalPingPongStrength;
}

public static class FastNoiseLiteExtensions
{
    public static void SetAllParameters(this FastNoiseLite self, FastNoiseLiteParameters noiseParameters)
    {
        // seeed
        self.SetSeed(noiseParameters.seed);
        // types
        self.SetNoiseType(noiseParameters.noiseType);
        self.SetRotationType3D(noiseParameters.rotationType3D);
        // frequency
        self.SetFrequency(noiseParameters.frequency / noiseParameters.frequencyHelper);
        // fractal
        self.SetFractalType(noiseParameters.fractalType);
        self.SetFractalOctaves(noiseParameters.fractalOctaves);
        self.SetFractalLacunarity(noiseParameters.fractalLacunarity);
        self.SetFractalGain(noiseParameters.fractalGain);
        self.SetFractalWeightedStrength(noiseParameters.fractalWeightedStrength);
        self.SetFractalPingPongStrength(noiseParameters.fractalPingPongStrength);
    }
}