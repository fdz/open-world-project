﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace FDZ.Projects.OpenWorldProject
{
    public class FlyMovement : MonoBehaviour
    {
#if UNITY_EDITOR
        static FlyMovement()
        {
            EditorApplication.playModeStateChanged -= PlayModeStateChanged;
            EditorApplication.playModeStateChanged += PlayModeStateChanged;
        }

        private static void PlayModeStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.EnteredEditMode)
            {
                FindObjectOfType<FlyMovement>().InitTransform();
            }
        }
#endif

        private Vector3 position;
        private Vector3 eulerAngles;

        private void Awake()
        {
            InitTransform();
        }

        private void InitTransform()
        {
            var p = transform.position;
            position.x = PlayerPrefs.GetFloat("cam.position.x", p.x);
            position.y = PlayerPrefs.GetFloat("cam.position.y", p.y);
            position.z = PlayerPrefs.GetFloat("cam.position.z", p.z);
            transform.position = position;

            var e = transform.eulerAngles;
            eulerAngles.x = PlayerPrefs.GetFloat("cam.eulerAngles.x", e.x);
            eulerAngles.y = PlayerPrefs.GetFloat("cam.eulerAngles.y", e.y);
            eulerAngles.z = PlayerPrefs.GetFloat("cam.eulerAngles.z", e.z);
            transform.eulerAngles = eulerAngles;
        }

        private void OnDestroy()
        {
            var p = transform.position;
            PlayerPrefs.SetFloat("cam.position.x", p.x);
            PlayerPrefs.SetFloat("cam.position.y", p.y);
            PlayerPrefs.SetFloat("cam.position.z", p.z);

            var e = transform.eulerAngles;
            PlayerPrefs.SetFloat("cam.eulerAngles.x", e.x);
            PlayerPrefs.SetFloat("cam.eulerAngles.y", e.y);
            PlayerPrefs.SetFloat("cam.eulerAngles.z", e.z);
        }

        public void Tick(float delta)
        {
            transform.position +=
                (
                    (transform.forward * UtilsInput.GetAxis(KeyCode.S, KeyCode.W)).normalized +
                    (transform.right * UtilsInput.GetAxis(KeyCode.A, KeyCode.D)).normalized +
                    (transform.up * UtilsInput.GetAxis(KeyCode.Q, KeyCode.E)).normalized
                ).normalized * Time.deltaTime * 16f;
            if (Input.GetMouseButton(1))
            {
                Cursor.lockState = CursorLockMode.Locked;
                transform.eulerAngles += new Vector3(
                    -Input.GetAxisRaw("Mouse Y"),
                    Input.GetAxisRaw("Mouse X"));
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}