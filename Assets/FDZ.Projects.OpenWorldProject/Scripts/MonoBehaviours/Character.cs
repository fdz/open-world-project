﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
	public class Character : MonoBehaviour
	{
		public CharacterController cc;
		
		[Space]
		public Vector3 inputMoveDir;
		public Vector3 inputLookDir;

		[Space]
		public float moveSpeed;

		public void Tick(float delta)
        {
			var moveDir = inputMoveDir.normalized;
			var lookDir = inputLookDir.xoz().normalized;
			cc.SimpleMove(moveDir * moveSpeed);
			transform.rotation = Quaternion.LookRotation(lookDir);
        }
	}
}