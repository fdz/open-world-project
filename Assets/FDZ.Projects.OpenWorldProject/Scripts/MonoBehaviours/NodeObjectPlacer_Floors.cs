﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;

namespace FDZ.Projects.OpenWorldProject
{
	public class NodeObjectPlacer_Floors : MonoBehaviour
    {
        public NodeObject prefab;

        private Node starNode;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void Tick()
        {
            var mousePos = Input.mousePosition;
            var cam = FindObjectOfType<Camera>();
            var ray = cam.ScreenPointToRay(mousePos);
            var rayMaxDistance = cam.farClipPlane;

            Draw.ingame.Ray(ray.origin, ray.direction * rayMaxDistance, Color.red);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, cam.farClipPlane))
            {
                Draw.ingame.WireSphere(hitInfo.point, .1f, Color.red);
                
                var world = GameManager.ins.worldMngr.world;
                var worldPos = hitInfo.point;
                var coords = world.CalculateCenterNodeGlobalCoordsFromWorldPositionRounding(worldPos, Player.currentGridLevel);
                var node = world.GetNodeFromNodeGlobalCoords(coords);

                if (node == null)
                {
                    return;
                }

                Draw.ingame.WireSphere(node.Position, .1f, Color.green);

                if (Input.GetMouseButtonDown(0))
                {
                    starNode = node;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    var startCoords = starNode.GlobalCoords;
                    var endCoords = node.GlobalCoords;
                    var difference = endCoords - startCoords;
                    difference /= world.nodesPerCell;
                    difference.x += System.Math.Sign(difference.x);
                    difference.z += System.Math.Sign(difference.z);
                    if (difference.z == 0) difference.z = 1;
                    if (difference.x == 0) difference.x = 1;
                    var xCount = Mathf.Abs(difference.x);
                    var zCount = Mathf.Abs(difference.z);
                    var zSign = System.Math.Sign(difference.z);
                    var xSign = System.Math.Sign(difference.x);
                    var offset = prefab.offset;
                    Debug.Log($"count: {xCount},{zCount}, s: {startCoords}, e: {endCoords}");
                    for (int z = 0; z < zCount; z++)
                    {
                        for (int x = 0; x < xCount; x++)
                        {
                            var curCoords =
                                startCoords
                                + new Vector3Int(x * xSign, 0, z * zSign)
                                +  new Vector3Int(offset.x * x * xSign, offset.y, offset.z * z * zSign);
                            var curNode = world.GetNodeFromNodeGlobalCoords(curCoords);
                            if (curNode != null)
                            {
                                var instance = Instantiate(prefab);
                                instance.transform.parent = curNode.grid.transform;
                                instance.transform.localPosition = new Vector3(curNode.localPosition.x, starNode.localPosition.y, curNode.localPosition.z);
                                instance.transform.localRotation = Quaternion.identity;
                                instance.gameObject.SetActive(true);
                                instance.Place_OLD(curNode, instance.transform.localEulerAngles.y, starNode.localPosition.y);
                            }
                        }
                    }
                }
            }
        }
	}
}