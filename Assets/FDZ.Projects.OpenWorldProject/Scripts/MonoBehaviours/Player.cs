﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;
using TMPro;

namespace FDZ.Projects.OpenWorldProject
{
    public class Player : MonoBehaviour
    {
        public static int currentGridLevel = int.MinValue;

        public TextMeshProUGUI textField;

        [Space]
        public int current;

        [Space]
        public FlyMovement flyMovement;
        public Character character;

        public void Tick(float delta)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                current++;
                if (current >= 2)
                {
                    current = 0;
                }
            }

            textField.text = "Press TAB to switch modes.";

            switch (current)
            {
                case 0:
                {
                    var world = GameManager.ins.worldMngr.world;
                    if (currentGridLevel == int.MinValue)
                    {
                        currentGridLevel = world.groundLevel;
                    }
                    var wheel = Input.GetAxis("Mouse ScrollWheel");
                    currentGridLevel += System.Math.Sign(wheel) * Mathf.CeilToInt(Mathf.Abs(wheel));
                    if (currentGridLevel < 0)
                    {
                        currentGridLevel = world.levelsCount - 1;
                    }
                    else if (currentGridLevel >= world.levelsCount)
                    {
                        currentGridLevel = 0;
                    }

                    textField.text += "\nCurrent mode: Building";
                    textField.text += "\n\tWASD to move camera";
                    textField.text += "\n\tRight Click + Mouse movement to rotate camera";
                    textField.text += "\n\tMouse Wheel to change grid level";
                    textField.text += $"\n\t\tCurrent: {currentGridLevel + 1}/{world.levelsCount} {(currentGridLevel == world.groundLevel ? "(Ground Level)" : "")}";

                    flyMovement.Tick(delta);
                    GetComponentInChildren<NodeObjectPlacer_Test>(true).Tick();
                }
                break;

                case 1:
                {
                    textField.text += "\nCurrent mode: Character WASD";
                    var inputMoveDir = UtilsInput.GetMoveDirectionFromMainCamera();
                    character.inputMoveDir = inputMoveDir;
                    if (inputMoveDir.sqrMagnitude != 0)
                    {
                        character.inputLookDir = inputMoveDir;
                    }
                }
                break;
            }
        }
    }
}