﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
	public class CharactersManager : MonoBehaviour
	{
		public Character[] all;

		public void Tick(float delta)
        {
            for (int i = 0; i < all.Length; i++)
            {
                all[i].Tick(delta);
            }
        }
	}
}