﻿/*

-----------------------------------------------------------------------------------------------------------------------------------

- it would be supper cool if the terrain normals would be calculated on the shader

- should I do the creation of arrays on other thread?

- should I have different LODS for the water and the terrain?

- use only PlaneMeshDataGroup for water, this cannot be done for terrains cus the vertex.y needs to be setted for the colliders

-----------------------------------------------------------------------------------------------------------------------------------

- test performance of Mesh.SetVertexBufferData()
  https://docs.unity3d.com/ScriptReference/Mesh.SetVertexBufferData.html
  how much will difficult my ability to continue working on the project?

- tessellation (gpu quadtrees?) vs what i'm doing now (multiple meshes)
  https://www.researchgate.net/publication/331761994_Quadtrees_on_the_GPU
  https://catlikecoding.com/unity/tutorials/advanced-rendering/tessellation/

-----------------------------------------------------------------------------------------------------------------------------------

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Drawing;


namespace FDZ.Projects.OpenWorldProject
{
    public class WorldManager : MonoBehaviour
    {
        public const int ACTIVE_LOD = 0;

        public Transform terrainParent;

        [Space]
        public float cellSize = 1f;

        [Space]
        public int chunksPerLine = 4;
        public int cellsPerLine = 64;
        public int nodesPerCell = 2;

        [Space]
        public int levelsUnderGround = 2;
        public int levelsOverGround = 2;

        [Space]
        public int[] terrainLOD = new int[] { 1, 2, 4, 8, 16 };

        [Space]
        public FastNoiseLiteParameters noiseGenerationParameters;

        [Space]
        public float heightMultiplier = 1f;
        [Range(0, 360)] public float rotationDegrees;

        [Header("# TERRAIN SHADER")]
        public Texture2D[] textures;
        public float[] texturesScales;
        [Range(0, 1)] public float[] startingHeights;
        [Range(0, 1)] public float[] blends;
        public Color[] tints;

        [Header("# WATER")]
        public bool instantiateWater = true;
        public Color waterColor;
        public float heightOffset;

        // PRIVATE
        public World world;

        private Material terrainMaterial;
        private MaterialPropertyBlock terrainMaterialPropertyBlock;

        private Material waterMaterial;
        private MaterialPropertyBlock waterMaterialPropertyBlock;

        public bool isGenerationInProgress;
        public bool isGenerationComplete;

        //Node startNode = null;

        public void BeginGeneration()
        {
            if (isGenerationComplete || isGenerationInProgress)
            {
                return;
            }

            isGenerationInProgress = true;

            var noiseGen = new FastNoiseLite();
            noiseGen.SetAllParameters(noiseGenerationParameters);

            terrainMaterial = new Material(Shader.Find("FDZ/Terrain"));
            {
                // MaterialPropertyBlock
                // Renderer.SetPropertyBlock() -> Lets you set or clear per-renderer or per-material parameter overrides.
                // also setting the shader values this way makes the shader to not loose values when unity editor reimports stuff during play mode,
                // like would happen if using Material.SetFloat(), Material.SetColorArray(), etc
                terrainMaterialPropertyBlock = new MaterialPropertyBlock();
                terrainMaterialPropertyBlock.SetFloat("minHeight", 0);
                terrainMaterialPropertyBlock.SetFloat("maxHeight", heightMultiplier);
                terrainMaterialPropertyBlock.SetInt("layersCount", textures.Length);
                terrainMaterialPropertyBlock.SetTexture("textures", MeshUtils.GenerateTextureArray(textures, 512, TextureFormat.RGB565));
                terrainMaterialPropertyBlock.SetFloatArray("texturesScales", texturesScales);
                terrainMaterialPropertyBlock.SetFloatArray("startingHeights", startingHeights);
                terrainMaterialPropertyBlock.SetFloatArray("blends", blends);
                terrainMaterialPropertyBlock.SetVectorArray("tints", tints.Select((color) => new Vector4(color.r, color.g, color.b, 1f)).ToArray());
                terrainMaterialPropertyBlock.SetFloatArray("tintsStrength", tints.Select((color) => color.a).ToArray());
                terrainMaterialPropertyBlock.SetFloat("rotationDegrees", rotationDegrees);
            }

            waterMaterial = new Material(Shader.Find("FDZ/Waves"));
            {
                waterMaterialPropertyBlock = new MaterialPropertyBlock();
                waterMaterialPropertyBlock.SetColor("_Color", waterColor);
            }

            StartCoroutine(CreateWorldCorotuine());
            IEnumerator CreateWorldCorotuine()
            {
                Debug.Log("World creation started");
                world = new World(chunksPerLine, cellSize, cellsPerLine, nodesPerCell, terrainLOD, levelsUnderGround, levelsOverGround);

                int chunkCount = 0;
                int chunkCountMax = world.chunksPerLine * chunksPerLine;

                for (int chunkY = 0; chunkY < world.chunksPerLine; chunkY++)
                {
                    for (int chunkX = 0; chunkX < world.chunksPerLine; chunkX++)
                    {
                        //loadingTextField.text = $"Creating world chunks ({chunkCount + 1} / {chunkCountMax})";

                        var chunk = world.chunks[chunkX, chunkY];
                        var terrainMeshData = chunk.terrain[ACTIVE_LOD];
                        var waterMeshData = chunk.water[ACTIVE_LOD];

                        // CREATE GAME OBJECTS
                        chunk.CreateGameObject(terrainParent);

                        // GENERATE VERTICES AND TRIANGLES
                        yield return null;
                        waterMeshData.GenerateVerticesAndTriangles();
                        terrainMeshData.GenerateVerticesAndTriangles();

                        // GENERATE HEIGHT MAP, ALIGN TERRAIN AND GRID TO HEIGHT MAP
                        yield return null;
                        chunk.GenerateHeightMap(noiseGen, heightMultiplier);
                        terrainMeshData.SetHeight(chunk.heightMap);
                        chunk.AlignGridToHeightMap();

                        // CALCULATE TERRAIN NORMALS
                        yield return null;
                        terrainMeshData.NormalsUpdate();

                        // ASSIGN TERRAIN STUFF
                        yield return null;
                        chunk.terrainMeshRenderer.sharedMaterial = terrainMaterial;
                        chunk.terrainMeshRenderer.SetPropertyBlock(terrainMaterialPropertyBlock);
                        chunk.UpdateTerrainMesh();

                        // ASSIGN WATER STUFF
                        yield return null;
                        if (instantiateWater)
                        {
                            var waterMesh = new Mesh();
                            waterMesh.Clear();
                            waterMesh.vertices = waterMeshData.vertices;
                            waterMesh.triangles = waterMeshData.triangles;
                            chunk.waterMeshFilter.sharedMesh = waterMesh;
                            chunk.waterMeshRenderer.sharedMaterial = waterMaterial;
                            chunk.waterMeshRenderer.SetPropertyBlock(waterMaterialPropertyBlock);
                            chunk.waterMeshRenderer.transform.localPosition = new Vector3(0f, heightOffset, 0f);
                        }

                        // COUNT
                        yield return null;
                        chunkCount++;
                    }
                }

                isGenerationComplete = true;
                Debug.Log("World creation finished");
            }
        }
        /******************/

        public void Tick()
        {
            if (isGenerationComplete == false)
            {
                return;
            }

            var mousePos = Input.mousePosition;
            var cam = FindObjectOfType<Camera>();
            var ray = cam.ScreenPointToRay(mousePos);
            var rayMaxDistance = cam.farClipPlane;
            if (Physics.Raycast(ray, out RaycastHit hitInfo, cam.farClipPlane))
            {
                return;
                var nodeCoords = world.CalculateNodeGlobalCoordsFromWorldPosition(hitInfo.point, world.groundLevel);
                var chunk = world.GetChunkFromGlobalCoords(nodeCoords);
                var y = world.groundLevel;
                for (int z = 0; z < world.cellsPerLine - 1; z++)
                {
                    for (int x = 0; x < world.cellsPerLine - 1; x++)
                    {
                        var xx = x * world.nodesPerCell;
                        var zz = z * world.nodesPerCell;
                        for (int i = 0; i < world.nodesPerCell; i++)
                        {
                            var s = .02f;
                            Draw.ingame.Line(
                                chunk.grid.nodes[xx + i, y, zz].Position + Vector3.up * s,
                                chunk.grid.nodes[xx + i + 1, y, zz].Position + Vector3.up * s,
                                Color.white);
                            Draw.ingame.Line(
                                chunk.grid.nodes[xx, y, zz + i].Position + Vector3.up * s,
                                chunk.grid.nodes[xx, y, zz + i + 1].Position + Vector3.up * s,
                                Color.white);
                        }
                    }
                }
                Draw.ingame.WireSphere(chunk.grid.nodes[0, world.groundLevel, 0].Position, .1f, Color.yellow.GetWithAlpha(.7f));
            }

            // DRAW CHUNK LINKS
            //foreach (var chunk in world.chunks)
            //{
            //    for (int xz = 0; xz < world.nodesPerLine - 1; xz++)
            //    {
            //        Draw.ingame.Line(
            //            chunk.grid.nodes[xz, world.groundLevel, 0].WorldPosition,
            //            chunk.grid.nodes[xz + 1, world.groundLevel, 0].WorldPosition,
            //            Color.red.GetWithAlpha(.7f));
            //        Draw.ingame.Line(
            //            chunk.grid.nodes[0, world.groundLevel, xz].WorldPosition,
            //            chunk.grid.nodes[0, world.groundLevel, xz + 1].WorldPosition,
            //            Color.red.GetWithAlpha(.7f));
            //    }
            //}

            /*
            var mousePos = Input.mousePosition;
            var cam = FindObjectOfType<Camera>();
            var ray = cam.ScreenPointToRay(mousePos);
            var rayMaxDistance = cam.farClipPlane;
            Draw.ingame.Ray(ray.origin, ray.direction * rayMaxDistance, Color.red);
            if (Physics.Raycast(ray, out RaycastHit hitInfo, cam.farClipPlane))
            {
                Draw.ingame.WireSphere(hitInfo.point, .03f, Color.red);
                var worldPos = hitInfo.point;
                
                var nodeGlobalCoords = WorldUtils.CalculateNodeGlobalCoordsFromWorldPositionRounding(world, worldPos);
                //WorldUtils.DrawNodeSquare(world, nodeGlobalCoords);
                
                //nodeObjectPlacer.Tick(world, worldPos);

                var currentNode = world.GetNodeFromNodeGlobalCoords(nodeGlobalCoords);
                //Draw.ingame.WireSphere(currentNode.WorldPosition, .1f, Color.blue);
                if (Input.GetMouseButtonDown(0))
                {
                    startNode = currentNode;
                }
                else if (Input.GetMouseButton(0))
                {
                    var path = new NodePathfinding().FindPath(startNode, currentNode);
                    if (path != null)
                    {
                        WorldUtils.DrawPath(world, path);
                    }
                }
            }
            */
        }

        /******************/
    }
}