﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
	public class GameManager : MonoBehaviour
	{
        public static GameManager ins;

        public Player player;
        [Space]
		public WorldManager worldMngr;
        public CharactersManager charactersMngr;

        private void Awake()
        {
            ins = this;
            worldMngr.BeginGeneration();
        }

        private void Update()
        {
            if (worldMngr.isGenerationComplete == false)
            {
                return;
            }
            var delta = Time.deltaTime;
            worldMngr.Tick();
            player.Tick(delta);
            charactersMngr.Tick(delta);
        }
    }
}