﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;
using System;

namespace FDZ.Projects.OpenWorldProject
{
    public class NodeObject : MonoBehaviour
    {
        public NodeObjectPlacer_Test.GetNodeMode placeOnNode;
        public Vector3Int size;
        public Vector3Int offset;
        public float height;

        public Node mainNode;
        public HashSet<Node> placedOnNodes = new HashSet<Node>();

        private void Update()
        {
            foreach (var node in placedOnNodes)
            {
                Draw.ingame.WireSphere(node.Position, .025f);
            }
        }

        public void RotateSizeAndOffset(float rotation, out Vector3Int size, out Vector3Int offset, out Vector3Int count, out Vector3Int dir)
        {
            // TODO: maybe use an enum? :p
            rotation = Utils.WrapAngle(rotation);
            size = this.size;
            offset = this.offset;
            switch (rotation)
            {
                default:
                case 0:
                //case 180:
                break;

                case 90:
                //case -90:
                size = new Vector3Int(size.z, size.y, size.x);
                offset = new Vector3Int(offset.z, offset.y, offset.x);
                break;

                case 180:
                size = new Vector3Int(size.x, size.y, -size.z);
                offset = new Vector3Int(offset.x, offset.y, -offset.z);
                break;

                case -90:
                size = new Vector3Int(-size.z, size.y, size.x);
                offset = new Vector3Int(-offset.z, offset.y, offset.x);
                break;
            }
            count = new Vector3Int(Mathf.Abs(size.x), Mathf.Abs(size.y), Mathf.Abs(size.z));
            dir = new Vector3Int(Math.Sign(size.x), Math.Sign(size.y), Math.Sign(size.z));
        }

        public virtual void Place(Node onNode, float rotation)
        {
            mainNode = onNode;
            placedOnNodes.Clear();
            var colliders = GetComponentsInChildren<Collider>(true);
            foreach (var coll in colliders)
            {
                coll.enabled = false;
            }
        }

        public virtual void PlaceConfirm()
        {
            foreach (var node in placedOnNodes)
            {
                node.objects.Add(this);
                if (node.localCoords.y == mainNode.localCoords.y)
                {
                    node.localPosition.y = transform.localPosition.y;
                }
                else
                {
                    node.localPosition.y = transform.localPosition.y + height;
                }
            }
            var colliders = GetComponentsInChildren<Collider>(true);
            foreach (var coll in colliders)
            {
                coll.enabled = true;
            }
        }

        protected void ForEachNode(Vector3Int offset, Vector3Int count, Vector3Int dir, Action<Node> OnNode)
        {
            for (int y = 0; y < count.y; y++)
            {
                for (int z = 0; z < count.z; z++)
                {
                    for (int x = 0; x < count.x; x++)
                    {
                        var worldChunk = mainNode.grid.worldChunk;
                        var world = worldChunk?.world;
                        var nodes = mainNode.grid.nodes;
                        var curOffset = new Vector3Int(dir.x * x - offset.x, y - offset.y, dir.z * z - offset.z);
                        var curNode = worldChunk != null
                            ? world.GetNodeFromNodeGlobalCoords(mainNode.GlobalCoords + curOffset)
                            : nodes[mainNode.localCoords.x + curOffset.x, mainNode.localCoords.y + curOffset.y, mainNode.localCoords.z + curOffset.z];
                        OnNode.Invoke(curNode);
                    }
                }
            }
        }

        /*
        private void PlaceConfirm_Old()
        {
            transform.parent = mainNode.grid.transform;
            transform.localPosition = mainNode.localPosition;

            var rotation = 0f;
            RotateSizeAndOffset(rotation, out Vector3Int size, out Vector3Int offset);

            var worldChunk = mainNode.grid.worldChunk;
            var world = worldChunk?.world;
            var nodes = mainNode.grid.nodes;

            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < Mathf.Abs(size.z); z++)
                {
                    for (int x = 0; x < Mathf.Abs(size.x); x++)
                    {
                        var xx = System.Math.Sign(size.x) * x - offset.x;
                        var yy = y - offset.y;
                        var zz = System.Math.Sign(size.z) * z - offset.z;
                        var curNode = worldChunk != null
                            ? world.GetNodeFromNodeGlobalCoords(mainNode.GlobalCoords + new Vector3Int(xx, yy, zz))
                            : nodes[mainNode.localCoords.x + xx, mainNode.localCoords.y + yy, mainNode.localCoords.z + zz];

                        if (curNode.grid.worldChunk != null)
                        {
                            if (curNode.localCoords.y == world.groundLevel)
                            {
                                var localPositionY = mainNode.localPosition.y;
                                // the terrain is only affected if the node height is lower than the terrain height
                                curNode.localPosition.y = localPositionY;
                                if (curNode.grid.worldChunk.GetTerrainHeight(curNode.localCoords.x, curNode.localCoords.z, out float h)
                                    && h > localPositionY)
                                {
                                    curNode.grid.worldChunk.SetTerrainHeight(curNode.localCoords.x, curNode.localCoords.z, localPositionY - float.Epsilon);
                                }
                            }
                        }

                        curNode.objects.Add(this);
                        placedOnNodes.Add(curNode);
                    }
                }
            }

            foreach (var chunk in world.chunks)
            {
                chunk.UpdateTerrainMesh();
                //chunk.AlignGridToHeightMap();
            }
        }
        */

        public void Place_OLD(Node onNode, float rotation, float height)
        {
            mainNode = onNode;

            var c = mainNode.localCoords;
            var size = this.size;
            var offset = this.offset;

            // TODO: maybe use an enum? :p
            rotation = Utils.WrapAngle(rotation);
            switch (rotation)
            {
                default:
                case 0:
                //case 180:
                break;

                case 90:
                //case -90:
                size = new Vector3Int(size.z, size.y, size.x);
                offset = new Vector3Int(offset.z, offset.y, offset.x);
                break;

                case 180:
                size = new Vector3Int(size.x, size.y, -size.z);
                offset = new Vector3Int(offset.x, offset.y, -offset.z);
                break;

                case -90:
                size = new Vector3Int(-size.z, size.y, size.x);
                offset = new Vector3Int(-offset.z, offset.y, offset.x);
                break;
            }

            var worldChunk = mainNode.grid.worldChunk;
            var world = worldChunk?.world;
            var nodes = mainNode.grid.nodes;

            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < Mathf.Abs(size.z); z++)
                {
                    for (int x = 0; x < Mathf.Abs(size.x); x++)
                    {
                        var xx = System.Math.Sign(size.x) * x - offset.x;
                        var yy = y - offset.y;
                        var zz = System.Math.Sign(size.z) * z - offset.z;

                        // NOTE: trying to prepare for boat-houses
                        var curNode = worldChunk != null
                            ? world.GetNodeFromNodeGlobalCoords(mainNode.GlobalCoords + new Vector3Int(xx, yy, zz))
                            : nodes[c.x + xx, c.y + yy, c.z + zz];

                        if (curNode.grid.worldChunk != null)
                        {
                            if (curNode.localCoords.y == world.groundLevel)
                            {
                                // the terrain is only affected if the node height is lower than the terrain height
                                curNode.localPosition.y = height;
                                // TODO: instead of using grid.worldChunk use world (?
                                if (curNode.grid.worldChunk.GetTerrainHeight(curNode.localCoords.x, curNode.localCoords.z, out float h)
                                    && h > curNode.localPosition.y)
                                {
                                    curNode.grid.worldChunk.SetTerrainHeight(curNode.localCoords.x, curNode.localCoords.z, height - float.Epsilon);
                                }
                            }
                        }

                        if (y != 0 && size.y > 1)
                        {
                            //var baseNode = worldChunk != null
                            //    ? world.GetNodeFromNodeGlobalCoords(targetNode.GlobalCoords + new Vector3Int(xx, 0, zz))
                            //    : nodes[c.x + xx, c.y, c.z + zz];
                            //curNode.localPosition.y = baseNode.localPosition.y + height;
                            curNode.localPosition.y = this.height + height;
                        }

                        curNode.objects.Add(this);
                        placedOnNodes.Add(curNode);
                    }
                }
            }

            foreach (var chunk in world.chunks)
            {
                chunk.UpdateTerrainMesh();
                //chunk.AlignGridToHeightMap();
            }

            //Debug.Log($"Placed: {name} at {mainNode.GlobalCoords} with rot of {rotation}, nodes affecetd {placedOnNodes.Count}");
        }
    }
}