﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;
using TMPro;

namespace FDZ.Projects.OpenWorldProject
{
    public class NodeObjectPlacer_Walls : MonoBehaviour
    {
        public NodeObject prefab;

        private Node starNode;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void Tick()
        {
            var mousePos = Input.mousePosition;
            var cam = FindObjectOfType<Camera>();
            var ray = cam.ScreenPointToRay(mousePos);
            var rayMaxDistance = cam.farClipPlane;

            Draw.ingame.Ray(ray.origin, ray.direction * rayMaxDistance, Color.red);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, cam.farClipPlane))
            {
                Draw.ingame.WireSphere(hitInfo.point, .1f, Color.red);

                var world = GameManager.ins.worldMngr.world;
                var worldPos = hitInfo.point;
                var coords = world.CalculateCornerNodeGlobalCoordsFromWorldPositionRounding(worldPos, Player.currentGridLevel);
                var node = world.GetNodeFromNodeGlobalCoords(coords);

                if (node == null)
                {
                    return;
                }

                Draw.ingame.WireSphere(node.Position, .1f, Color.green);

                if (Input.GetMouseButtonDown(0))
                {
                    starNode = node;
                }

                if (starNode == null)
                {
                    return;
                }

                if (Input.GetMouseButton(0) &&
                    starNode.grid.worldChunk != null && node.grid.worldChunk != null &&
                    starNode.localCoords.y == node.localCoords.y)
                {
                    Draw.ingame.WireSphere(starNode.Position, .1f, Color.blue);
                    Draw.ingame.Line(starNode.Position, node.Position, Color.green);
                }

                if (Input.GetMouseButtonUp(0))
                {
                    var startCoords = starNode.GlobalCoords;
                    var endCoords = node.GlobalCoords;
                    var difference = endCoords - startCoords;
                    var offset = prefab.offset;
                    var size = prefab.size;
                    size.x -= 1;
                    size.z -= 1;
                    if (Mathf.Abs(difference.x) > Mathf.Abs(difference.z))
                    {
                        difference.z = 0;
                        offset = new Vector3Int(offset.z, offset.y, offset.x);
                        size = new Vector3Int(size.z, size.y, size.x);
                    }
                    else
                    {
                        difference.x = 0;
                    }
                    var direction = new Vector3Int(System.Math.Sign(difference.x), 0, System.Math.Sign(difference.z));
                    var placeCount = (int)(difference.magnitude / (prefab.size.z - 1));
                    for (int i = 0; i < placeCount; i++)
                    {
                        var curCoords = startCoords + offset * direction + size * direction * i;
                        var curNode = world.GetNodeFromNodeGlobalCoords(curCoords);
                        if (curNode != null)
                        {
                            var instance = Instantiate(prefab);
                            instance.transform.parent = curNode.grid.transform;
                            //instance.transform.localPosition = curNode.localPosition;
                            instance.transform.localPosition = new Vector3(curNode.localPosition.x, starNode.localPosition.y, curNode.localPosition.z);
                            instance.transform.localRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z).normalized);
                            instance.gameObject.SetActive(true);
                            instance.Place_OLD(curNode, instance.transform.localEulerAngles.y, starNode.localPosition.y);
                        }
                    }
                }
            }
        }

    }
}