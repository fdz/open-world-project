﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;

namespace FDZ.Projects.OpenWorldProject
{
    public class NodeObjectPlacer_Test : MonoBehaviour
    {
        public enum GetNodeMode
        {
            Center,
            Corner,
            EdgeMiddle,
        }

        public NodeObject prefab;

        private NodeObject placing;
        private NodeObject prevPrefab;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void Tick()
        {
            var mousePos = Input.mousePosition;
            var cam = FindObjectOfType<Camera>();
            var ray = cam.ScreenPointToRay(mousePos);
            var rayMaxDistance = cam.farClipPlane;

            Draw.ingame.Ray(ray.origin, ray.direction * rayMaxDistance, Color.red);

            if (Physics.Raycast(ray, out RaycastHit hitInfo, cam.farClipPlane))
            {
                Draw.ingame.WireSphere(hitInfo.point, .1f, Color.red);

                var world = GameManager.ins.worldMngr.world;
                var rayHitPoint = hitInfo.point;
                Vector3Int nodeGlobalCoords;
                switch (prefab.placeOnNode)
                {
                    default:
                    case GetNodeMode.Center:
                    nodeGlobalCoords = world.CalculateCenterNodeGlobalCoordsFromWorldPositionRounding(rayHitPoint, Player.currentGridLevel);
                    break;

                    case GetNodeMode.Corner:
                    nodeGlobalCoords = world.CalculateCornerNodeGlobalCoordsFromWorldPositionRounding(rayHitPoint, Player.currentGridLevel);
                    break;

                    case GetNodeMode.EdgeMiddle:
                    nodeGlobalCoords = world.CalculateEdgeMiddleNodeGlobalCoordsFromWorldPositionRounding(rayHitPoint, Player.currentGridLevel);
                    break;
                }

                var node = world.GetNodeFromNodeGlobalCoords(nodeGlobalCoords);
                if (node == null)
                {
                    return;
                }

                Draw.ingame.WireSphere(node.Position, .1f, Color.green);

                if (prevPrefab == null || prevPrefab != prefab)
                {
                    prevPrefab = prefab;
                    if (placing != null)
                    {
                       Destroy(placing.gameObject);
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    placing.PlaceConfirm();
                    placing = null;
                }
                else
                {
                    if (placing == null)
                    {
                        placing = Instantiate(prefab);
                    }
                    placing.Place(node, 0);
                }
            }
        }
    }
}