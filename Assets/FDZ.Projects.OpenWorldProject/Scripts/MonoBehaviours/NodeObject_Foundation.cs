﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace FDZ.Projects.OpenWorldProject
{
    public class NodeObject_Foundation : NodeObject
    {
        public override void Place(Node onNode, float rotation)
        {
            base.Place(onNode, rotation);

            if (mainNode.grid.worldChunk == null || mainNode.localCoords.y != mainNode.grid.worldChunk.world.groundLevel)
            {
                Debug.LogError("can't place, is not ground level");
                Destroy(gameObject);
                return;
            }

            var localPosition = mainNode.localPosition;

            RotateSizeAndOffset(rotation, out Vector3Int size, out Vector3Int offset, out Vector3Int count, out Vector3Int dir);
            ForEachNode(offset, count, dir, curNode =>
            {
                foreach (var obj in curNode.objects)
                {
                    if (localPosition.y < obj.mainNode.localPosition.y)
                    {
                        localPosition.y = obj.mainNode.localPosition.y;
                    }
                }
            });

            ForEachNode(offset, count, dir, curNode =>
            {
                placedOnNodes.Add(curNode);
                if (localPosition.y < curNode.localPosition.y)
                {
                    localPosition.y = curNode.localPosition.y + float.Epsilon;
                }
            });

            transform.parent = mainNode.grid.transform;
            transform.localPosition = localPosition;
        }
    }
}