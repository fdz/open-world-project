﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FDZ.Projects.OpenWorldProject
{
    public class NodeObject_Wall : NodeObject
    {
        public override void Place(Node onNode, float rotation)
        {
            base.Place(onNode, rotation);

            var world = GameManager.ins.worldMngr.world;
            
            if (onNode.localCoords.x % world.nodesPerCell == 0)
            {
                rotation = 0;
            }
            else
            {
                rotation = 90f;
            }

            var localPosition = mainNode.localPosition;

            RotateSizeAndOffset(rotation, out Vector3Int size, out Vector3Int offset, out Vector3Int count, out Vector3Int dir);
            ForEachNode(offset, count, dir, curNode =>
            {
                placedOnNodes.Add(curNode);
            });

            transform.parent = mainNode.grid.transform;
            transform.localRotation = Quaternion.Euler(0f, rotation, 0f);
            transform.localPosition = localPosition;
        }
    }
}