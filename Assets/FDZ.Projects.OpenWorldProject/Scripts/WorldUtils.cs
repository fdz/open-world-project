﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Drawing;

namespace FDZ.Projects.OpenWorldProject
{
    public static class WorldUtils
    {
        public static void DrawPath(World world, List<Node> path)
        {
            if (path.Count > 1)
            {
                Draw.ingame.WireSphere(path[0].Position, world.nodeSize / 8, Color.green);
                for (int i = 0; i < path.Count - 1; i++)
                {
                    var a = path[i];
                    var b = path[i + 1];
                    Draw.ingame.Line(
                        a.Position + Vector3.up * .01f,
                        b.Position + Vector3.up * .01f,
                        Color.green);
                }
            }
        }
    }
}