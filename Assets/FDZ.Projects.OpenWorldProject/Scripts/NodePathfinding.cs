﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
	public class NodePathfinding
    {
        public List<Node> FindPath(Node startNode, Node endNode)
        {
            if (startNode == null)
            {
                Debug.LogError("start node is null");
                return null;
            }

            if (endNode == null)
            {
                Debug.LogError("end node is null");
                return null;
            }

            float CalculateDistance(Node a, Node b)
            {
                //var diff = b.grid != a.grid ? (b.WorldPosition - a.WorldPosition) : (b.localPosition - a.localPosition);
                var diff = (b.Position - a.Position);
                //return diff.x * diff.x + diff.y * diff.y + diff.z * diff.z;
                return diff.x * diff.x + diff.z * diff.z;
                //return diff.x * diff.x + diff.z * diff.z + diff.y / a.grid.nodeSize;
            }

            float CalculateFCost(NodePathingInfo nodePathInfo)
            {
                return nodePathInfo.gCost + nodePathInfo.hCost;
            }

            var open = new List<NodePathingInfo>();
            var closed = new List<NodePathingInfo>();

            var startNodePInfo = new NodePathingInfo();
            startNodePInfo.node = startNode;
            startNodePInfo.cameFromNode = null;

            startNodePInfo.gCost = 0;
            startNodePInfo.hCost = CalculateDistance(startNode, endNode);
            startNodePInfo.fCost = CalculateFCost(startNodePInfo);

            startNode.pathingInfo.Add(this, startNodePInfo);
            open.Add(startNodePInfo);

            var startDateTime = System.DateTime.Now;
            while (open.Count > 0)
            {
                // GET LOWEST F COST NODE
                // TODO: binary tree ?
                var currentNodePInfo = open[0];
                foreach (var node in open)
                {
                    if (node.fCost < currentNodePInfo.fCost)
                    {
                        currentNodePInfo = node;
                    }
                }

                if (currentNodePInfo.node == endNode)
                {
                    // CALCULATE PATH
                    var path = new List<Node>();
                    var auxNodePInfo = currentNodePInfo;
                    path.Insert(0, auxNodePInfo.node);
                    while (auxNodePInfo.cameFromNode != null)
                    {
                        // TRYING TO FIX ZIG ZAGS
                        // a
                        // |\
                        // b-c
                        var b = auxNodePInfo.cameFromNode.pathingInfo[this];
                        if (b != null)
                        {
                            var c = b.cameFromNode;
                            if (c != null && c.neighbors.Contains(auxNodePInfo.node))
                            {
                                auxNodePInfo.cameFromNode = c;
                                continue;
                            }
                        }
                        path.Insert(0, auxNodePInfo.cameFromNode);
                        auxNodePInfo = auxNodePInfo.cameFromNode.pathingInfo[this];
                    }
                    //path.Reverse();
                    return path;
                }

                open.Remove(currentNodePInfo);
                closed.Add(currentNodePInfo);
                if (currentNodePInfo.node.neighbors.Count == 0)
                {
                    Debug.LogError("node " + currentNodePInfo.node.localCoords + "has no neighbors");
                }
                foreach (var neighborNode in currentNodePInfo.node.neighbors)
                {
                    //if (neighborNode.isWalkable == false)
                    //{
                    //    closed.Add(neighborNode);
                    //    continue;
                    //}
                    NodePathingInfo neighborNodePInfo;
                    if (neighborNode.pathingInfo.TryGetValue(this, out neighborNodePInfo) == false)
                    {
                        neighborNodePInfo = new NodePathingInfo();
                        neighborNodePInfo.node = neighborNode;
                        neighborNodePInfo.cameFromNode = null;

                        neighborNodePInfo.gCost = float.MaxValue;
                        neighborNodePInfo.hCost = CalculateDistance(neighborNodePInfo.node, endNode);
                        neighborNodePInfo.fCost = CalculateFCost(neighborNodePInfo);

                        neighborNode.pathingInfo.Add(this, neighborNodePInfo);
                    }
                    if (closed.Contains(neighborNodePInfo) == false)
                    {
                        var tentativeGCost = currentNodePInfo.gCost + CalculateDistance(currentNodePInfo.node, neighborNode);
                        if (tentativeGCost < neighborNodePInfo.gCost)
                        {
                            neighborNodePInfo.cameFromNode = currentNodePInfo.node;
                            neighborNodePInfo.gCost = tentativeGCost;
                            neighborNodePInfo.hCost = CalculateDistance(neighborNode, endNode);
                            neighborNodePInfo.fCost = CalculateFCost(neighborNodePInfo);
                            if (open.Contains(neighborNodePInfo) == false)
                            {
                                open.Add(neighborNodePInfo);
                            }
                        }
                    }
                }
                if (System.DateTime.Now.Subtract(startDateTime).TotalSeconds > 1f)
                {
                    Debug.LogError("canceled while loop cust it took to long");
                    return null;
                }
            }

            return null;
        }
    }
}