﻿using UnityEngine;
using System.Collections.Generic;

namespace FDZ.Projects.OpenWorldProject
{
    public class Node
    {
        public bool isEnabled;
        public NodeGrid grid;
        public HashSet<Node> neighbors = new HashSet<Node>();
        public HashSet<NodeObject> objects = new HashSet<NodeObject>();

        public Vector3Int localCoords;
        public Vector3 localPosition;

        public Vector3Int GlobalCoords => grid.worldChunk.world.CalculateNodeGlobalCoordsFromLocalCoords(grid.worldChunk.coords, localCoords);

        public Vector3 Position => grid.transform.TransformPoint(localPosition);

        // TODO: what about perfomance?? NodePathfinding pooling? what about clearing values in NodePathingInfo??
        public Dictionary<NodePathfinding, NodePathingInfo> pathingInfo = new Dictionary<NodePathfinding, NodePathingInfo>();

        public void Link(Node other)
        {
            // TODO:
            // connectedToOtherGrids.Add(other.grid);
            // connectedToOtherGrids.AddRange(other.connectedToOtherGrids);
            // other.neighbors.Add(this);
            // neighbors.Add(other);
            // this could be used for check if the grids are connected before finding the path
        }
    }

    public class NodePathingInfo
    {
        public Node node;
        public Node cameFromNode;

        // walking cost from the start node
        public float gCost;
        
        // heuristic coast to reach end node
        public float hCost;

        // gCost + hCost
        public float fCost;
    }
}