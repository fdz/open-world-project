﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
    public class PlaneMeshData
    {
        public readonly int verticesPerLine;
        public readonly float vertexOffset;
        public readonly int vertexStep;
        public readonly Vector3[] vertices;
        public readonly Vector3[] normals;
        public readonly int[] triangles;

        public PlaneMeshData(float cellSize, int cellsPerLine, int vertexStep, bool createNormals = true)
        {
            this.verticesPerLine = cellsPerLine / vertexStep + 1;
            this.vertexOffset = cellSize;
            this.vertexStep = vertexStep;
            vertices = new Vector3[verticesPerLine * verticesPerLine];
            triangles = new int[vertices.Length * 6];
            if (createNormals)
            {
                normals = new Vector3[vertices.Length];
            }
        }

        public void GenerateVerticesAndTriangles()
        {
            int vi = 0;
            int ti = 0;
            for (int y = 0; y < verticesPerLine; y++)
            {
                for (int x = 0; x < verticesPerLine; x++)
                {
                    int xx = x * vertexStep;
                    int yy = y * vertexStep;
                    vertices[vi] = new Vector3(xx * vertexOffset, 0, yy * vertexOffset);
                    if (y < verticesPerLine - 1 && x < verticesPerLine - 1)
                    {
                        triangles[ti] = vi;
                        triangles[ti + 1] = triangles[ti + 4] = vi + verticesPerLine;
                        triangles[ti + 2] = triangles[ti + 3] = vi + 1;
                        triangles[ti + 5] = vi + verticesPerLine + 1;
                        ti += 6;
                    }
                    vi++;
                }
            }
        }

        public void NormalsUpdate()
        {
            MeshUtils.CalculateNormals(vertices, normals, triangles);
        }

        public void SetHeight(float[,] heightMap)
        {
            for (int y = 0, vi = 0; y < verticesPerLine; y++)
            {
                for (int x = 0; x < verticesPerLine; x++, vi++)
                {
                    vertices[vi].y = heightMap[x * vertexStep, y * vertexStep];
                }
            }
        }

        public void SetHeight(float[,] heightMap, int x, int y)
        {
            var xx = x / (float)vertexStep;
            var yy = y / (float)vertexStep;
            if (xx % 1 == 0 && yy % 1 == 0)
            {
                vertices[(int)xx * verticesPerLine + (int)yy].y = heightMap[x, y];
            }
        }

        public void SetHeight(int x, int y, float value)
        {
            var xx = x / (float)vertexStep;
            var yy = y / (float)vertexStep;
            if (xx % 1 == 0 && yy % 1 == 0)
            {
                vertices[(int)xx * verticesPerLine + (int)yy].y = value;
            }
        }
    }
}