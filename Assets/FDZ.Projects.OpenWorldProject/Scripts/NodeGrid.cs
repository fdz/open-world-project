﻿using UnityEngine;
using System.Collections.Generic;

namespace FDZ.Projects.OpenWorldProject
{
    public class NodeGrid
    {
        public Transform transform;
        public readonly WorldChunk worldChunk;
        public readonly int level;
        public readonly Vector3Int size;
        public readonly Node[,,] nodes;
        public readonly float nodeSize;
        
        // TODO: make sure to use x, y, z properly, since size was a 2D and now is 3D
        public NodeGrid(WorldChunk worldChunk, Vector3Int size, float nodeSize, bool[] enabledLevels)
        {
            this.worldChunk = worldChunk;
            this.size = size;
            this.nodeSize = nodeSize;

            nodes = new Node[size.x, size.y, size.z];

            void AddNeighbor(Node node, int x, int y, int z, int xDir, int yDir, int zDir)
            {
                int xx = x + xDir;
                int yy = y + yDir;
                int zz = z + zDir;
                if (xx > 0 && xx < size.x &&
                    yy > 0 && yy < size.y &&
                    zz > 0 && zz < size.z)
                {
                    var newNeighbor = nodes[xx, yy, zz];
                    node.neighbors.Add(newNeighbor);
                    newNeighbor.neighbors.Add(node);
                }
            }

            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    for (int x = 0; x < size.x; x++)
                    {
                        var node = new Node();
                        node.isEnabled = enabledLevels[y];
                        node.grid = this;
                        node.localCoords = new Vector3Int(x, y, z);
                        node.localPosition = new Vector3(x * nodeSize, 0f, z * nodeSize);
                        //if (x > 0)
                        //{
                        //    var newNeighbor = nodes[x - 1, y, z];
                        //    node.neighbors.Add(newNeighbor);
                        //    newNeighbor.neighbors.Add(node);
                        //}
                        //if (z > 0)
                        //{
                        //    var newNeighbor = nodes[x, y, z - 1];
                        //    node.neighbors.Add(newNeighbor);
                        //    newNeighbor.neighbors.Add(node);
                        //}
                        //if (x > 0 && z > 0)
                        //{
                        //    var newNeighbor = nodes[x - 1, y, z - 1];
                        //    node.neighbors.Add(newNeighbor);
                        //    newNeighbor.neighbors.Add(node);
                        //}
                        //if (x < size.x - 1 && z > 0)
                        //{
                        //    var newNeighbor = nodes[x + 1, y, z - 1];
                        //    node.neighbors.Add(newNeighbor);
                        //    newNeighbor.neighbors.Add(node);
                        //}
                        AddNeighbor(node, x, y, z, -1, 0, 0);
                        AddNeighbor(node, x, y, z, 0, 0, -1);
                        AddNeighbor(node, x, y, z, -1, 0, -1);
                        AddNeighbor(node, x, y, z, 1, 0, -1);
                        nodes[x, y, z] = node;
                    }
                }
            }

        }
    }
}