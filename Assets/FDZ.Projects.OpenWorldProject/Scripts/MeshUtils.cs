﻿using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
	public static class MeshUtils
    {
        public static Texture2DArray GenerateTextureArray(Texture2D[] textures, int textureSize, TextureFormat textureFormat)
        {
            var textureArray = new Texture2DArray(textureSize, textureSize, textures.Length, textureFormat, true);
            for (int i = 0; i < textures.Length; i++)
            {
                textureArray.SetPixels(textures[i].GetPixels(), i);
            }
            textureArray.Apply();
            return textureArray;
        }

        public static void CalculateNormals(Vector3[] vertices, Vector3[] normals, int[] triangles)
        {
            int triangleCount = triangles.Length / 3;
            for (int i = 0; i < triangleCount; i++)
            {
                int normalTriangleIndex = i * 3;
                int vertexIndexA = triangles[normalTriangleIndex];
                int vertexIndexB = triangles[normalTriangleIndex + 1];
                int vertexIndexC = triangles[normalTriangleIndex + 2];
                Vector3 triangleNormal = SurfaceNormalFromIndices(vertices, vertexIndexA, vertexIndexB, vertexIndexC);
                normals[vertexIndexA] += triangleNormal;
                normals[vertexIndexB] += triangleNormal;
                normals[vertexIndexC] += triangleNormal;
            }
            // NOTE: for fixing the normals at the edges, we have to account for the neighbour chunks
            // so this second for loop should be done after that
            for (int i = 0; i < normals.Length; i++)
            {
                normals[i].Normalize();
            }
        }

        public static Vector3 SurfaceNormalFromIndices(Vector3[] vertices, int indexA, int indexB, int indexC)
        {
            Vector3 pointA = vertices[indexA];
            Vector3 pointB = vertices[indexB];
            Vector3 pointC = vertices[indexC];
            Vector3 sideAB = pointB - pointA;
            Vector3 sideAC = pointC - pointA;
            return Vector3.Cross(sideAB, sideAC).normalized;
        }
    }
}