﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace FDZ.Projects.OpenWorldProject
{
    public class World
    {
        public readonly float size;

        public readonly float chunkSize;
        public readonly int chunksPerLine;

        public readonly float cellSize;
        public readonly int cellsPerLine;

        public readonly float nodeSize;
        public readonly int nodesPerCell;
        public readonly int nodesPerLine;

        public readonly int levelsUnderGround;
        public readonly int levelsOverGround;
        public readonly int levelsCount;
        public readonly int groundLevel;

        public readonly int heightMapLength;

        public readonly WorldChunk[,] chunks;

        public World(int chunksPerLine, float cellSize, int cellsPerLine, int nodesPerCell, int[] vertexSteps, int levelsUnderGround, int levelsOverGround)
        {
            this.chunksPerLine = chunksPerLine;
            this.cellSize = cellSize;
            this.cellsPerLine = cellsPerLine;
            this.nodesPerCell = nodesPerCell;

            this.levelsUnderGround = levelsUnderGround < 0 ? 0 : levelsUnderGround;
            this.levelsOverGround = levelsOverGround < 0 ? 0 : levelsOverGround;
            levelsCount = 1 + this.levelsUnderGround + this.levelsOverGround;
            groundLevel = this.levelsUnderGround;

            chunkSize = cellsPerLine * cellSize;
            size = chunksPerLine * chunkSize;

            nodeSize = cellSize / nodesPerCell;

            // + 1 means duplicated nodes / overlaping noises
            //nodesPerLine = cellsPerLine * nodesPerCell + 1;
            // + 2, 1 for the edge vertex, 1 for the edge node
            //heightMapLength = cellsPerLine + 2;

            nodesPerLine = cellsPerLine * nodesPerCell;
            heightMapLength = cellsPerLine + 1;

            chunks = new WorldChunk[chunksPerLine, chunksPerLine];
            for (int y = 0; y < chunksPerLine; y++)
            {
                for (int x = 0; x < chunksPerLine; x++)
                {
                    chunks[x, y] = new WorldChunk(
                        this,
                        new Vector2Int(x, y),
                        vertexSteps);
                }
            }

            //var n = nodesPerLine - 1;
            //var a = chunks[0, 0].grid.nodes[n, n];
            //var b = chunks[1, 1].grid.nodes[0, 0];
            //a.neighbors.Add(b);
            //b.neighbors.Add(a);
            //var c = chunks[1, 0].grid.nodes[0, n];
            //a.neighbors.Add(c);
            //c.neighbors.Add(a);
            //var d = chunks[0, 1].grid.nodes[n, 0];
            //a.neighbors.Add(d);
            //d.neighbors.Add(a);

            // LINKING GROUND LEVELS
            void Link(WorldChunk chunk, int nodeLocalCoordsX, int nodeLocalCoordsZ, Vector3Int dir)
            {
                var node = chunk.grid.nodes[nodeLocalCoordsX, groundLevel, nodeLocalCoordsZ];
                var nodeGlobalCoords = CalculateNodeGlobalCoordsFromLocalCoords(chunk.coords, node.localCoords);
                var other = GetNodeFromNodeGlobalCoords(nodeGlobalCoords + dir);
                if (other != null)
                {
                    if (node.neighbors.Contains(other) == false)
                    {
                        node.neighbors.Add(other);
                    }
                    if (other.neighbors.Contains(node) == false)
                    {
                        other.neighbors.Add(node);
                    }
                }
            }
            int N = nodesPerLine - 1;
            for (int cy = 0; cy < chunksPerLine; cy++)
            {
                for (int cx = 0; cx < chunksPerLine; cx++)
                {
                    var chunk = chunks[cx, cy];
                    for (int n = 0; n < nodesPerLine; n++)
                    {
                        Link(chunk, N, n, new Vector3Int(1, 0, 0));
                        Link(chunk, N, n, new Vector3Int(1, 0, 1));
                        Link(chunk, N, n, new Vector3Int(1, 0, -1));

                        Link(chunk, n, N, new Vector3Int(0, 0, 1));
                        Link(chunk, n, N, new Vector3Int(1, 0, 1));
                        Link(chunk, n, N, new Vector3Int(-1, 0, 1));
                    }
                    //Link(chunk, N, N, new Vector2Int(1, 1));
                }
            }
        }

        public Vector2Int CalculateChunkCoordsFromWorldPosition(Vector3 worldPos)
        {
            return new Vector2Int(
                (int)(worldPos.x / (cellSize * cellsPerLine) + chunksPerLine / 2),
                (int)(worldPos.z / (cellSize * cellsPerLine) + chunksPerLine / 2));
        }

        public Vector2Int CalculateChunkCoordsFromNodeGlobalCoords(Vector3Int nodeGlobalCoords)
        {
            return new Vector2Int(
                nodeGlobalCoords.x /= nodesPerLine,
                nodeGlobalCoords.z /= nodesPerLine);
        }

        public Vector3Int CalculateNodeGlobalCoordsFromWorldPosition(Vector3 worldPos, int level)
        {
            return new Vector3Int(
                (int)(worldPos.x / nodeSize + chunksPerLine / 2 * nodesPerLine),
                level,
                (int)(worldPos.z / nodeSize + chunksPerLine / 2 * nodesPerLine));
        }

        public Vector3Int CalculateNodeGlobalCoordsFromWorldPositionRounding(Vector3 worldPos, int level)
        {
            return new Vector3Int(
                Mathf.RoundToInt(worldPos.x / nodeSize + chunksPerLine / 2 * nodesPerLine),
                level,
                Mathf.RoundToInt(worldPos.z / nodeSize + chunksPerLine / 2 * nodesPerLine));
        }

        public Vector3Int CalculateNodeGlobalCoordsFromLocalCoords(Vector2Int chunkCoords, Vector3Int nodeLocalCoords)
        {
            nodeLocalCoords.x += chunkCoords.x * nodesPerLine;
            nodeLocalCoords.z += chunkCoords.y * nodesPerLine;
            return nodeLocalCoords;
        }

        public Vector3Int CalculateNodeLocalCoordsFromGlobalCoords(Vector2Int chunkCoords, Vector3Int nodeGlobalCoords)
        {
            nodeGlobalCoords.x -= chunkCoords.x * nodesPerLine;
            nodeGlobalCoords.z -= chunkCoords.y * nodesPerLine;
            return nodeGlobalCoords;
        }

        public Vector3Int CalculateCornerNodeGlobalCoordsFromWorldPositionRounding(Vector3 worldPos, int level)
        {
            return new Vector3Int(
                Mathf.RoundToInt(worldPos.x / cellSize + chunksPerLine / 2 * cellsPerLine) * nodesPerCell,
                level,
                Mathf.RoundToInt(worldPos.z / cellSize + chunksPerLine / 2 * cellsPerLine) * nodesPerCell);
        }

        public Vector3Int CalculateCenterNodeGlobalCoordsFromWorldPositionRounding(Vector3 worldPos, int level)
        {
            var x = worldPos.x / cellSize + chunksPerLine / 2 * cellsPerLine;
            var z = worldPos.z / cellSize + chunksPerLine / 2 * cellsPerLine;

            var xf = x * nodesPerCell;
            var zf = z * nodesPerCell;

            var xi = Mathf.RoundToInt(x) * nodesPerCell;
            var zi = Mathf.RoundToInt(z) * nodesPerCell;

            var xd = System.Math.Sign(xf - xi);
            xd = xd % nodesPerCell == 0 ? 1 : xd;
            xd *= (nodesPerCell / 2);

            var zd = System.Math.Sign(zf - zi);
            zd = zd % nodesPerCell == 0 ? 1 : zd;
            zd *= (nodesPerCell / 2);

            return new Vector3Int(
                xi + xd,
                level,
                zi + zd);
        }

        public Vector3Int CalculateEdgeMiddleNodeGlobalCoordsFromWorldPositionRounding(Vector3 worldPos, int level)
        {
            var x = worldPos.x / cellSize + chunksPerLine / 2 * cellsPerLine;
            var z = worldPos.z / cellSize + chunksPerLine / 2 * cellsPerLine;

            var xf = x * nodesPerCell;
            var zf = z * nodesPerCell;

            var xi = Mathf.RoundToInt(x) * nodesPerCell;
            var zi = Mathf.RoundToInt(z) * nodesPerCell;

            if (Mathf.Abs(xf - xi) > Mathf.Abs(zf - zi))
            {
                var xd = System.Math.Sign(xf - xi);
                xd = xd % nodesPerCell == 0 ? 1 : xd;
                xd *= (nodesPerCell / 2);
                return new Vector3Int(
                    xi + xd,
                    level,
                    zi);
            }
            else
            {
                var zd = System.Math.Sign(zf - zi);
                zd = zd % nodesPerCell == 0 ? 1 : zd;
                zd *= (nodesPerCell / 2);
                return new Vector3Int(
                    xi,
                    level,
                    zi + zd);
            }
        }

        public bool AreChunkCoordsInRange(Vector2Int chunkCoords)
        {
            return chunkCoords.x >= 0 && chunkCoords.x < chunksPerLine && chunkCoords.y >= 0 && chunkCoords.y < chunksPerLine;

        }

        public bool AreNodeLocalCoordsInRange(Vector3Int nodeCoords)
        {
            return nodeCoords.x >= 0 && nodeCoords.x < nodesPerLine && nodeCoords.z>= 0 && nodeCoords.z < nodesPerLine;
        }

        public bool AreCornerNodeCoords(Vector2Int coords)
        {
            return coords.x % nodesPerCell == 0 && coords.y % nodesPerCell == 0;
        }

        public WorldChunk GetChunkFromGlobalCoords(Vector3Int nodeGlobalCoords)
        {
            var chunkCoords = CalculateChunkCoordsFromNodeGlobalCoords(nodeGlobalCoords);
            if (AreChunkCoordsInRange(chunkCoords))
            {
                return chunks[chunkCoords.x, chunkCoords.y];
            }
            return null;
        }

        public Node GetNodeFromNodeGlobalCoords(Vector3Int nodeGlobalCoords)
        {
            var chunk = GetChunkFromGlobalCoords(nodeGlobalCoords);
            if (chunk != null)
            {
                var nodeLocalCoords = CalculateNodeLocalCoordsFromGlobalCoords(chunk.coords, nodeGlobalCoords);
                if (AreNodeLocalCoordsInRange(nodeLocalCoords))
                {
                    return chunk.grid.nodes[nodeLocalCoords.x, nodeGlobalCoords.y, nodeLocalCoords.z];
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
    }
}