﻿using System.Collections.Generic;
using UnityEngine;

namespace FDZ.Projects.OpenWorldProject
{
    public class WorldChunk
    {
        public World world;
        public Vector2Int coords;

        public readonly float[,] heightMap;

        public readonly PlaneMeshData[] terrain;
        public readonly PlaneMeshData[] water;

        public readonly NodeGrid grid;

        public GameObject gameObject;

        public MeshFilter terrainMeshFilter;
        public MeshRenderer terrainMeshRenderer;
        public MeshCollider terrainMeshCollider;

        public MeshFilter waterMeshFilter;
        public MeshRenderer waterMeshRenderer;

        public WorldChunk(World world, Vector2Int coords, int[] vertexSteps)
        {
            this.world = world;
            this.coords = coords;

            heightMap = new float[world.heightMapLength, world.heightMapLength];

            terrain = new PlaneMeshData[vertexSteps.Length];
            for (int i = 0; i < terrain.Length; i++)
            {
                terrain[i] = new PlaneMeshData(world.cellSize, world.cellsPerLine, vertexSteps[i], true);
            }

            water = new PlaneMeshData[vertexSteps.Length];
            for (int i = 0; i < terrain.Length; i++)
            {
                water[i] = new PlaneMeshData(world.cellSize, world.cellsPerLine, vertexSteps[i], false);
            }

            var enabledLevels = new bool[world.levelsCount];
            enabledLevels[world.groundLevel] = true;
            grid = new NodeGrid(this, new Vector3Int(world.nodesPerLine, world.levelsCount, world.nodesPerLine), world.nodeSize, enabledLevels);
        }

        public void CreateGameObject(Transform parent)
        {
            if (gameObject == null)
            {
                gameObject = new GameObject();
                gameObject.transform.parent = parent;
                gameObject.transform.localPosition = new Vector3(
                    coords.x * world.chunkSize - world.size / 2,
                    0,
                    coords.y * world.chunkSize - world.size / 2);
                gameObject.name = $"Chunk({coords.x},{coords.y})";

                var terrainGO = new GameObject("Terrain");
                terrainGO.transform.SetParent(gameObject.transform, false);
                terrainMeshFilter = terrainGO.AddComponent<MeshFilter>();
                terrainMeshRenderer = terrainGO.AddComponent<MeshRenderer>();
                terrainMeshCollider = terrainGO.AddComponent<MeshCollider>();

                var waterGO = new GameObject("Water");
                waterGO.transform.SetParent(gameObject.transform, false);
                waterMeshFilter = waterGO.AddComponent<MeshFilter>();
                waterMeshRenderer = waterGO.AddComponent<MeshRenderer>();

                grid.transform = gameObject.transform;
            }
        }

        public bool GetTerrainHeight(int nodeLocalCoordsX, int nodeLocalCoordsZ, out float height)
        {
            // TODO: get the neighbour chunk like in SetTerrainHeight
            var hx = nodeLocalCoordsX / (float)world.nodesPerCell;
            var hy = nodeLocalCoordsZ / (float)world.nodesPerCell;
            if (hx % 1 == 0 && hy % 1 == 0)
            {
                height = heightMap[(int)hx, (int)hy];
                return true;
            }
            height = int.MinValue;
            return false;
        }

        public void SetTerrainHeight(int nodeLocalCoordsX, int nodeLocalCoordsZ, float height)
        {
            //grid.nodes[nodeLocalCoordsX, world.groundLevel, nodeLocalCoordsZ].localPosition.y = height;

            SetVertexHeight(this, nodeLocalCoordsX, nodeLocalCoordsZ, height);

            if (nodeLocalCoordsX == 0 && nodeLocalCoordsZ == 0)
            {
                SetVertexHeight(world.chunks[coords.x - 1, coords.y], world.nodesPerLine, nodeLocalCoordsZ, height);
                SetVertexHeight(world.chunks[coords.x, coords.y - 1], nodeLocalCoordsX, world.nodesPerLine, height);
                SetVertexHeight(world.chunks[coords.x - 1, coords.y - 1], world.nodesPerLine, world.nodesPerLine, height);
            }
            else if (nodeLocalCoordsX == 0)
            {
                SetVertexHeight(world.chunks[coords.x - 1, coords.y], world.nodesPerLine, nodeLocalCoordsZ, height);
            }
            else if (nodeLocalCoordsZ == 0)
            {
                SetVertexHeight(world.chunks[coords.x, coords.y - 1], nodeLocalCoordsX, world.nodesPerLine, height);
            }

            void SetVertexHeight(WorldChunk chunk, int nodeX, int nodeY, float h)
            {
                var hx = nodeX / (float)chunk.world.nodesPerCell;
                var hy = nodeY / (float)chunk.world.nodesPerCell;
                if (hx % 1 == 0 && hy % 1 == 0)
                {
                    chunk.heightMap[(int)hx, (int)hy] = h;
                }
                chunk.terrain[WorldManager.ACTIVE_LOD].SetHeight((int)hx, (int)hy, h);
                chunk.terrain[WorldManager.ACTIVE_LOD].SetHeight(chunk.heightMap);
            }
        }

        public void UpdateTerrainMesh()
        {
            var terrainMeshData = terrain[WorldManager.ACTIVE_LOD];
            var terrainMesh = terrainMeshFilter.sharedMesh;
            if (terrainMesh == null)
            {
                terrainMesh = terrainMeshFilter.sharedMesh = new Mesh();
            }
            terrainMesh.Clear();
            terrainMesh.vertices = terrainMeshData.vertices;
            terrainMesh.triangles = terrainMeshData.triangles;
            terrainMeshData.NormalsUpdate();
            terrainMesh.normals = terrainMeshData.normals;
            terrainMeshFilter.sharedMesh = terrainMesh;
            terrainMeshCollider.sharedMesh = terrainMesh;
        }

        public void GenerateHeightMap(FastNoiseLite noiseGen, float heightMultiplier)
        {
            for (int y = 0; y < heightMap.GetLength(1); y++)
            {
                for (int x = 0; x < heightMap.GetLength(0); x++)
                {
                    var noise = noiseGen.GetNoise(
                        (coords.x * world.cellsPerLine) + x,
                        (coords.y * world.cellsPerLine) + y);
                    noise += 1;
                    noise *= .5f;
                    heightMap[x, y] = noise * heightMultiplier;
                    //TERRACES
                    //heightMap[x, y] = Mathf.FloorToInt(heightMap[x, y] / world.cellSize) * (world.cellSize);
                }
            }
        }

        public void AlignGridToHeightMap()
        {
            for (int z = 0; z < world.nodesPerLine; z++)
            {
                for (int x = 0; x < world.nodesPerLine; x++)
                {
                    var xx = x / (float)world.nodesPerCell;
                    var yy = z / (float)world.nodesPerCell;
                    var blx = (int)xx;
                    var bly = (int)yy;
                    grid.nodes[x, world.groundLevel, z].localPosition.y = Utils.QuadLerp(
                        // bottom left
                        heightMap[blx, bly],
                        // bottom right
                        heightMap[blx + 1, bly],
                        // top right
                        heightMap[blx + 1, bly + 1],
                        // top left
                        heightMap[blx, bly + 1],
                        // u
                        xx % 1,
                        // v
                        yy % 1);
                }
            }
        }
    }
}