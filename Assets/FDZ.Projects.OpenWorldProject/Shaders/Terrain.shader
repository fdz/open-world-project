﻿// https://www.youtube.com/watch?v=XdahmaohYvI&list=PLFt_AvWsXl0eBW2EiBtl_sxmDtSgZBxB3&index=16
Shader "FDZ/Terrain"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        CGPROGRAM

        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        /*******************************************************************************/

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
        
        // put more perinstance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        /*******************************************************************************/

        const static int MAX_LAYERS_COUNT = 8;
        const static float EPSILON = 1E-4;

        float minHeight;
        float maxHeight;

        int layersCount;
        UNITY_DECLARE_TEX2DARRAY(textures);
        float texturesScales[MAX_LAYERS_COUNT];
        float startingHeights[MAX_LAYERS_COUNT];
        float blends[MAX_LAYERS_COUNT];
        float3 tints[MAX_LAYERS_COUNT];
        float tintsStrength[MAX_LAYERS_COUNT];

        float rotationDegrees;

        /*******************************************************************************/

        struct Input
        {
            float3 worldPos;
            float3 worldNormal;
        };
        
        /*******************************************************************************/

        // saturate() = clamp()

        float inverseLerp(float a, float b, float value)
        {
            return saturate((value - a) / (b - a));
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float heightPercent = inverseLerp(minHeight, maxHeight, IN.worldPos.y);
            float3 blendAxes = abs(IN.worldNormal);
            blendAxes /= (blendAxes.x + blendAxes.y + blendAxes.z);
            for (int i = 0; i < layersCount; i++)
            {
                float drawStrength = inverseLerp(-blends[i] * .5f - EPSILON, blends[i] * .5f, heightPercent - startingHeights[i]);
                float3 color = tints[i] * tintsStrength[i];
                float3 scaledWorldPos = IN.worldPos / texturesScales[i];

                // triplanar mapping
                //float3 xProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(scaledWorldPos.y, scaledWorldPos.z, i)) * blendAxes.x;
                //float3 yProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(scaledWorldPos.x, scaledWorldPos.z, i)) * blendAxes.y;
                //float3 zProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(scaledWorldPos.x, scaledWorldPos.y, i)) * blendAxes.z;

                // triplanar mapping + texture rotation
                // https://forum.unity.com/threads/rotation-of-texture-uvs-directly-from-a-shader.150482/
                float s = sin(rotationDegrees);
                float c = cos(rotationDegrees);
                float2x2 rotationMatrix = float2x2(c, -s, s, c);
                rotationMatrix *= 0.5;
                rotationMatrix += 0.5;
                rotationMatrix = rotationMatrix * 2 - 1;
                float3 xProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(mul(scaledWorldPos.yz - 0.5, rotationMatrix) + 0.5, i)) * blendAxes.x;
                float3 yProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(mul(scaledWorldPos.xz - 0.5, rotationMatrix) + 0.5, i)) * blendAxes.y;
                float3 zProjection = UNITY_SAMPLE_TEX2DARRAY(textures, float3(mul(scaledWorldPos.xy - 0.5, rotationMatrix) + 0.5, i)) * blendAxes.z;

                float3 textureColor = xProjection + yProjection + zProjection;

                textureColor *= (1 - tintsStrength[i]);
                o.Albedo = o.Albedo * (1 - drawStrength) + (color + textureColor) * drawStrength;
            }
        }
        
        /*******************************************************************************/

        ENDCG
    }
    FallBack "Diffuse"
}
