﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// https://catlikecoding.com/unity/tutorials/flow/waves/
Shader "FDZ/Waves"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        
        // WAVE STUFF
        //_Steepness ("Steepness", Range(0, 1)) = 0.5
        //_Length ("Length", Float) = 10.0
        //_WaveA ("Wave A (dir, steepness, length)", Vector) = (1.0, 1.0, 0.25, 64)
        //_WaveB ("Wave B (dir, steepness, length)", Vector) = (1.0, 0.6, 0.25, 31)
        //_WaveC ("Wave C (dir, steepness, length)", Vector) = (1.0, 1.3, 0.25, 18)
        
        _WaveA ("Wave A (dir, steepness, length)", Vector) = (1.0, 1.0, 0.09, 17)
        _WaveB ("Wave B (dir, steepness, length)", Vector) = (1.0, 0.6, 0.11, 9)
        _WaveC ("Wave C (dir, steepness, length)", Vector) = (1.0, 1.3, 0.05, 5)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM

        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert addshadow
        // addshadow: makes the shadows of other object to adjust to the waves

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        /*********************************************************************************/

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
        
        // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        /*********************************************************************************/

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        struct v2f {
            float4 vertex : SV_POSITION;
            fixed4 color : COLOR;
            float2 texcoord : TEXCOORD0;
            float4 projPos : TEXCOORD1;
        };
           

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // WATER STUFF
        float4 _WaveA;
        float4 _WaveB;
        float4 _WaveC;

        /*********************************************************************************/

        float3 GerstnerWave (float4 wave, float3 p, inout float3 tangent, inout float3 binormal)
        {
            float steepness = wave.z;
            float3 length   = wave.w;
            float k = 2 * UNITY_PI / length;
            // instead of speed, 9.8 = earth gravity his is true for waves in deep water. In shallow water the water depth also plays a role, but we won't cover that here.
            float c = sqrt(9.8 / k);
            float2 d = normalize(wave.xy);
            float f = k * ( dot(d, p.xz) - c * _Time.y );
            float a = steepness / k;
            
            // tangent.z = binormal.x We don't need to optimize for this,
            // because the shader compiler takes care of that,
            // just like the sine and cosine are calculate only once.

            tangent += float3(
                1 - d.x * d.x * (steepness * sin(f)),
                d.x * (steepness * cos(f)),
                -d.x * d.y * (steepness * sin(f))
            );

            binormal += float3(
                -d.x * d.y * (steepness * sin(f)),
                d.y * (steepness * cos(f)),
                1 - d.y * d.y * (steepness * sin(f))
            );

            return float3(
                d.x * (a * cos(f)),
                a * sin(f),
                d.y * (a * cos(f))
            );
        }

        /*********************************************************************************/

        void vert (inout appdata_full vertexData)
        {
            float3 steepness = float3(_WaveA.z, _WaveB.z, _WaveC.z);
            float l = length(steepness);
            if (l > 1)
            {
                steepness /= l;
                _WaveA.z = steepness.x;
                _WaveB.z = steepness.y;
                _WaveC.z = steepness.z;
            }

            float3 localPos = vertexData.vertex.xyz;
            float3 worldPos = mul(unity_ObjectToWorld, vertexData.vertex).xyz;            
			float3 tangent = 0;
			float3 binormal = 0;
			float3 p = localPos;

			p += GerstnerWave(_WaveA, worldPos, tangent, binormal);
			p += GerstnerWave(_WaveB, worldPos, tangent, binormal);
			p += GerstnerWave(_WaveC, worldPos, tangent, binormal);

			float3 normal = normalize(cross(binormal, tangent));
			
            vertexData.vertex.xyz = p;
			vertexData.normal = normal;
        }

        /*********************************************************************************/

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        
        /*********************************************************************************/
        
        ENDCG
    }
    FallBack "Diffuse"
}
